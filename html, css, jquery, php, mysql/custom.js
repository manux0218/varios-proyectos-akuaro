// Si navega por movil lo enviamos a la APP
$(document).ready(function(){
	"use strict";
	if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
		$("html, body").empty();
		window.location = "http://gacinformatica.com/extranet/m/";
		return false;
	}
});

var swal;
var moment;

// Tooltip
$(function(){
	"use strict";
	$("[data-toggle='tooltip']").tooltip();
});

// Generador contraseña
function genPass(){
	"use strict";
	var chars = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
	var string_length = 12;
	var randomstring = '';
	var charCount = 0;
	var numCount = 0;
	for(var i=0; i<string_length; i++){
		if((Math.floor(Math.random() * 2) === 0) && numCount < 3 || charCount >= 5) {
			var rnum = Math.floor(Math.random() * 10);
			randomstring += rnum;
			numCount += 1;
		}else{
			var rnum = Math.floor(Math.random() * chars.length);
			randomstring += chars.substring(rnum,rnum+1);
			charCount += 1;
		}
	}
	return randomstring;
}

// Calendario
$(function(){
	"use strict";
	$("#fecha, #ffecha, #mffecha, #f_entrada, #edtf_fin, #edtf_aviso, #edtf_recogida, #asgf_recogida, #FentSchSpaguera, #FentSchSponsa, #spf_entrada, #spedtf_fin, #spedtf_aviso, #spedtf_recogida, #spasgf_recogida, #FechaSchpedidos, #PedidoSchpedidos, #pedfecha, #pedpedido, #pedstock, #pedfinalizado, #pededtpedido, #pededtstock, #pededtfinalizado, #pedfinfinalizado, #lldia, #DiaSchllamadas, #edtffecha, #Frecog1SchSponsa, #Frecog2SchSponsa, #Frecog1SchSpaguera, #Frecog2SchSpaguera, #incedt_fecha, #lledtdia, #pededtfecha, #Ntafecha, #FechaSchNta, #FechaTSchNta, #salfecha, #salfecha_llegada, #saledtfecha, #FechaSchSalidas, #FechaLlegadaSchSalidas, #fhifecha, #fhiedtfecha").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: "dd/mm/yy",
		firstDay: 1,
		monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Nomviembre", "Diciembre"],
		monthNamesShort : ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
		dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"]
	});
	$("#FcSchInc1").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: "dd/mm/yy",
		firstDay: 1,
		monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Nomviembre", "Diciembre"],
		monthNamesShort : ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
		dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
		numberOfMonths: 2,
		onClose: function(selectedDate){
			$("#FcSchInc2").datepicker("option", "minDate", selectedDate);
			$("#FcSchInc2").datepicker("show");
		}
    });
    $("#FcSchInc2").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: "dd/mm/yy",
		firstDay: 1,
		monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Nomviembre", "Diciembre"],
		monthNamesShort : ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
		dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
		numberOfMonths: 2,
		onClose: function(selectedDate){
			$("#FcSchInc1").datepicker("option", "maxDate", selectedDate);
		}
    });
	$("#FcSchInc1").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: "dd/mm/yy",
		firstDay: 1,
		monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Nomviembre", "Diciembre"],
		monthNamesShort : ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
		dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
		numberOfMonths: 2,
		onClose: function(selectedDate){
			$("#FcSchInc2").datepicker("option", "minDate", selectedDate);
			$("#FcSchInc2").datepicker("show");
		}
    });
	// Fecha solo para dia acceso remoto
	$("#accremotoDia, #accremotoEdtDia").datepicker({
		changeMonth: false,
		changeYear: false,
		dateFormat: "dd/mm",
		firstDay: 1,
		monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Nomviembre", "Diciembre"],
		monthNamesShort : ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
		dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"]
	});
});

// Formato fecha mientras se escribe
var patron;
patron = new Array(2, 2, 4);
var val; var val2; var val3; var largo; var r; var f; var letra; var s; var q;
function FormatoFecha(d, sep, pat, nums){
	"use strict";
	if(d.valant !== d.value){
		val   = d.value;
		largo = val.length;
		val   = val.split(sep);
		val2  = "";
		for(r=0; r < val.length; r++){
			val2 += val[r];	
		}
		if(nums){
			for(f = 0; f < val2.length; f++){
				if(isNaN(val2.charAt(f))){
					letra = new RegExp(val2.charAt(f), "g");
					val2  = val2.replace(letra, "");
				}
			}
		}
		val  = "";
		val3 = [];
		for(s = 0; s < pat.length; s++){
			val3[s] = val2.substring(0, pat[s]);
			val2    = val2.substr(pat[s]);
		}
		for(q = 0; q < val3.length; q++){
			if(q === 0){
				val = val3[q];
			}else{
				if(val3[q] !== ""){
					val += sep + val3[q];
				}
			}
		}
		d.value  = val;
		d.valant = val;
	}
}

// Comprobar que ha cargado el documento
$(document).ready(function(){
	"use strict";
	
	// Añadir por defecto para PCs (no movil)
	$(".breadcrumb").hide();
	$("#TableResponsive").removeClass("table-responsive");
	$("#TxtIncSch").css("width", "120px");
	
	// Cerrar Modal (X)
	$("#ModalCloseBtn, .modal .close").click(function(){
		window.open($(location).attr("href"), "_parent");
	});

	// Detectar fecha automaticamente
	var TodayAsign = new Date();
	var TodayDay = TodayAsign.getDate();
	var TodayMonth = TodayAsign.getMonth() + 1;
	var TodayYear = TodayAsign.getFullYear();
	var TodayHour = TodayAsign.getHours();
	var TodayMinute = TodayAsign.getMinutes();
	if(TodayMonth < 10){
		TodayMonth = "0" + TodayMonth;
	}
	if(TodayDay < 10){
		TodayDay = "0" + TodayDay;
	}
	// Fecha hoy
	$(".TodayDate, .TodayAll").click(function(){
		$("#spf_entrada, #f_entrada, #fecha, #incedt_fecha, #edtffecha, #lledtdia, #lldia, #pedfecha, #pededtfecha, #edtf_entrada, #Ntafecha").val(TodayDay + "/" + TodayMonth + "/" + TodayYear);
	});
	// Hora actual
	$(".TodayTime, .TodayAll").click(function(){
		$("#hora, #incedt_hora, #edtfhora, #lledthora, #llhora, #pedhora, #pededthora, #Ntahora").val(TodayHour);
		$("#minuto, #minutos, #incedt_minuto, #edtfminutos, #lledtminutos, #llminutos, #pedminutos, #pededtminutos, #Ntaminutos").val(TodayMinute);
	});
	// Sacar fecha individual para fecha pedido
	$("#TodayPedido").click(function(){
		$("#pedpedido, #pededtpedido").val(TodayDay + "/" + TodayMonth + "/" + TodayYear);
	});
	// Sacar fecha individual para fecha pedido en stock
	$("#TodayStock").click(function(){
		$("#pedstock, #pededtstock").val(TodayDay + "/" + TodayMonth + "/" + TodayYear);
	});
	// Sacar fecha individual para fecha pedido finalizado
	$("#TodayFinalizado").click(function(){
		$("#pedfinalizado, #pededtfinalizado, #pedfinfinalizado").val(TodayDay + "/" + TodayMonth + "/" + TodayYear);
	});
	// Sacar fecha individual para fecha fin reparacion
	$("#TodayFin, #TodayFin2").click(function(){
		$("#spedtf_fin, #edtf_fin").val(TodayDay + "/" + TodayMonth + "/" + TodayYear);
	});
	// Sacar fecha individual para fecha aviso
	$("#TodayAviso, #TodayAviso2").click(function(){
		$("#spedtf_aviso, #edtf_aviso").val(TodayDay + "/" + TodayMonth + "/" + TodayYear);
	});
	// Sacar fecha individual para fecha recogida
	$("#TodayRecogida, #TodayRecogida2").click(function(){
		$("#spedtf_recogida, #edtf_recogida").val(TodayDay + "/" + TodayMonth + "/" + TodayYear);
	});
	// Sacar fecha hoy individual para fecha salidas
	$("#TodaySalFecha, #TodaySalEdtFecha").click(function(){
		$("#salfecha, #saledtfecha").val(TodayDay + "/" + TodayMonth + "/" + TodayYear);
		$("#salhora, #saledthora").val(TodayHour);
		$("#salminutos, #saledtminutos").val(TodayMinute);
	});
	// Sacar fecha hoy individual para fecha llegada salidas
	$("#TodaySalFechaLlegada, #TodaySalEdtFechaLlegada").click(function(){
		$("#salfecha_llegada, #saledtfecha_llegada").val(TodayDay + "/" + TodayMonth + "/" + TodayYear);
		$("#salfecha_llegada_hora, #saledtfecha_llegada_hora").val(TodayHour);
		$("#salfecha_llegada_minutos, #saledtfecha_llegada_minutos").val(TodayMinute);
	});
	// Sacar fecha hoy individual para fecha fhios
	$("#TodayFhiFecha, #TodayFhiEdtFecha").click(function(){
		$("#fhifecha, #fhiedtfecha").val(TodayDay + "/" + TodayMonth + "/" + TodayYear);
		$("#fhihora, #fhiedthora").val(TodayHour);
		$("#fhiminutos, #fhiedtminutos").val(TodayMinute);
	});
	// Sacar dia y hora actual individual para acceso remoto
	$(".TodayDate").click(function(){
		$("#accremotoDia, #accremotoEdtDia").val(TodayDay + "/" + TodayMonth);
		$("#accremotoHora, #accremotoEdtHora").val(TodayHour);
		$("#accremotoMinutos, #accremotoEdtMinutos").val(TodayMinute);
	});
	
	// Botones personalizados (radio, checkbox)
	$("input:radio, input:checkbox").change(function(){
		if($(this).val() === "Si" || $(this).val() === "1"){
			$("#btnSi").css({
				"background-color": "#7EC142",
				"border-color": "transparent"
			});
		}else{
			$("#btnSi").css({
				"background-color": "#ffffff",
				"border-color": "transparent"
			});
		}
		if($(this).val() === "No" || $(this).val() === "0"){
			$("#btnNo").css({
				"background-color": "#F0592A",
				"border-color": "transparent"
			});
		}else{
			$("#btnNo").css({
				"background-color": "#fff",
				"border-color": "transparent"
			});
		}
		if($(this).val() === "paguera"){
			$("#btnPaguera").css({
				"background-color": "#FFC60A",
				"color": "#fff",
				"border-color": "transparent"
			});
		}else{
			$("#btnPaguera").css({
				"background-color": "#fff",
				"color": "#333333",
				"border-color": "transparent"
			});
		}
		if($(this).val() === "sponsa"){
			$("#btnSPonsa").css({
				"background-color": "#66CDF2",
				"color": "#fff",
				"border-color": "transparent"
			});
		}else{
			$("#btnSPonsa").css({
				"background-color": "#fff",
				"color": "#333333",
				"border-color": "transparent"
			});
		}
	});
	$("#frmAddTareas input:radio[name=color], #frmEdtTareas input:radio[name=edtcolor]").change(function(){
		if($(this).val() === "true"){
			$("#btnTclSi, #btnEdtTclSi").css({
				"background-color": "#7EC142",
				"color": "#ffffff",
				"border-color": "transparent"
			});
		}else{
			$("#btnTclSi, #btnEdtTclSi").css({
				"background-color": "#fff",
				"color": "#333333",
				"border-color": "transparent"
			});
		}
		if($(this).val() === "false"){
			$("#btnTclNo, #btnEdtTclNo").css({
				"background-color": "#F0592A",
				"color": "#ffffff",
				"border-color": "transparent"
			});
		}else{
			$("#btnTclNo, #btnEdtTclNo").css({
				"background-color": "#fff",
				"color": "#333333",
				"border-color": "transparent"
			});
		}
	});
	$("#frmAddTareas input:radio[name=allday], #frmEdtTareas input:radio[name=edtallday]").change(function(){
		if($(this).val() === "true"){
			$("#btnTallSi, #btnEdtTallSi").css({
				"background-color": "#7EC142",
				"color": "#ffffff",
				"border-color": "transparent"
			});
		}else{
			$("#btnTallSi, #btnEdtTallSi").css({
				"background-color": "#fff",
				"color": "#333333",
				"border-color": "transparent"
			});
		}
		if($(this).val() === "false"){
			$("#btnTallNo, #btnEdtTallNo").css({
				"background-color": "#F0592A",
				"color": "#ffffff",
				"border-color": "transparent"
			});
		}else{
			$("#btnTallNo, #btnEdtTallNo").css({
				"background-color": "#fff",
				"color": "#333333",
				"border-color": "transparent"
			});
		}
	});
	$("#btnNo, #btnSi, #btnPaguera, #btnSPonsa, #btnTclSi, #btnTclNo, #btnTallSi, #btnTallSi, #btnEdtTclSi, #btnEdtTclNo, #btnEdtTallSi, #btnEdtTallSi").css("border-color", "#cccccc");
	
	// Eliminar color error (label)
	$(".control-group").on("keypress change", function(){
		$("label", this).css("color", "#333333");
	});
	
	// Acceso
	$("#dni, #password").keypress(function(e){
		var PressEnter = e.which;
 		if(PressEnter === 13){
			$("#BtnAcceso").click();
    		return false;  
  		}
	});
	$("#BtnAcceso").click(function(){
		$("#ErrorAcceso").html("");
		var AccDNI = $("#dni");
		var AccPassword = $("#password");
		AccDNI.on("keypress", function(){
			AccDNI.closest(".control-group").removeClass("error");
		});
		AccPassword.on("keypress", function(){
			AccPassword.closest(".control-group").removeClass("error");
		});
		if(AccDNI.val().length <= 0){
			AccDNI.closest(".control-group").addClass("error");
			AccDNI.addClass("parpadeo");
			setTimeout(function(){
				AccDNI.removeClass("parpadeo");
			}, 1500);
		}
		if(AccPassword.val().length <= 0){
			AccPassword.closest(".control-group").addClass("error");
			AccPassword.addClass("parpadeo");
			setTimeout(function(){
				AccPassword.removeClass("parpadeo");
			}, 1500);
		}
		if(AccDNI.val().length > 0 && AccPassword.val().length > 0){
			AccDNI.closest(".control-group").removeClass("error").addClass("success");
			AccPassword.closest(".control-group").removeClass("error").addClass("success");
			$.ajax({
				type: "POST",
				url: "pages/ajax/acceso.php",
				data: {
					dni: AccDNI.val(),
					password: AccPassword.val()
				},
				dataType: "html",
				beforeSend: function(){
					$("#BtnAcceso").attr("disabled", true);
					$("#BtnAcceso").html("<img src='images/ajax-loader.gif'> Cargando...");
				},
				success: function(data){
					setTimeout(function(){
						$("#AccessLoading, #fadeIn").hide();
						$("#ResultAcceso").html(data);
						$("#BtnAcceso").html("Entrar");
						$("#BtnAcceso").attr("disabled", false);
					}, 1000);
				}
			});
		}
	});
	
	// Calendario (Agenda)
	$(".CancelTarea").click(function(){
		$(".sweet-overlay").css("display", "none");
		$("#AlertTarAdd, #AlertTarEdt").removeClass("sweet-alert showSweetAlert visible").addClass("hidden");
		$("#Calendario").fullCalendar("refetchEvents");
	});
	setInterval(function(){
		$("#Calendario").fullCalendar("refetchEvents");
	}, 60000);
	$("#Calendario").fullCalendar({
		defaultDate: new Date(),
		contentHeight: "auto",
		header: {
			center: "",
			left: "title prev,next,today agendaDay,agendaWeek,month",
			right: ""
		},
		eventLimit: 2,
		timeFormat: " ",
		defaultView: "agendaDay",
		editable: false,
		selectable: true,
		selectHelper: true,
		hiddenDays: [0],
		events: "pages/ajax/mostrartareas.php",
		loading: function(calendar){
			if(calendar){
				$("#LoadCalendar, #FadeCalendar").show();
			}else{
				setTimeout(function(){
					$("#FadeCalendar, #LoadCalendar").hide();
				}, 1000);
			}
		},
		eventMouseover: function(event) {
			$(this).css("z-index", 9);
			$(".DeleteTarea").click(function(){
				swal({
					title: "¿Está seguro?",
					text: "¡Está apunto de eliminar la tarea nº " + event.id + "!",
					type: "warning",
					showCancelButton: true,
					showLoaderOnConfirm: true,
					confirmButtonText: "Si, Eliminar",
					cancelButtonText: "No, Cancelar",
					closeOnConfirm: false,
					closeOnCancel: true
				}, function(isConfirm){
					if(isConfirm){
						$.ajax({
							type: "POST",
							url: "pages/ajax/eliminartarea.php",
							data: {
								delid: event.id
							},
							success: function(){
								swal({
									title: "¡Tarea eliminada!",
									type: "success",
									confirmButtonText: "Cerrar"
								});
								$("#Calendario").fullCalendar("removeEvents", event.id);
								$("#Calendario").fullCalendar("refetchEvents");
							}
						});
					}
				});
			});
			var TitleTooltip = event.title.split("//");
			if(TitleTooltip[2].length > 19){
				var tooltip = "<div class='tooltipevent top'>" + TitleTooltip[2] + "</div>";
				$("body").append(tooltip);
				$(this).mouseover(function(){
					$(this).css("z-index", 10000);
					$(".tooltipevent").fadeIn("500");
					$(".tooltipevent").fadeTo("10", 1.9);
				}).mousemove(function(e) {
					$(".tooltipevent").css({
						"top": e.pageY + 10,
						"left": e.pageX + 20,
						"width": "auto",
						"max-width": "400px"
					});
				});
			}
		},
		eventMouseout: function() {
			$(this).css('z-index', 0);
			$('.tooltipevent').remove();
		},
		select: function(start){
			var AllDay = "";
			var Color = "";
			$("#AlertTarAdd, .sweet-overlay").css("display", "block");
			$("#AlertTarAdd").removeClass("hidden").addClass("sweet-alert showSweetAlert visible");
			var DateStart = moment(start).format();
			var DateStartFormat = DateStart.split("T");
			var DateSwalHour = DateStartFormat[1].split("+");
			var eventData;
			$("#btnTclNo, #btnTallNo").css({
				"background-color": "#d9534f",
				"color": "#ffffff"
			});
			if(DateSwalHour[0] === "00:00:00"){
				$("#btnTallNo").css({ "background-color": "#fff", "color": "#333" });
				$("#btnTallSi").css({ "background-color": "#5cb85c", "color": "#fff" });
				$("#alldayT").attr("checked", true);
				$("#alldayF").attr("checked", false);
			}else{
				$("#btnTallNo").css({ "background-color": "#d9534f", "color": "#fff" });
				$("#btnTallSi").css({ "background-color": "#fff", "color": "#333" });
				$("#alldayT").attr("checked", false);
				$("#alldayF").attr("checked", true);
			}
			$("#frmAddTareas input:radio[name=color]").change(function(){
				if($(this).val() === "true"){
					Color = "#FD5050";
				}else if($(this).val() === "false"){
					Color = "";
				}
			});
			$("#frmAddTareas input:radio[name=allday]").change(function(){
				if($(this).val() === "true"){
					AllDay = "true";
				}else if($(this).val() === "false") {
					AllDay = "";
				}
			});
			$("#tomador, #title").on("keypress change", function(){
				$("#AddinputError").removeClass("show");
			});
			$("#ConfirmAdd").click(function(){
				if($("#title").val().length > 0 && $("#tomador").val().length > 0 && $("#hora").val().length > 0 && $("#minutos").val().length > 0){
					var Hora = $("#hora").val();
					var Minutos = $("#minutos").val();
					var Tomador = $("#tomador").val();
					var Title = $("#title").val();
					eventData = {
						title: Title,
						start: DateStartFormat[0] + " " + Hora + ":" + Minutos
					};
					$.ajax({
						type: "POST",
						url: "pages/ajax/subirtarea.php",
						data: {
							addtomador: Tomador,
							addtitle: Title,
							addstart: DateStartFormat[0] + " " + Hora + ":" + Minutos,
							addallday: AllDay,
							addcolor: Color
						},
						success: function(){
							$("#frmAddTareas")[0].reset();
							$("#Calendario").fullCalendar("refetchEvents");
							window.open($(location).attr("href"), "_parent");
						}
					});
				}else{
					$("#AddinputError").addClass("show");
				}
			});
		},
		eventRender: function(event, element){
			element.bind("dblclick", function(){
				$("#AlertTarEdt, .sweet-overlay").css("display", "block");
				$("#AlertTarEdt").removeClass("hidden").addClass("sweet-alert showSweetAlert visible");
				var EdtID = event.id;
				var EdtTitleFormat = event.title.split("//");
				var EdtTomador = EdtTitleFormat[0];
				var EdtTitle = EdtTitleFormat[2];
				var EdtAllDay = event.allDay;
				var EdtColor = event.color;
				var eventData;
				var EdtStartFormat = moment(event.start).format().split("T");
				var EdtStart = EdtStartFormat[1].split("+");
				$("#edtTid").text(EdtStart[0].substring(0, 5));
				$("#edttitle").val(EdtTitle);
				$("#edttomador").text(EdtTomador);
				$(".delete").attr("data-id", EdtID);
				if(EdtColor === ""){
					$("#btnEdtTclNo").css({ "background-color": "#F0592A", "color": "#333" });
					$("#btnEdtTclSi").css({ "background-color": "#fff", "color": "#333" });
					$("#frmEdtTareas input:radio[name=edtcolor]").attr("checked", true);
					$("#edtcolorF").attr("checked", true);
				}else{
					$("#btnEdtTclNo").css({ "background-color": "#fff", "color": "#333" });
					$("#btnEdtTclSi").css({ "background-color": "#7EC142", "color": "#333" });
					$("#edtcolorT").attr("checked", true);
				}
				if(EdtAllDay === ""){
					$("#btnEdtTallNo").css({ "background-color": "#F0592A", "color": "#333" });
					$("#btnEdtTallSi").css({ "background-color": "#fff", "color": "#333" });
					$("#edtalldayF").attr("checked", true);
				}else{
					$("#btnEdtTallNo").css({ "background-color": "#fff", "color": "#333" });
					$("#btnEdtTallSi").css({ "background-color": "#7EC142", "color": "#333" });
					$("#edtalldayT").attr("checked", true);
				}
				$("#frmEdtTareas input:radio[name=edtcolor]").change(function(){
					if($(this).val() === "true"){
						EdtColor = "#F0592A";
					}else if($(this).val() === "false"){
						EdtColor = "";
					}
				});
				$("#frmEdtTareas input:radio[name=edtallday]").change(function(){
					if($(this).val() === "true"){
						EdtAllDay = "true";
					}else if($(this).val() === "false") {
						EdtAllDay = "";
					}
				});
				$("#edttitle").on("keypress", function(){
					$("#EdtinputError").removeClass("show");
				});
				$("#ConfirmEdt").click(function(){
					var EdtTitle = $("#edttitle").val();
					if($("#edttitle").val().length > 0){
						eventData = {
							title: EdtTitle,
							start: event.start
						};
						$.ajax({
							type: "POST",
							url: "pages/ajax/editartarea.php",
							data: {
								edtid: event.id,
								edttitle: EdtTitle,
								edtallday: EdtAllDay,
								edtcolor: EdtColor
							},
							success: function(){
								$("#Calendario").fullCalendar("refetchEvents");
								window.open($(location).attr("href"), "_parent");
							}
						});
					}else{
						$("#EdtinputError").addClass("show");
					}
				});
			});
		},
		eventDrop: function(event) {
			var NewStart = moment(event.start).format();
			var NewStartFormat = NewStart.split("T");
			var NewStartFormat2 = NewStartFormat[1].split("+");
			var NewEnd = moment(event.end).format();
			var NewEndFormat = NewEnd.split("T");
			var NewEndFormat2 = NewEndFormat[1].split("+");
			$.ajax({
				type: "POST",
				url: "pages/ajax/actualizartarea.php",
				data: {
					updid: event.id,
					updstart: NewStartFormat[0] + " " + NewStartFormat2[0],
					updend: NewEndFormat[0] + " " + NewEndFormat2[0]
				},
				success: function(){
					$("#Calendario").fullCalendar("updateEvent", event.id);
				}
			});
		},
	});
	
	// Eliminar tarea
	$(".DelHisTarea").click(function(){
		var DelID = $(this).data("id");
		var DelTomador = $(this).data("tomador");
		swal({
			title: "¿Está seguro?",
		  	text: "¡Está apunto de eliminar la tarea de " + DelTomador + "!",
		  	type: "warning",
		  	showCancelButton: true,
			showLoaderOnConfirm: true,
		  	confirmButtonText: "Si, Eliminar",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true
		},
		function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Tarea eliminada!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						type: "POST",
						url: "pages/ajax/eliminarhistarea.php",
						data: {
							deltarid: DelID
						}
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open($(location).attr("href"), "_parent");
				});
			}
		});
	});
	
	// Eliminar historial de tareas
	$("#DelHisAllTar").click(function(){
		swal({
			title: "¿Está seguro?",
		  	text: "¡Está apunto de eliminar todo el historial de tareas!",
		  	type: "warning",
		  	showCancelButton: true,
		  	confirmButtonText: "Si, Eliminar Todo",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true,
			showLoaderOnConfirm: true
		},function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Historial eliminado!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						url: "pages/ajax/eliminartodohistareas.php"
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open($(location).attr("href"), "_parent");
				});
			}
		});
	});
	
	// Agregar incidencias
	var IncUrgente;
	var IncErrores;
	var IncTomador = $("#tomador");
	var IncFecha = $("#fecha");
	var IncHora = $("#hora");
	var IncMinuto = $("#minuto");
	var IncCliente = $("#cliente");
	var IncSolicitante = $("#solicitante");
	var IncDescripcion = $("#descripcion");
	var IncTipoIncidencia = $("#tipo");
	var IncEstado = $("#estado");
	$("#IncEstado").hide();
	$("#frmAIncidencias input:radio[name=urgente]").change(function(){
		if($(this).val() === "Si"){
			IncUrgente = 1;
			$("#IncFinalizada, #IncEstado").hide();
			IncEstado.val("");
		}else if($(this).val() === "No"){
			IncUrgente = 0;
			$("#IncEstado").show();
		}
	});
	$("#BtnAIncidencia").click(function(){
		if(IncTomador.val().length <= 0) {
			$("#frmAIncidencias label[for=tomador]").css("color", "#f94442");
			IncErrores = 1;
		}else{
			$("#frmAIncidencias label[for=tomador]").css("color", "#333333");
			IncErrores = 0;
		}
		if(IncFecha.val().length <= 0 || IncHora.val().length <= 0 || IncMinuto.val().length <= 0) {
			$("#frmAIncidencias label[for=fecha]").css("color", "#f94442");
			IncErrores = 1;
		}else{
			$("#frmAIncidencias label[for=fecha]").css("color", "#333333");
			IncErrores = 0;
		}
		if(IncCliente.val().length <= 0) {
			$("#frmAIncidencias label[for=cliente]").css("color", "#f94442");
			IncErrores = 1;
		}else{
			$("#frmAIncidencias label[for=cliente]").css("color", "#333333");
			IncErrores = 0;
		}
		if(IncSolicitante.val().length <= 0) {
			$("#frmAIncidencias label[for=solicitante]").css("color", "#f94442");
			IncErrores = 1;
		}else{
			$("#frmAIncidencias label[for=solicitante]").css("color", "#333333");
			IncErrores = 0;
		}
		if(IncDescripcion.val().length <= 0) {
			$("#frmAIncidencias label[for=descripcion]").css("color", "#f94442");
			IncErrores = 1;
		}else{
			$("#frmAIncidencias label[for=descripcion]").css("color", "#333333");
			IncErrores = 0;
		}
		if(IncTipoIncidencia.val().length <= 0) {
			$("#frmAIncidencias label[for=tipo]").css("color", "#f94442");
			IncErrores = 1;
		}else{
			$("#frmAIncidencias label[for=tipo]").css("color", "#333333");
			IncErrores = 0;
		}
		if(!$("#frmAIncidencias input:radio[name=urgente]").is(":checked")){
			$("#frmAIncidencias label[for=urgencia]").css("color", "#f94442");
			IncErrores = 1;
		}else{
			$("#frmAIncidencias label[for=urgencia]").css("color", "#333333");
			IncErrores = 0;
			if(IncUrgente === 0){
				if(IncEstado.val().length <= 0) {
					$("#frmAIncidencias label[for=estado]").css("color", "#f94442");
					IncErrores = 1;
				}else{
					$("#frmAIncidencias label[for=estado]").css("color", "#333333");
					IncErrores = 0;
				}
			}
		}
		if(IncErrores){
			swal({
				title: "¡Error al añadir la incidencia!",
				text: "Corrija los errores marcados en rojo",
				type: "error",
				confirmButtonText: "Cerrar"
			});
		}else{
			swal({
				title: "¡Incidencia añadida!",
				type: "success",
				confirmButtonText: "Cerrar"
			});
			$.ajax({
				type: "POST",
				url: "pages/ajax/subirincidencia.php",
				data: {
					tomador: IncTomador.val(),
					fecha: IncFecha.val(),
					hora: IncHora.val(),
					minutos: IncMinuto.val(),
					cliente: IncCliente.val(),
					solicitante: IncSolicitante.val(),
					descripcion: IncDescripcion.val(),
					tipo_incidencia: IncTipoIncidencia.val(),
					urgente: IncUrgente,
					estado: IncEstado.val()
				}
			});
			$("#frmAIncidencias")[0].reset();
			$("#urgeSi, #urgeNo").prop("checked", false);
			$("button.confirm").click(function(){
				window.open("./incidencias", "_parent");
			});
		}
	});
	
	// Asignar Técnico Incidencias
	$(".AsgTInc").click(function(){
		var AsgIncID = $(this).data("id");
		var AsgIncTecnico = $("#AsgTecIncSel");
		$("#AlertIncAsgTecnico, .sweet-overlay").css("display", "block");
		$("#AlertIncAsgTecnico").removeClass("hidden").addClass("sweet-alert showSweetAlert visible");
		$("#CancelAsgTinc").click(function(){
			$(".sweet-overlay").css("display", "none");
			$("#AlertIncAsgTecnico").removeClass("sweet-alert showSweetAlert visible").addClass("hidden");
		});
		$("#AsgTecIncSel").click(function(){
			$("#AsgTinputError").removeClass("show");
		});
		$("#ConfirmAsgTinc").click(function(){
			if(AsgIncTecnico.val().length <= 0){
				$("#AsgTinputError").addClass("show");
			}else{
				$(".sweet-overlay").css("display", "none");
				$("#AlertIncAsgTecnico").removeClass("sweet-alert showSweetAlert visible").addClass("hidden");
				swal({
					title: "Se ha asignado a", 
					text: "<div class='text-info' style='font-size:48px'>" + AsgIncTecnico.val() + "</div>", 
					type: "success",
					confirmButtonText: "Cerrar",
					html: true,
					animation: false
				});
				$.ajax({
					type: "POST",
					url: "pages/ajax/asgtecnicoincidencia.php",
					data: {
						asgTecnicoID: AsgIncID,
						asgTecnicoSel: AsgIncTecnico.val()
					}
				});
				$("button.confirm").click(function(){
					window.open("./incidencias", "_parent");
				});
			}
		});
	});
	
	// Ver Incidencias (modal)
	$(".VerInc").click(function(){
		var VerIncID = $(this).data("id");
		$("#ShowVerInc").modal({
			show: true,
			backdrop: "static",
    		keyboard: false
		});
		$("#IDIncVer").text(VerIncID);
		$.ajax({
			type: "POST",
			url: "pages/ajax/verincidencia.php",
			data: {
				incvid: VerIncID
			},
			success: function(data){
				$("#ResultVerInc").html(data);
			}
		});
	});
	
	// Editar incidencia
	var IncEdtErrores;
	var IncEdtID = $("#incedt_id");
	var IncEdtTomador = $("#incedt_tomador");
	var IncEdtFecha = $("#incedt_fecha");
	var IncEdtHora = $("#incedt_hora");
	var IncEdtMinuto = $("#incedt_minuto");
	var IncEdtCliente = $("#incedt_cliente");
	var IncEdtSolicitante = $("#incedt_solicitante");
	var IncEdtDescripcion = $("#incedt_descripcion");
	var IncEdtTecnico = $("#incedt_tecnico");
	var IncEdtTipo = $("#incedt_tipo");
	var IncEdtUrgente = $("#urge_sel").val();
	var IncEdtEstado = $("#incedt_estado");
	$("#frmEIncidencias input:radio[name=incedt_urgente]").change(function(){
		if($(this).val() === "Si"){
			IncEdtUrgente = 1;
			$("#ShowEdtEstado").hide();
		}else{
			IncEdtUrgente = 0;
			$("#ShowEdtEstado").show();
		}
	});
	$("#BtnEIncidencia").click(function(){
		if(IncEdtTomador.val() <= 0 || IncEdtFecha.val() <= 0 || IncEdtHora.val() <= 0 || IncEdtMinuto.val() <= 0 || IncEdtCliente.val() <= 0 || IncEdtSolicitante.val() <= 0 || IncEdtDescripcion.val() <= 0){
			$(this).css("background", "#f94442");
			IncEdtErrores = 1;
		}else{
			$(this).css("background", "#ffffff");
			IncEdtErrores = 0;
		}
		swal({
			title: "¡Incidencia Guardada!",
			type: "success",
			confirmButtonText: "Cerrar"
		});
		$.ajax({
			type: "POST",
			url: "pages/ajax/editarincidencia.php",
			data: {
				edtid: IncEdtID.val(),
				edttomador: IncEdtTomador.val(),
				edtfecha: IncEdtFecha.val(),
				edthora: IncEdtHora.val(),
				edtminuto: IncEdtMinuto.val(),
				edtcliente: IncEdtCliente.val(),
				edtsolicitante: IncEdtSolicitante.val(),
				edtdescripcion: IncEdtDescripcion.val(),
				edttecnico: IncEdtTecnico.val(),
				edttipo: IncEdtTipo.val(),
				edturgente: IncEdtUrgente,
				edtestado: IncEdtEstado.val()
			}
		});
		$("button.confirm").click(function(){
			window.open("./incidencias", "_parent");
		});
	});
	
	// Finalizar incidencia (modal)
	var ModalEdtFError;
	$(".FinalizarInc").click(function(){
		$(".finIncidenciaEdt").modal({
			show: true,
			backdrop: "static",
    		keyboard: false
		});
		var ModalEdtFNinc = $(this).data("id");
		var ModalEdtFTecnico = $("#edtftecnico");
		var ModalEdtFFecha = $("#edtffecha");
		var ModalEdtFHora = $("#edtfhora");
		var ModalEdtFMinutos = $("#edtfminutos");
		var ModalEdtFSolucion = $("#edtfsolucion");
		var ModalEdtFFacturado = $("#edtffacturado");
		ModalEdtFFacturado.numeric();
		$("#NincFinModal").text($(this).data("id"));
		$("#BtnEdtFIncidencia").click(function(){
			if(ModalEdtFTecnico.val().length <= 0) {
				$("#frmMFIncidencias label[for=tecnico]").css("color", "#f94442");
				ModalEdtFError = 1;
			}else{
				$("#frmMFIncidencias label[for=tecnico]").css("color", "#333333");
				ModalEdtFError = 0;
			}
			if(ModalEdtFFecha.val().length <= 0 || ModalEdtFHora.val().length <= 0 || ModalEdtFMinutos.val().length <= 0) {
				$("#frmMFIncidencias label[for=fecha]").css("color", "#f94442");
				ModalEdtFError = 1;
			}else{
				$("#frmMFIncidencias label[for=fecha]").css("color", "#333333");
				ModalEdtFError = 0;
			}
			if(ModalEdtFSolucion.val().length <= 0) {
				$("#frmMFIncidencias label[for=solucion]").css("color", "#f94442");
				ModalEdtFError = 1;
			}else{
				$("#frmMFIncidencias label[for=solucion]").css("color", "#333333");
				ModalEdtFError = 0;
			}
			if(ModalEdtFFacturado.val().length <= 0) {
				$("#frmMFIncidencias label[for=facturado]").css("color", "#f94442");
				ModalEdtFError = 1;
			}else{
				$("#frmMFIncidencias label[for=facturado]").css("color", "#333333");
				ModalEdtFError = 0;
			}
			if(ModalEdtFError){
				swal({
					title: "¡Incidencia no finalizada!",
					text: "Corrija los errores marcados en rojo",
					type: "error",
					confirmButtonText: "Cerrar"
				});
			}else{
				swal({
					title: "¡Incidencia finalizada!",
					type: "success",
					confirmButtonText: "Cerrar"
				});
				$.ajax({
					type: "POST",
					url: "pages/ajax/finalizarincidencia.php",
					data: {
						EdtFincNInc: ModalEdtFNinc,
						EdtFincTecnico: ModalEdtFTecnico.val(),
						EdtFincFecha: ModalEdtFFecha.val(),
						EdtFincHora: ModalEdtFHora.val(),
						EdtFincMinutos: ModalEdtFMinutos.val(),
						EdtFincSolucionado: ModalEdtFSolucion.val(),
						EdtFincFacturado: ModalEdtFFacturado.val()
					}
				});
				$("button.confirm").click(function(){
					window.open($(location).attr("href"), "_parent");
				});
			}
		});
	});
	
	// Eliminar Incidencias
	$(".DelInc").click(function(){
		var DelID = $(this).data("id");
		swal({
			title: "¿Está seguro?",
		  	text: "¡Está apunto de eliminar la incidencia nº " + DelID + "!",
		  	type: "warning",
		  	showCancelButton: true,
			showLoaderOnConfirm: true,
		  	confirmButtonText: "Si, Eliminar",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true
		},
		function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Incidencia eliminada!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						type: "POST",
						url: "pages/ajax/eliminarincidencia.php",
						data: {
							DIncNincidencia: DelID
						}
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open($(location).attr("href"), "_parent");
				});
			}
		});
	});
	
	// Eliminar Todas Incidencias Finalizadas
	$("#DelHisAllInc").click(function(){
		swal({
			title: "¿Está seguro?",
		  	text: "¡Está apunto de eliminar todo el historial de incidencias!",
		  	type: "warning",
		  	showCancelButton: true,
		  	confirmButtonColor: "#DD6B55",
		  	confirmButtonText: "Si, Eliminar Todo",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true,
			showLoaderOnConfirm: true
		},function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Historial incidencias eliminado!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						url: "pages/ajax/eliminarhisincidencias.php"
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open($(location).attr("href"), "_parent");
				});
			}
		});
	});
	
	// Buscador Incidencias
	$("#SchInc > li > a").removeAttr("href").css("cursor", "pointer");
	$("#ShowSchNincidenciaInc, #ShowSchFechaInc, #ShowSchTecnicoInc, #ShowSchTipoInc, #ShowSchEstadoInc").hide();
	$("#SchInc > li > a").click(function(){
		var DataSchType = $(this).data("type");
		if(DataSchType === "n_incidencia"){
			$("#NinSchInc").numeric();
			$("#TxtIncSch").text("Nº Incidencia");
			$("#ShowSchDefaultInc, #ShowSchFechaInc, #ShowSchTecnicoInc, #ShowSchTipoInc, #ShowSchEstadoInc").hide();
			$("#ShowSchNincidenciaInc").show();
			$("#BtnSchInc").click(function(){
				window.open("incidencias/buscar/?type=" + DataSchType + "&sch=" + $("#NinSchInc").val(), "_parent");
			});
		}else{
			$("#TxtIncSch").text(DataSchType.charAt(0).toUpperCase() + DataSchType.slice(1));
		}
		if(DataSchType === "fecha"){
			$("#ShowSchDefaultInc, #ShowSchNincidenciaInc, #ShowSchTecnicoInc, #ShowSchTipoInc, #ShowSchEstadoInc").hide();
			$("#ShowSchFechaInc").show();
			$("#BtnSchInc").click(function(){
				window.open("incidencias/buscar/?type=" + DataSchType + "&sch=" + $("#FcSchInc1").val() + "-" + $("#FcSchInc2").val(), "_parent");
			});
		}
		if(DataSchType === "tecnico"){
			$("#ShowSchDefaultInc, #ShowSchNincidenciaInc, #ShowSchFechaInc, #ShowSchTipoInc, #ShowSchEstadoInc").hide();
			$("#ShowSchTecnicoInc").show();
			$("#BtnSchInc").click(function(){
				window.open("incidencias/buscar/?type=" + DataSchType + "&sch=" + $("#TecSchInc").val(), "_parent");
			});
		}
		if(DataSchType === "tipo"){
			$("#ShowSchDefaultInc, #ShowSchNincidenciaInc, #ShowSchFechaInc, #ShowSchTecnicoInc, #ShowSchEstadoInc").hide();
			$("#ShowSchTipoInc").show();
			$("#BtnSchInc").click(function(){
				window.open("incidencias/buscar/?type=" + DataSchType + "&sch=" + $("#TipSchInc").val(), "_parent");
			});
		}
		if(DataSchType === "estado"){
			$("#ShowSchDefaultInc, #ShowSchNincidenciaInc, #ShowSchFechaInc, #ShowSchTecnicoInc, #ShowSchTipoInc").hide();
			$("#ShowSchEstadoInc").show();
			$("#BtnSchInc").click(function(){
				window.open("incidencias/buscar/?type=" + DataSchType + "&sch=" + $("#EstSchInc").val(), "_parent");
			});
		}
	});
	
	// Agregar Llamadas
	var LLError;
	var LLTienda = "";
	var LLTomador = $("#lltomador");
	var LLDia     = $("#lldia");
	var LLHora    = $("#llhora");
	var LLMinutos = $("#llminutos");
	var LLDonde   = $("#lldonde");
	var LLQuien   = $("#llquien");
	var LLMotivo  = $("#llmotivo");
	var LLPhone = $("#llphone");
	var LLCon     = $("#llcon");
	$("#frmAllamada input:radio[name=lltienda]").on("change", function(){
		LLTienda = $(this).val();
	});
	$("#showhablarcon").hide();
	$("#frmAllamada select[name=llcon]").on("change", function(){
		$("#showhablarcon").show();
	});
	$("#BtnAllamada").click(function(){
		if(LLTienda === ""){
			$("#frmAllamada label[for=tienda]").css("color", "#f94442");
			LLError = 1;
		}else{
			$("#frmAllamada label[for=tienda]").css("color", "#333333");
			LLError = 0;
		}
		if(LLTomador.val().length <= 0){
			$("#frmAllamada label[for=tomador]").css("color", "#f94442");
			LLError = 1;
		}else{
			$("#frmAllamada label[for=tomador]").css("color", "#333333");
			LLError = 0;
		}
		if(LLDia.val().length <= 0 || LLHora.val().length <= 0 || LLMinutos.val().length <= 0){
			$("#frmAllamada label[for=dia]").css("color", "#f94442");
			LLError = 1;
		}else{
			$("#frmAllamada label[for=tomador]").css("color", "#333333");
			LLError = 0;
		}
		if(LLDonde.val().length <= 0){
			$("#frmAllamada label[for=donde]").css("color", "#f94442");
			LLError = 1;
		}else{
			$("#frmAllamada label[for=donde]").css("color", "#333333");
			LLError = 0;
		}
		if(LLQuien.val().length <= 0){
			$("#frmAllamada label[for=quien]").css("color", "#f94442");
			LLError = 1;
		}else{
			$("#frmAllamada label[for=quien]").css("color", "#333333");
			LLError = 0;
		}
		if(LLMotivo.val().length <= 0){
			$("#frmAllamada label[for=motivo]").css("color", "#f94442");
			LLError = 1;
		}else{
			$("#frmAllamada label[for=motivo]").css("color", "#f94442");
			LLError = 0;
		}
		if(LLError){
			swal({
				title: "¡Error al añadir la llamada!",
				text: "Corrija los campos marcados en rojo",
				type: "error",
				confirmButtonText: "Cerrar"
			});
		}else{
			$.ajax({
				type: "POST",
				url: "pages/ajax/subirllamada.php",
				data: {
					llatienda: LLTienda,
					llatomador: LLTomador.val(),
					lladia: LLDia.val(),
					llahora: LLHora.val(),
					llaminutos: LLMinutos.val(),
					lladonde: LLDonde.val(),
					llaquien: LLQuien.val(),
					llamotivo: LLMotivo.val(),
					llaphone: LLPhone.val(),
					llacon: LLCon.val()
				}
			});
			swal({
				title: "¡Llamada añadida!",
				type: "success",
				confirmButtonText: "Cerrar"
			});
			$("button.confirm").click(function(){
				window.open("./llamadas", "_parent");
			});
		}
	});
	
	// Ver LLamadas (modal)
	$(".VerLlamada").click(function(){
		var VerLlaID = $(this).data("id");
		$("#ShowVerLlamada").modal({
			show: true,
			backdrop: "static",
			keyboard: false
		});
		$("#IDLlaVer").text(VerLlaID);
		$.ajax({
			type: "POST",
			url: "pages/ajax/verllamada.php",
			data: {
				llavid: VerLlaID
			},
			success: function(data){
				$("#ResultVerLla").html(data);
			}
		});
		$(".modal button.btn-default").click(function(){
			window.open($(location).attr("href"), "_parent");
		});
	});
	
	// Pasar LLamadas a Incidencias (modal)
	$(".PIncLlamada").click(function(){
		var PasLLaNo = $(this).data("nid");
		$("#NoLlaPas").text(PasLLaNo);
		swal({
			title: "¿Está seguro?",
		  	text: "¡Está apunto de pasar la llamada a incidencias! <br> Nº Llamada: <strong>" + PasLLaNo + "</strong>",
		  	type: "warning",
		  	showCancelButton: true,
		  	confirmButtonText: "Si, Continuar",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true,
			html: true
		},
		function(isConfirm){
			if(isConfirm){
				$(".sweet-overlay").css("display", "none");
				$(".showSweetAlert ").addClass("hidden");
				$("#ShowPasLlamadaInc").modal({
					show: true,
					backdrop: "static",
					keyboard: false
				});
				$.ajax({
					type: "POST",
					url: "pages/ajax/pasaraincidencia.php",
					data: {
						pasllaid: PasLLaNo
					},
					success: function(data){
						$("#ResultPasLlaInc").html(data);
					}
				});
			}
		});
	});
	
	// Editar llamadas
	var LLEdtError;
	var LLEdtID = $("#lledtid");
	var LLEdtTienda = $("#lledttienda").val();
	var LLEdtTomador = $("#lledttomador");
	var LLEdtDia = $("#lledtdia");
	var LLEdtHora = $("#lledthora");
	var LLEdtMinutos = $("#lledtminutos");
	var LLEdtDonde = $("#lledtdonde");
	var LLEdtQuien = $("#lledtquien");
	var LLEdtMotivo = $("#lledtmotivo");
	var LLEdtPhone = $("#lledtphone");
	var LLEdtCon = $("#lledtcon");
	$("input:radio[name=lledttienda]").on("change", function(){
		LLEdtTienda = $(this).val();
	});
	$("#frmEdtllamada select[name=lledtcon]").on("change", function(){
		$("#showedthablarcon").show();
	});
	$("#BtnEdtllamada").click(function(){
		if(LLEdtDia.val().length <= 0){
			$("#frmEdtllamada label[for=dia]").css("color", "#f94442");
			LLEdtError = 1;
		}else{
			$("#frmEdtllamada label[for=dia]").css("color", "#333333");
			LLEdtError = 0;
		}
		if(LLEdtDonde.val().length <= 0){
			$("#frmEdtllamada label[for=donde]").css("color", "#f94442");
			LLEdtError = 1;
		}else{
			$("#frmEdtllamada label[for=donde]").css("color", "#333333");
			LLEdtError = 0;
		}
		if(LLEdtQuien.val().length <= 0){
			$("#frmEdtllamada label[for=quien]").css("color", "#f94442");
			LLEdtError = 1;
		}else{
			$("#frmEdtllamada label[for=quien]").css("color", "#333333");
			LLEdtError = 0;
		}
		if(LLEdtMotivo.val().length <= 0){
			$("#frmEdtllamada label[for=motivo]").css("color", "#f94442");
			LLEdtError = 1;
		}else{
			$("#frmEdtllamada label[for=motivo]").css("color", "#333333");
			LLEdtError = 0;
		}
		if(LLEdtError){
			swal({
				title: "¡Llamada no guardada!",
				text: "Corrija los errores marcados en rojo",
				type: "error",
				confirmButtonText: "Cerrar"
			});
		}else{
			$.ajax({
				type: "POST",
				url: "pages/ajax/editarllamada.php",
				data: {
					edtid: LLEdtID.val(),
					edttienda: LLEdtTienda,
					edttomador: LLEdtTomador.val(),
					edtdia: LLEdtDia.val(),
					edthora: LLEdtHora.val(),
					edtminutos: LLEdtMinutos.val(),
					edtdonde: LLEdtDonde.val(),
					edtquien: LLEdtQuien.val(),
					edtmotivo: LLEdtMotivo.val(),
					edtphone: LLEdtPhone.val(),
					edtcon: LLEdtCon.val()
				}
			});
			swal({
				title: "¡Llamada guardada!",
				type: "success",
				confirmButtonText: "Cerrar"
			});
			$("button.confirm").click(function(){
				window.open("./llamadas", "_parent");
			});
		}
	});
	
	// Guardar Llamada en el historial
	$(".SavLlamada").click(function(){
		var SavID = $(this).data("id");
		swal({
			title: "¿Guardar llamada?",
		  	text: "Llamada nº " + SavID + "",
		  	type: "info",
		  	showCancelButton: true,
			showLoaderOnConfirm: true,
		  	confirmButtonText: "Guardar",
			cancelButtonText: "Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true
		},
		function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Llamada guardada!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						type: "POST",
						url: "pages/ajax/guardarllamada.php",
						data: {
							savllaid: SavID
						}
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open($(location).attr("href"), "_parent");
				});
			}
		});
	});
	
	// Eliminar Llamada
	$(".DelLlamada").click(function(){
		var DelID = $(this).data("id");
		swal({
			title: "¿Está seguro?",
		  	text: "¡Estás apunto de eliminar la llamada nº " + DelID + "!",
		  	type: "warning",
		  	showCancelButton: true,
			showLoaderOnConfirm: true,
		  	confirmButtonText: "Si, Eliminar",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true
		},
		function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Llamada eliminada!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						type: "POST",
						url: "pages/ajax/eliminarllamada.php",
						data: {
							delllaid: DelID
						}
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open($(location).attr("href"), "_parent");
				});
			}
		});
	});
	
	// Eliminar historial llamadas
	$("#DelHisAllLla").click(function(){
		swal({
			title: "¿Está seguro?",
		  	text: "¡Está apunto de eliminar todo el historial de llamadas!",
		  	type: "warning",
		  	showCancelButton: true,
		  	confirmButtonText: "Si, Eliminar Todo",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true,
			showLoaderOnConfirm: true
		},function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Historial llamadas eliminado!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						url: "pages/ajax/eliminarhisllamadas.php"
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open("./llamadas/historial", "_parent");
				});
			}
		});
	});
	
	// Buscador Llamadas
	$("#Schllamadas > li > a").removeAttr("href").css("cursor", "pointer");
	$("#ShowSchTomadorllamadas, #ShowSchDiallamadas, #ShowSchQuienllamadas, #ShowSchConllamadas").hide();
	$("#Schllamadas > li > a").click(function(){
		var DataSchType = $(this).data("type");
		if(DataSchType === "tomador"){
			$("#TxtllamadasSch").text("Tomador");
			$("#ShowSchDefaultllamadas, #ShowSchDiallamadas, #ShowSchQuienllamadas, #ShowSchConllamadas").hide();
			$("#ShowSchTomadorllamadas").show();
			$("#BtnSchllamadas").click(function(){
				window.open("llamadas/buscar/?type=" + DataSchType + "&sch=" + $("#TomadorSchllamadas").val(), "_parent");
			});
		}
		if(DataSchType === "dia"){
			$("#TxtllamadasSch").text("Día");
			$("#ShowSchDefaultllamadas, #ShowSchTomadorllamadas, #ShowSchQuienllamadas, #ShowSchConllamadas").hide();
			$("#ShowSchDiallamadas").show();
			$("#BtnSchllamadas").click(function(){
				window.open("llamadas/buscar/?type=" + DataSchType + "&sch=" + $("#DiaSchllamadas").val(), "_parent");
			});
		}
		if(DataSchType === "quien"){
			$("#TxtllamadasSch").text("Quién");
			$("#ShowSchDefaultllamadas, #ShowSchTomadorllamadas, #ShowSchDiallamadas, #ShowSchConllamadas").hide();
			$("#ShowSchQuienllamadas").show();
			$("#BtnSchllamadas").click(function(){
				window.open("llamadas/buscar/?type=" + DataSchType + "&sch=" + $("#QuienSchllamadas").val(), "_parent");
			});
		}
		if(DataSchType === "con"){
			$("#TxtllamadasSch").text("Hablar con");
			$("#ShowSchDefaultllamadas, #ShowSchTomadorllamadas, #ShowSchDiallamadas, #ShowSchQuienllamadas").hide();
			$("#ShowSchConllamadas").show();
			$("#BtnSchllamadas").click(function(){
				window.open("llamadas/buscar/?type=" + DataSchType + "&sch=" + $("#ConSchllamadas").val(), "_parent");
			});
		}
	});
	
	// Buscador Pedidos
	$("#Schpedidos > li > a").removeAttr("href").css("cursor", "pointer");
	$("#ShowSchFechapedidos, #ShowSchTomadorpedidos, #ShowSchClientepedidos, #ShowSchPedidopedidos").hide();
	$("#Schpedidos > li > a").click(function(){
		var DataSchType = $(this).data("type");
		if(DataSchType === "fecha"){
			$("#ShowSchDefaultpedidos, #ShowSchTomadorpedidos, #ShowSchClientepedidos, #ShowSchPedidopedidos").hide();
			$("#ShowSchFechapedidos").show();
			$("#BtnSchpedidos").click(function(){
				window.open("pedidos/buscar/?type=" + DataSchType + "&sch=" + $("#FechaSchpedidos").val(), "_parent");
			});
		}
		if(DataSchType === "tomador"){
			$("#ShowSchDefaultpedidos, #ShowSchFechapedidos, #ShowSchClientepedidos, #ShowSchPedidopedidos").hide();
			$("#ShowSchTomadorpedidos").show();
			$("#BtnSchpedidos").click(function(){
				window.open("pedidos/buscar/?type=" + DataSchType + "&sch=" + $("#TomSchpedidos").val(), "_parent");
			});
		}
		if(DataSchType === "cliente"){
			$("#ShowSchDefaultpedidos, #ShowSchFechapedidos, #ShowSchTomadorpedidos, #ShowSchPedidopedidos").hide();
			$("#ShowSchClientepedidos").show();
			$("#BtnSchpedidos").click(function(){
				window.open("pedidos/buscar/?type=" + DataSchType + "&sch=" + $("#CliSchpedidos").val(), "_parent");
			});
		}
		if(DataSchType === "pedido"){
			$("#ShowSchDefaultpedidos, #ShowSchFechapedidos, #ShowSchTomadorpedidos, #ShowSchClientepedidos").hide();
			$("#ShowSchPedidopedidos").show();
			$("#BtnSchpedidos").click(function(){
				window.open("pedidos/buscar/?type=" + DataSchType + "&sch=" + $("#PedidoSchpedidos").val(), "_parent");
			});
		}
	});
	
	// Añadir Pedidos
	var PedidoError;
	var PedFecha = $("#pedfecha");
	var PedHora = $("#pedhora");
	var PedMinutos = $("#pedminutos");
	var PedTomador = $("#pedtomador");
	var PedCliente = $("#pedcliente");
	var PedMaterial = $("#pedmaterial");
	var PedPedido = $("#pedpedido");
	var PedStock = $("#pedstock");
	var PedAvisado = $("#pedavisado");
	var PedAvisadoSel;
	var PedFinalizado = $("#pedfinalizado");
	var PedFinPor = $("#pedfin_por");
	$("#frmApedidos input:radio[name=avisado]").change(function(){
		if($(this).val() === "1"){
			PedAvisadoSel = 1;
			$("#show_finped").show();
		}else{
			PedAvisadoSel = 0;
			$("#show_finped").hide();
		}
	});
	$("#BtnAPedido").click(function(){
		if(PedFecha.val().length <= 0 || PedHora.val().length <= 0 || PedMinutos.val().length <= 0){
			$("#frmApedidos label[for=fecha]").css("color", "#f94442");
			PedidoError = 1;
		}else{
			$("#frmApedidos label[for=fecha]").css("color", "#333333");
			PedidoError = 0;
		}
		if(PedTomador.val().length <= 0){
			$("#frmApedidos label[for=tomador]").css("color", "#f94442");
			PedidoError = 1;
		}else{
			$("#frmApedidos label[for=tomador]").css("color", "#333333");
			PedidoError = 0;
		}
		if(PedCliente.val().length <= 0){
			$("#frmApedidos label[for=cliente]").css("color", "#f94442");
			PedidoError = 1;
		}else{
			$("#frmApedidos label[for=cliente]").css("color", "#333333");
			PedidoError = 0;
		}
		if(PedMaterial.val().length <= 0){
			$("#frmApedidos label[for=material]").css("color", "#f94442");
			PedidoError = 1;
		}else{
			$("#frmApedidos label[for=material]").css("color", "#333333");
			PedidoError = 0;
		}
		if(PedAvisadoSel > 0){
			PedAvisado = 1;
		}else{
			PedAvisado = 0;
		}
		if(PedidoError){
			swal({
				title: "¡Error al añadir el pedido!",
				text: "Corrije los campos marcados en rojo",
				type: "error",
				confirmButtonText: "Cerrar"
			});
		}else{
			$.ajax({
				type: "POST",
				url: "pages/ajax/subirpedido.php",
				data: {
					pedfecha: PedFecha.val(),
					pedhora: PedHora.val(),
					pedminutos: PedMinutos.val(),
					pedtomador: PedTomador.val(),
					pedcliente: PedCliente.val(),
					pedmaterial: PedMaterial.val(),
					pedpedido: PedPedido.val(),
					pedstock: PedStock.val(),
					pedavisado: PedAvisado,
					pedfinalizado: PedFinalizado.val(),
					pedfin_por: PedFinPor.val()
				}
			});
			swal({
				title: "¡Pedido añadido!",
				type: "success",
				confirmButtonText: "Cerrar"
			});
			$("button.confirm").click(function(){
				window.open("./pedidos", "_parent");
			});
		}
	});
	
	// Ver Pedido (modal)
	$(".VerPedido").click(function(){
		var VerPedID = $(this).data("id");
		$("#ShowVerPedido").modal("show");
		$("#IDPedVer").text(VerPedID);
		$.ajax({
			type: "POST",
			url: "pages/ajax/verpedido.php",
			data: {
				pedvid: VerPedID
			},
			success: function(data){
				$("#ResultVerPed").html(data);
			}
		});
	});
	
	// Finalizar pedido (modal)
	$(".FinalizarPed").click(function(){
		var FinPedID = $(this).data("id");
		$("#ShowFinPedido").modal("show");
		$("#NpedFinModal").text(FinPedID);
		$("#BtnFPedido").click(function(){
			if($("#pedfinfinalizado").val() <= 0){
				swal({
					title: "¡Error al finalizar el pedido!",
					text: "La fecha de finalización no puede estar vacia",
					type: "error",
					confirmButtonText: "Cerrar"
			});
			}else{
				$.ajax({
					type: "POST",
					url: "pages/ajax/finalizarpedido.php",
					data: {
						pedfinid: FinPedID,
						pedfinfinalizado: $("#pedfinfinalizado").val(),
						pedfinfinpor: $("#pedfinfin_por").val()
					}
				});
				swal({
					title: "¡Pedido finalizado!",
					type: "success",
					confirmButtonText: "Cerrar"
				});
				$("button.confirm").click(function(){
					window.open("./pedidos", "_parent");
				});
			}
		});
	});
	
	// Editar Pedido
	var PedidoEdtError, PedEdtAvisadoSel;
	var PedEdtID = $("#pededtid");
	var PedEdtFecha = $("#pededtfecha");
	var PedEdtHora = $("#pededthora");
	var PedEdtMinutos = $("#pededtminutos");
	var PedEdtTomador = $("#pededttomador");
	var PedEdtCliente = $("#pededtcliente");
	var PedEdtMaterial = $("#pededtmaterial");
	var PedEdtPedido = $("#pededtpedido");
	var PedEdtStock = $("#pededtstock");
	var PedEdtAvisado = $("#pededtavisado");
	var PedEdtFinalizado = $("#pededtfinalizado");
	var PedEdtFinPor = $("#pededtfin_por");
	if($("#pededtavisadoSi").is(":checked")){
		$("#show_edtfinped").show();
	}else if($("#pededtavisadoNo").is(":checked")){
		$("#show_edtfinped").hide();
	}
	$("#frmEdtpedidos input:radio[name=pededtavisado]").change(function(){
		if($(this).val() === "1"){
			PedEdtAvisadoSel = 1;
			$("#show_edtfinped").show();
		}else if($(this).val() === "0"){
			PedEdtAvisadoSel = 0;
			$("#show_edtfinped").hide();
		}
	});
	$("#BtnEdtPedido").click(function(){
		if(PedEdtFecha.val().length <= 0){
			$("#frmEdtpedidos label[for=fecha]").css("color", "#f94442");
			PedidoEdtError = 1;
		}else{
			$("#frmEdtpedidos label[for=fecha]").css("color", "#333333");
			PedidoEdtError = 0;
		}
		if(PedEdtCliente.val().length <= 0){
			$("#frmEdtpedidos label[for=cliente]").css("color", "#f94442");
			PedidoEdtError = 1;
		}else{
			$("#frmEdtpedidos label[for=cliente]").css("color", "#333333");
			PedidoEdtError = 0;
		}
		if(PedEdtMaterial.val().length <= 0){
			$("#frmEdtpedidos label[for=material]").css("color", "#f94442");
			PedidoEdtError = 1;
		}else{
			$("#frmEdtpedidos label[for=material]").css("color", "#333333");
			PedidoEdtError = 0;
		}
		if(PedEdtAvisadoSel > 0){
			PedEdtAvisado = 1;
		}else{
			PedEdtAvisado = 0;
		}
		if(PedidoEdtError){
			swal({
				title: "¡Pedido no guardado!",
				text: "Corrige los errores marcados en rojo",
				type: "error",
				confirmButtonText: "Cerrar"
			});
		}else{
			$.ajax({
				type: "POST",
				url: "pages/ajax/editarpedido.php",
				data: {
					pededtid: PedEdtID.val(),
					pededtfecha: PedEdtFecha.val(),
					pededthora: PedEdtHora.val(),
					pededtminutos: PedEdtMinutos.val(),
					pededttomador: PedEdtTomador.val(),
					pededtcliente: PedEdtCliente.val(),
					pededtmaterial: PedEdtMaterial.val(),
					pededtpedido: PedEdtPedido.val(),
					pededtstock: PedEdtStock.val(),
					pededtavisado: PedEdtAvisado,
					pededtfinalizado: PedEdtFinalizado.val(),
					pededtfinpor: PedEdtFinPor.val()
				}
			});
			swal({
				title: "¡Pedido guardado!",
				type: "success",
				confirmButtonText: "Cerrar"
			});
			$("button.confirm").click(function(){
				window.open("./pedidos", "_parent");
			});
		}
	});
	
	// Eliminar Pedido
	$(".DelPedido").click(function(){
		var DelID = $(this).data("id");
		swal({
			title: "¿Está seguro?",
		  	text: "¡Estás apunto de eliminar el pedido nº " + DelID + "!",		  	type: "warning",
		  	showCancelButton: true,
			showLoaderOnConfirm: true,		  	confirmButtonColor: "#DD6B55",
		  	confirmButtonText: "Si, Eliminar",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: false
		},
		function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Pedido eliminado!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						type: "POST",
						url: "pages/ajax/eliminarpedido.php",
						data: {
							delpedid: DelID
						}
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open($(location).attr("href"), "_parent");
				});
			}else{
				swal({
					title: "¡Pedido no eliminado!",
					type: "error",
		  			confirmButtonText: "Cerrar"
				});
			}
		});
	});
	
	// Restaurar Pedidos
	$(".NoPedido").click(function(){
		var ResetID = $(this).data("id");
		swal({
			title: "¿Restaurar pedido?",
			text: "Pedido nº: " + ResetID,
			type: "info",
			showCancelButton: true,
			confirmButtonText: "Si, Restaurar Pedido",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
			closeOnCancel: true,
			showLoaderOnConfirm: true
		}, function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Pedido Restaurado!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						type: "POST",
						url: "pages/ajax/restaurarhispedidos.php",
						data: { pedidoid: ResetID }
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open("./pedidos/", "_parent");
				});
			}
		});
	});
	
	// Eliminar Todos Pedidos Finalizados
	$("#DelHisAllpedido").click(function(){
		swal({
			title: "¿Está seguro?",
		  	text: "¡Está apunto de eliminar todo el historial de pedidos!",
		  	type: "warning",
		  	showCancelButton: true,
		  	confirmButtonText: "Si, Eliminar Todo",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true,
			showLoaderOnConfirm: true
		},function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Historial pedidos eliminado!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						url: "pages/ajax/eliminarhispedidos.php"
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open("./pedidos/historial", "_parent");
				});
			}
		});
	});
	
	// Agregar tecnico
	var TecError;
	var TecNombre = $("#nombre");
	var TecDNI = $("#dni");
	var TecEmail = $("#email");
	var TecPassword = $("#password");
	var TecAdmSel = "";
	var TecPermisosSel = "";
	TecDNI.numeric();
	$("#frmAtecnicos input:radio").change(function(){
		if($(this).val() === "Si"){
			TecAdmSel = 1;
		}else if($(this).val() === "No"){
			TecAdmSel = 0;
		}
	});
	$("#btnPrSelAll").on("change", function(){
		if($(this).is(":checked")){
			$("#frmAtecnicos input[type=checkbox]").attr("checked", true);
			$("#frmAtecnicos label.checkbox").addClass("active");
		}else{
			$("#frmAtecnicos input[type=checkbox]").attr("checked", false);
			$("#frmAtecnicos label.checkbox").removeClass("active");
		}
	});
	$("#BtnAtecnico").click(function(){
		$("#frmAtecnicos input[type=checkbox]").each(function(){
			if(this.checked){
                TecPermisosSel += $(this).val() + ",";
            }
		});
		if(TecNombre.val().length <= 0){
			$("#frmAtecnicos label[for=nombre]").css("color", "#f94442");
			TecError = 1;
		}else{
			$("#frmAtecnicos label[for=nombre]").css("color", "#333333");
			TecError = 0;
		}
		if(TecDNI.val().length <= 0){
			$("#frmAtecnicos label[for=dni]").css("color", "#f94442");
			TecError = 1;
		}else{
			$("#frmAtecnicos label[for=dni]").css("color", "#333333");
			TecError = 0;
		}
		if(TecEmail.val().length <= 0){
			$("#frmAtecnicos label[for=email]").css("color", "#f94442");
			TecError = 1;
		}else{
			$("#frmAtecnicos label[for=email]").css("color", "#333333");
			TecError = 0;
		}
		if(TecAdmSel === ""){
			$("#frmAtecnicos label[for=admin]").css("color", "#f94442");
			TecError = 1;
		}else{
			$("#frmAtecnicos label[for=admin]").css("color", "#333333");
			TecError = 0;
		}
		if(TecPermisosSel === ""){
			$("#frmAtecnicos label[for=permisos]").css("color", "#f94442");
			TecError = 1;
		}else{
			$("#frmAtecnicos label[for=permisos]").css("color", "#333333");
			TecError = 0;
		}
		if(TecError){
			swal({
				title: "¡Error al añadir el técnico!",
				text: "Corrija los errores marcados en rojo",
				type: "error",
				confirmButtonText: "Cerrar"
			});
		}else{
			$.ajax({
				type: "POST",
				url: "pages/ajax/subirtecnico.php",
				data: {
					nombre: TecNombre.val(),
					dni: TecDNI.val(),
					email: TecEmail.val(),
					password: TecPassword.val(),
					admin: TecAdmSel,
					permisos: TecPermisosSel.replace(/^,|,$/g, ""),
				}
			});
			--TecPermisosSel;
			swal({
				title: "¡Técnico añadido!",
				type: "success",
				confirmButtonText: "Cerrar"
			});
			$("button.confirm").click(function(){
				window.open("./tecnicos", "_parent");
			});
		}
	});
	
	// Editar Tecnicos
	/*var EdtTecID = $("#edtid");
	var EdtTecNombre = $("#edtnombre");
	var EdtTecDNI = $("#edtdni");
	var EdtTecEmail = $("#edtemail");
	var EdtTecPass = $("#edtpassword");
	var EdtTecAdmin = $("#edtadmsel").val();
	var EdtTecPermisos = "";
	$("#frmEdtTecnico input:radio").change(function(){
		if($(this).val() === "Si"){
			EdtTecAdmin = 1;
		}else{
			EdtTecAdmin = 0;
		}
	});
	$("#btnEdtPrSelAll").on("change", function(){
		if($(this).is(":checked")){
			$("#frmEdttecnicos input[type=checkbox]").attr("checked", true);
			$("#frmEdttecnicos label.checkbox").addClass("active");
		}else{
			$("#frmEdttecnicos input[type=checkbox]").attr("checked", false);
			$("#frmEdttecnicos label.checkbox").removeClass("active");
		}
	});
	$("#BtnEdttecnico").click(function(){
		$("#frmEdttecnicos input[type=checkbox]").each(function(){
			if(this.checked){
                EdtTecPermisos += $(this).val() + ",";
            }
		});
		$.ajax({
			type: "POST",
			url: "pages/ajax/editartecnico.php",
			data: {
				edtid: EdtTecID.val(),
				edtnombre: EdtTecNombre.val(),
				edtdni: EdtTecDNI.val(),
				edtemail: EdtTecEmail.val(),
				edtpermisos: EdtTecPermisos.replace(/^,|,$/g, ""),
				edtadmin: EdtTecAdmin
			}
		});
		swal({
			title: "¡Técnico modificado!",
			type: "success",
			confirmButtonText: "Cerrar"
		});
		--EdtTecPermisos;
		$("button.confirm").click(function(){
			window.open("./tecnicos", "_parent");
		});
	});*/
	
	// Eliminar Tecnicos
	$(".DelTec").click(function(){
		var DelID = $(this).data("id");
		var DelName = $(this).data("name");
		swal({
			title: "¿Está seguro?",
		  	text: "¡Estás apunto de eliminar al técnico " + DelName + "!",
		  	type: "warning",
		  	showCancelButton: true,
			showLoaderOnConfirm: true,
		  	confirmButtonText: "Si, Eliminar",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true
		},
		function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Técnico Eliminado!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						type: "POST",
						url: "pages/ajax/eliminartecnico.php",
						data: {
							deltecid: DelID
						}
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open("./tecnicos", "_parent");
				});
			}
		});
	});
	
	// Agregar Clientes
	var CliError;
	var CliNombre = $("#clinombre");
	var CliUsername = $("#cliusername");
	var CliEmail = $("#cliemail");
	var CliPassword = $("#clipassword");
	$("#clinombre").autocomplete({
			source: 'pages/ajax/autocompleteclientes.php'
		});
	setInterval(function(){
		if(CliPassword.val().length > 0){
			$(".CliVerPassGen").show();
		}else{
			$(".CliVerPassGen").hide();
		}
	}, 0);
	$(".CliVerPassGen").mousedown(function(event){
		event.preventDefault();
		$("i", this).removeClass("glyphicon-eye-open").addClass("glyphicon-eye-close");
		CliPassword.attr("type", "text");
	}).click(function(event){
		event.preventDefault();
		$("i", this).removeClass("glyphicon-eye-close").addClass("glyphicon-eye-open");
		CliPassword.attr("type", "password");
	});
	$(".BtnGenPassCli").click(function(event){
		event.preventDefault();
		CliPassword.val(genPass());
	});
	$("#BtnAclientes").click(function(){
		if(CliNombre.val() === ""){
			$("#frmAclientes label[for=nombre]").css("color", "#f94442");
			CliError = 1;
		}else{
			$("#frmAclientes label[for=nombre]").css("color", "#333333");
			CliError = 0;
		}
		if(CliUsername.val() === ""){
			$("#frmAclientes label[for=username]").css("color", "#f94442");
			CliError = 1;
		}else{
			$("#frmAclientes label[for=username]").css("color", "#333333");
			CliError = 0;
		}
		if(CliPassword.val() === ""){
			$("#frmAclientes label[for=password]").css("color", "#f94442");
			CliError = 1;
		}else{
			$("#frmAclientes label[for=password]").css("color", "#333333");
			CliError = 0;
		}
		if(CliError){
			swal({
				title: "¡Error al añadir al cliente!",
				text: "Corrija los errores marcados en rojo",
				type: "error",
				confirmButtonText: "Cerrar"
			});
		}else{
			$.ajax({
				type: "POST",
				url: "pages/ajax/subircliente.php",
				data: {
					clinombre: CliNombre.val(),
					cliusername: CliUsername.val(),
					cliemail: CliEmail.val(),
					clipassword: CliPassword.val()
				}
			});
			swal({
				title: "¡Cliente añadido!",
				type: "success",
				confirmButtonText: "Cerrar"
			});
			$("button.confirm").click(function(){
				window.open("./clientes", "_parent");
			});
		}
	});
	
	// Editar Clientes
	var CliEditError;
	var CliEditID = $("#clieditid");
	var CliEditNombre = $("#clieditnombre");
	var CliEditUsername = $("#clieditusername");
	var CliEditEmail = $("#clieditemail");
	var CliEditPassword = $("#clieditpassword");
	setInterval(function(){
		if(CliEditPassword.val().length > 0){
			$(".CliEditVerPassGen").show();
		}else{
			$(".CliEditVerPassGen").hide();
		}
	}, 0);
	$(".CliEditVerPassGen").mousedown(function(event){
		event.preventDefault();
		$("i", this).removeClass("glyphicon-eye-open").addClass("glyphicon-eye-close");
		CliEditPassword.attr("type", "text");
	}).click(function(event){
		event.preventDefault();
		$("i", this).removeClass("glyphicon-eye-close").addClass("glyphicon-eye-open");
		CliEditPassword.attr("type", "password");
	});
	$(".BtnGenPassCliEdit").click(function(event){
		event.preventDefault();
		CliEditPassword.val(genPass());
	});
	$("#BtnEditclientes").click(function(){
		if(CliEditNombre.val() === ""){
			$("#frmEditclientes label[for=nombre]").css("color", "#f94442");
			CliEditError = 1;
		}else{
			$("#frmEditclientes label[for=nombre]").css("color", "#333333");
			CliEditError = 0;
		}
		if(CliEditUsername.val() === ""){
			$("#frmEditclientes label[for=username]").css("color", "#f94442");
			CliEditError = 1;
		}else{
			$("#frmEditclientes label[for=username]").css("color", "#333333");
			CliEditError = 0;
		}
		if(CliEditError){
			swal({
				title: "¡Error al guardar los cambios!",
				text: "Corrija los errores marcados en rojo",
				type: "error",
				confirmButtonText: "Cerrar"
			});
		}else{
			$.ajax({
				type: "POST",
				url: "pages/ajax/editarcliente.php",
				data: {
					clieditid: CliEditID.val(),
					clieditnombre: CliEditNombre.val(),
					clieditusername: CliEditUsername.val(),
					clieditemail: CliEditEmail.val(),
					clieditpassword: CliEditPassword.val()
				}
			});
			swal({
				title: "¡Los cambios se han guardado!",
				type: "success",
				confirmButtonText: "Cerrar"
			});
			$("button.confirm").click(function(){
				window.open("./clientes", "_parent");
			});
		}
	});
	
	// Eliminar Clientes
	$(".DelCli").click(function(){
		var DelID = $(this).data("id");
		var DelName = $(this).data("name");
		swal({
			title: "¿Está seguro?",
		  	text: "¡Estás apunto de eliminar al cliente " + DelName + "!",
		  	type: "warning",
		  	showCancelButton: true,
			showLoaderOnConfirm: true,
		  	confirmButtonText: "Si, Eliminar",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true
		},
		function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Cliente Eliminado!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						type: "POST",
						url: "pages/ajax/eliminarcliente.php",
						data: {
							delcliid: DelID
						}
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open("./clientes", "_parent");
				});
			}
		});
	});
	
	// Buscador SAT Paguera
	$("#SchSpaguera > li > a").removeAttr("href").css("cursor", "pointer");
	$("#ShowSchNordenSpaguera, #ShowSchTomadorSpaguera, #ShowSchClienteSpaguera, #ShowSchFentradaSpaguera, #ShowSchFrecogidaSpaguera").hide();
	$("#SchSpaguera > li > a").click(function(){
		var DataSchType = $(this).data("type");
		if(DataSchType === "n_orden"){
			$("#NordSchSpaguera").numeric();
			$("#TxtSpagueraSch").text("Nº Orden");
			$("#ShowSchDefaultSpaguera, #ShowSchTomadorSpaguera, #ShowSchClienteSpaguera, #ShowSchFentradaInc, #ShowSchFrecogidaSpaguera").hide();
			$("#ShowSchNordenSpaguera").show();
			$("#BtnSchSpaguera").click(function(){
				window.open("satpaguera/buscar/?type=" + DataSchType + "&sch=" + $("#NordSchSpaguera").val(), "_parent");
			});
		}else{
			$("#TxtSpagueraSch").text(DataSchType.charAt(0).toUpperCase() + DataSchType.slice(1));
		}
		if(DataSchType === "tomador"){
			$("#ShowSchDefaultSpaguera, #ShowSchNordenSpaguera, #ShowSchClienteSpaguera, #ShowSchFentradaSpaguera, #ShowSchFrecogidaSpaguera").hide();
			$("#ShowSchTomadorSpaguera").show();
			$("#BtnSchSpaguera").click(function(){
				window.open("satpaguera/buscar/?type=" + DataSchType + "&sch=" + $("#TomSchSpaguera").val(), "_parent");
			});
		}
		if(DataSchType === "cliente"){
			$("#ShowSchDefaultSpaguera, #ShowSchNordenSpaguera, #ShowSchTomadorSpaguera, #ShowSchFentradaSpaguera, #ShowSchFrecogidaSpaguera").hide();
			$("#ShowSchClienteSpaguera").show();
			$("#BtnSchSpaguera").click(function(){
				window.open("satpaguera/buscar/?type=" + DataSchType + "&sch=" + $("#CliSchSpaguera").val(), "_parent");
			});
		}
		if(DataSchType === "f_entrada"){
			$("#TxtSpagueraSch").text("Fecha Entrada");
			$("#ShowSchDefaultSpaguera, #ShowSchTomadorSpaguera, #ShowSchClienteSpaguera, #ShowSchFrecogidaSpaguera").hide();
			$("#ShowSchFentradaSpaguera").show();
			$("#BtnSchSpaguera").click(function(){
				window.open("satpaguera/buscar/?type=" + DataSchType + "&sch=" + $("#FentSchSpaguera").val(), "_parent");
			});
		}
		if(DataSchType === "f_recogida"){
			$("#TxtSpagueraSch").text("Fecha Recogida");
			$("#ShowSchDefaultSpaguera, #ShowSchTomadorSpaguera, #ShowSchClienteSpaguera, #ShowSchFentradaSpaguera").hide();
			$("#ShowSchFrecogidaSpaguera").show();
			$("#BtnSchSpaguera").click(function(){
				window.open("satpaguera/buscar/?type=" + DataSchType + "&sch=" + $("#Frecog1SchSpaguera").val() + "-" + $("#Frecog2SchSpaguera").val(), "_parent");
			});
		}
	});
	
	// Buscador SAT Santa Ponsa
	$("#SchSponsa > li > a").removeAttr("href").css("cursor", "pointer");
	$("#ShowSchNordenSponsa, #ShowSchTomadorSponsa, #ShowSchClienteSponsa, #ShowSchFentradaSponsa, #ShowSchFrecogidaSponsa").hide();
	$("#SchSponsa > li > a").click(function(){
		var DataSchType = $(this).data("type");
		if(DataSchType === "n_orden"){
			$("#NordSchSponsa").numeric();
			$("#TxtSponsaSch").text("Nº Orden");
			$("#ShowSchDefaultSponsa, #ShowSchTomadorSponsa, #ShowSchClienteSponsa, #ShowSchFentradaSponsa, #ShowSchFrecogidaSponsa").hide();
			$("#ShowSchNordenSponsa").show();
			$("#BtnSchSponsa").click(function(){
				window.open("satponsa/buscar/?type=" + DataSchType + "&sch=" + $("#NordSchSponsa").val(), "_parent");
			});
		}else{
			$("#TxtSponsaSch").text(DataSchType.charAt(0).toUpperCase() + DataSchType.slice(1));
		}
		if(DataSchType === "tomador"){
			$("#ShowSchDefaultSponsa, #ShowSchNordenSponsa, #ShowSchClienteSponsa, #ShowSchFentradaSponsa, #ShowSchFrecogidaSponsa").hide();
			$("#ShowSchTomadorSponsa").show();
			$("#BtnSchSponsa").click(function(){
				window.open("satponsa/buscar/?type=" + DataSchType + "&sch=" + $("#TomSchSponsa").val(), "_parent");
			});
		}
		if(DataSchType === "cliente"){
			$("#ShowSchDefaultSponsa, #ShowSchNordenSponsa, #ShowSchTomadorSponsa, #ShowSchFentradaSponsa, #ShowSchFrecogidaSponsa").hide();
			$("#ShowSchClienteSponsa").show();
			$("#BtnSchSponsa").click(function(){
				window.open("satponsa/buscar/?type=" + DataSchType + "&sch=" + $("#CliSchSponsa").val(), "_parent");
			});
		}
		if(DataSchType === "f_entrada"){
			$("#TxtSponsaSch").text("Fecha Entrada");
			$("#ShowSchDefaultSponsa, #ShowSchTomadorSponsa, #ShowSchClienteSponsa").hide();
			$("#ShowSchFentradaSponsa").show();
			$("#BtnSchSponsa").click(function(){
				window.open("satponsa/buscar/?type=" + DataSchType + "&sch=" + $("#FentSchSponsa").val(), "_parent");
			});
		}
		if(DataSchType === "f_recogida"){
			$("#TxtSponsaSch").text("Fecha Recogida");
			$("#ShowSchDefaultSponsa, #ShowSchTomadorSponsa, #ShowSchClienteSponsa, #ShowSchFentradaSponsa").hide();
			$("#ShowSchFrecogidaSponsa").show();
			$("#BtnSchSponsa").click(function(){
				window.open("satponsa/buscar/?type=" + DataSchType + "&sch=" + $("#Frecog1SchSponsa").val() + "-" + $("#Frecog2SchSponsa").val(), "_parent");
			});
		}
	});
	
	// Añadir SAT Paguera
	var SatPError;
	var SatPTomador = $("#tomador");
	var SatPCliente = $("#cliente");
	var SatPNorden  = $("#n_orden");
	var SatPFentrada = $("#f_entrada");
	var SatPDescripcion = $("#descripcion");
	$("#BtnASatPaguera").click(function(){
		if(SatPCliente.val().length <= 0){
			$("#frmASatPaguera label[for=cliente]").css("color", "#f94442");
			SatPError = 1;
		}else{
			$("#frmASatPaguera label[for=cliente]").css("color", "#333333");
			SatPError = 0;
		}
		if(SatPFentrada.val().length <= 0){
			$("#frmASatPaguera label[for=fecha]").css("color", "#f94442");
			SatPError = 1;
		}else{
			$("#frmASatPaguera label[for=fecha]").css("color", "#333333");
			SatPError = 0;
		}
		if(SatPError){
			swal({
				title: "¡Error al añadir el SAT!",
				text: "Corrija los errores marcados en rojo",
				type: "error",
				confirmButtonText: "Cerrar"
			});
		}else{
			$.ajax({
				type: "POST",
				url: "pages/ajax/subirsatpaguera.php",
				data: {
					n_orden: SatPNorden.val(),
					tomador: SatPTomador.val(),
					cliente: SatPCliente.val(),
					f_entrada: SatPFentrada.val(),
					descripcion: SatPDescripcion.val()
				}
			});
			swal({
				title: "¡SAT añadido!",
				type: "success",
				confirmButtonText: "Cerrar"
			});
			$("button.confirm").click(function(){
				window.open("./satpaguera", "_parent");
			});
		}
	});
	
	// Añadir SAT Santa Ponsa
	var SatSPError;
	var SatSPTomador = $("#sptomador");
	var SatSPCliente = $("#spcliente");
	var SatSPNorden  = $("#spn_orden");
	var SatSPFentrada = $("#spf_entrada");
	var SatSPDescripcion = $("#spdescripcion");
	SatSPNorden.numeric();
	$("#BtnASatSPonsa").click(function(){
		if(SatSPCliente.val().length <= 0){
			$("#frmASatponsa label[for=cliente]").css("color", "#f94442");
			SatSPError = 1;
		}else{
			$("#frmASatponsa label[for=cliente]").css("color", "#333333");
			SatSPError = 0;
		}
		if(SatSPFentrada.val().length <= 0){
			$("#frmASatponsa label[for=fecha]").css("color", "#f94442");
			SatSPError = 1;
		}else{
			$("#frmASatponsa label[for=fecha]").css("color", "#333333");
			SatSPError = 0;
		}
		if(SatSPError){
			swal({
				title: "¡Error al añadir el SAT!",
				text: "Corrija los errores marcados en rojo",
				type: "error",
				confirmButtonText: "Cerrar"
			});
		}else{
			$.ajax({
				type: "POST",
				url: "pages/ajax/subirsatponsa.php",
				data: {
					spn_orden: SatSPNorden.val(),
					sptomador: SatSPTomador.val(),
					spcliente: SatSPCliente.val(),
					spf_entrada: SatSPFentrada.val(),
					spdescripcion: SatSPDescripcion.val()
				}
			});
			swal({
				title: "¡SAT añadido!",
				type: "success",
				confirmButtonText: "Cerrar"
			});
			$("button.confirm").click(function(){
				window.open("./satponsa", "_parent");
			});
		}
	});
	
	// Editar SAT Paguera
	var SatPEdtError;
	var SatPEdtID = $("#edtid");
	var SatPEdtCliente = $("#edtcliente");
	var SatPEdtFentrada = $("#edtf_entrada");
	var SatPEdtDescripcion = $("#edtdescripcion");
	var SatPEdtFfin = $("#edtf_fin");
	var SatPEdtFaviso = $("#edtf_aviso");
	var SatPEdtFrecogida = $("#edtf_recogida");
	var SatpEdtSatFacturado = $("#edtsatfacturado");
	var SatpEdtNfactura = $("#edtnfactura");
	$("#nofacturado2").on("change", function(){
		if($(this).is(":checked")){
			$(SatpEdtSatFacturado).attr("disabled", true).val("/");
		}else{
			$(SatpEdtSatFacturado).attr("disabled", false).val("");
		}
	});
	$("#edtsatfacturado").numeric(".");
	$("#BtnEdtSatPaguera").click(function(){
		SatPEdtError = 0;
		if(SatPEdtError){
			swal({
				title: "¡Error al guardar SAT!",
				text: "Corrija los errores marcados en rojo",
				type: "error",
				confirmButtonText: "Cerrar"
			});
		}else{
			swal({
				title: "¡SAT Guardado!",
				type: "success",
				confirmButtonText: "Cerrar"
			});
			$.ajax({
				type: "POST",
				url: "pages/ajax/editarsatpaguera.php",
				data: {
					edtid: SatPEdtID.val(),
					edtcliente: SatPEdtCliente.val(),
					edtfentrada: SatPEdtFentrada.val(),
					edtdescripcion: SatPEdtDescripcion.val(),
					edtffin: SatPEdtFfin.val(),
					edtfaviso: SatPEdtFaviso.val(),
					edtfrecogida: SatPEdtFrecogida.val(),
					edtsatfacturado: SatpEdtSatFacturado.val(),
					edtnfactura: SatpEdtNfactura.val()
				}
			});
			$("button.confirm").click(function(){
				window.open(history.back(-1), "_parent");
			});
		}
	});
	
	// Editar SAT Santa Ponsa
	var SatSPEdtError;
	var SatSPEdtID = $("#spedtid");
	var SatSPEdtCliente = $("#spedtcliente");
	var SatSPEdtFentrada = $("#spf_entrada");
	var SatSPEdtDescripcion = $("#spedtdescripcion");
	var SatSPEdtFfin = $("#spedtf_fin");
	var SatSPEdtFaviso = $("#spedtf_aviso");
	var SatSPEdtFrecogida = $("#spedtf_recogida");
	var SatSPEdtSatFacturado = $("#spedtsatfacturado");
	var SatSPEdtNfactura = $("#spedtnfactura");
	$("#nofacturado").on("change", function(){
		if($(this).is(":checked")){
			$(SatSPEdtSatFacturado).attr("disabled", true).val("/");
		}else{
			$(SatSPEdtSatFacturado).attr("disabled", false).val("");
		}
	});
	$("#spedtsatfacturado").numeric(".");
	$("#BtnEdtSatSPonsa").click(function(){
		SatSPEdtError = 0;
		if(SatSPEdtError){
			swal({
				title: "¡Error al guardar SAT!",
				text: "Corrija los errores marcados en rojo",
				type: "error",
				confirmButtonText: "Cerrar"
			});
		}else{
			swal({
				title: "¡SAT guardado!",
				type: "success",
				confirmButtonText: "Cerrar"
			});
			$.ajax({
				type: "POST",
				url: "pages/ajax/editarsatponsa.php",
				data: {
					spedtid: SatSPEdtID.val(),
					spedtcliente: SatSPEdtCliente.val(),
					spedtfentrada: SatSPEdtFentrada.val(),
					spedtdescripcion: SatSPEdtDescripcion.val(),
					spedtffin: SatSPEdtFfin.val(),
					spedtfaviso: SatSPEdtFaviso.val(),
					spedtfrecogida: SatSPEdtFrecogida.val(),
					spedtsatfacturado: SatSPEdtSatFacturado.val(),
					spedtnfactura: SatSPEdtNfactura.val()
				}
			});
			$("button.confirm").click(function(){
				window.open(history.back(-1), "_parent");
			});
		}
	});
	
	// Asignar Recogida SAT Paguera
	$(".AsgSatPFrecogida").click(function(){
		var AsgSatPNorden = $(this).data("norden");
		var AsgSatPTomador = $("#asgfrtomador");
		var AsgSatPFrecogida = $("#asgf_recogida");
		$("#AlertEdtFrecogida, .sweet-overlay").css("display", "block");
		$("#AlertEdtFrecogida").removeClass("hidden").addClass("sweet-alert showSweetAlert visible");
		$("#CancelAsgSatP").click(function(){
			$(".sweet-overlay").css("display", "none");
			$("#AlertEdtFrecogida").removeClass("sweet-alert showSweetAlert visible").addClass("hidden");
			$("#AsgFrecginputError").removeClass("show");
		});
		$("#ConfirmAsgSatP").click(function(){
			$(".sweet-overlay").css("display", "none");
			$("#AlertEdtFrecogida").removeClass("sweet-alert showSweetAlert visible").addClass("hidden");
			if(AsgSatPTomador.val() === "" && AsgSatPFrecogida.val() === ""){
				$("#AsgFrecginputError").removeClass("hide").addClass("show");
				$("#AlertEdtFrecogida, .sweet-overlay").css("display", "block");
				$("#AlertEdtFrecogida").removeClass("hidden").addClass("sweet-alert showSweetAlert visible");
			}else{
				$("#AsgFrecginputError").removeClass("show").addClass("hide");
				swal({
					title: "¡Recogida asignada!",
					type: "success",
					confirmButtonText: "Cerrar"
				});
				$.ajax({
					type: "POST",
					url: "pages/ajax/asgfrecogidasatpaguera.php",
					data: {
						asgnorden: AsgSatPNorden,
						asgtomador: AsgSatPTomador.val(),
						asgfrecogida: AsgSatPFrecogida.val()
					}
				});
				$("button.confirm").click(function(){
					window.open("./satpaguera", "_parent");
				});
			}
		});
	});
	
	// Asignar Recogida SAT Santa Ponsa
	$(".AsgSatSPFrecogida").click(function(){
		var AsgSatSPNorden = $(this).data("norden");
		var AsgSatSPTomador = $("#spasgfrtomador");
		var AsgSatSPFrecogida = $("#spasgf_recogida");
		$("#AlertEdtSPFrecogida, .sweet-overlay").css("display", "block");
		$("#AlertEdtSPFrecogida").removeClass("hidden").addClass("sweet-alert showSweetAlert visible");
		$("#CancelAsgSatSP").click(function(){
			$(".sweet-overlay").css("display", "none");
			$("#AlertEdtSPFrecogida").removeClass("sweet-alert showSweetAlert visible").addClass("hidden");
			$("#AsgSPFrecginputError").removeClass("show");
		});
		$("#ConfirmAsgSatSP").click(function(){
			$(".sweet-overlay").css("display", "none");
			$("#AlertEdtSPFrecogida").removeClass("sweet-alert showSweetAlert visible").addClass("hidden");
			if(AsgSatSPTomador.val() === "" && AsgSatSPFrecogida.val() === ""){
				$("#AsgSPFrecginputError").addClass("show");
			}else{
				$("#AsgSPFrecginputError").removeClass("show");
				swal({
					title: "¡Recogida asignada!",
					type: "success",
					confirmButtonText: "Cerrar"
				});
				$.ajax({
					type: "POST",
					url: "pages/ajax/asgfrecogidasatponsa.php",
					data: {
						spasgnorden: AsgSatSPNorden,
						spasgtomador: AsgSatSPTomador.val(),
						spasgfrecogida: AsgSatSPFrecogida.val()
					}
				});
				$("button.confirm").click(function(){
					window.open("./satponsa", "_parent");
				});
			}
		});
	});
	
	// Eliminar SAT Paguera
	$(".DelSatP").click(function(){
		var DelID = $(this).data("id");
		swal({
			title: "¿Está seguro?",
		  	text: "¡Está apunto de eliminar un SAT de Paguera! <br> <strong>Nº Orden: " + DelID + "</strong>",
		  	type: "warning",
		  	showCancelButton: true,
			showLoaderOnConfirm: true,
		  	confirmButtonColor: "#DD6B55",
		  	confirmButtonText: "Si, Eliminar",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: false,
			html: true
		},
		function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡SAT eliminado!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						type: "POST",
						url: "pages/ajax/eliminarsatpaguera.php",
						data: {
							DsatPNorden: DelID
						}
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open("./satpaguera", "_parent");
				});
			}else{
				swal({
					title: "¡SAT no eliminado!",
					type: "error",
		  			confirmButtonText: "Cerrar"
				});
			}
		});
	});
	
	// Eliminar SAT Santa Ponsa
	$(".DelSatSP").click(function(){
		var DelID = $(this).data("id");
		swal({
			title: "¿Está seguro?",
		  	text: "¡Está apunto de eliminar un SAT de Santa Ponsa! <br> <strong>Nº Orden: " + DelID + "</strong>",
		  	type: "warning",
		  	showCancelButton: true,
			showLoaderOnConfirm: true,
		  	confirmButtonColor: "#DD6B55",
		  	confirmButtonText: "Si, Eliminar",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: false,
			html: true
		},
		function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡SAT eliminado!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						type: "POST",
						url: "pages/ajax/eliminarsatponsa.php",
						data: {
							DsatSPNorden: DelID
						}
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open("./satponsa", "_parent");
				});
			}else{
				swal({
					title: "¡SAT no eliminado!",
					type: "error",
		  			confirmButtonText: "Cerrar"
				});
			}
		});
	});
	
	// Eliminar Todo Historial SAT Paguera
	$("#DelHisAllSatPaguera").click(function(){
		swal({
			title: "¿Está seguro?",
		  	text: "¡Está apunto de eliminar todo el historial de SAT Paguera!",
		  	type: "warning",
		  	showCancelButton: true,
		  	confirmButtonColor: "#DD6B55",
		  	confirmButtonText: "Si, Eliminar Todo",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true,
			showLoaderOnConfirm: true
		},function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Historial eliminado!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						url: "pages/ajax/eliminarhissatpaguera.php"
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open("./satpaguera/historial/", "_parent");
				});
			}
		});
	});
	
	// Eliminar Todo Historial SAT Santa Ponsa
	$("#DelHisAllsatponsa").click(function(){
		swal({
			title: "¿Está seguro?",
		  	text: "¡Está apunto de eliminar todo el historial de SAT Santa Ponsa!",
		  	type: "warning",
		  	showCancelButton: true,
		  	confirmButtonColor: "#DD6B55",
		  	confirmButtonText: "Si, Eliminar Todo",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: false,
			showLoaderOnConfirm: true
		},function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Historial eliminado!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						url: "pages/ajax/eliminarhissatponsa.php"
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open("./satponsa/historial/", "_parent");
				});
			}else{
				swal({
					title: "¡Cancelado!",
					type: "error",
					confirmButtonText: "Cerrar"
				});
			}
		});
	});
	
	// Buscador Salidas
	$("#SchSalidas > li > a").removeAttr("href").css("cursor", "pointer");
	$("#ShowSchFechaSalidas, #ShowSchTecnicoSalidas, #ShowSchClienteSalidas, #ShowSchFechaLlegadaSalidas, #ShowSchPIncidenciaSalidas, #ShowSchNFacturaSalidas").hide();
	$("#SchSalidas > li > a").click(function(){
		var DataSchType = $(this).data("type");
		if(DataSchType === "fecha"){
			$("#TxtSalidasSch").text("Fecha Ida");
			$("#ShowSchDefaultSalidas, #ShowSchTecnicoSalidas, #ShowSchClienteSalidas, #ShowSchFechaLlegadaSalidas, #ShowSchPIncidenciaSalidas, #ShowSchNFacturaSalidas").hide();
			$("#ShowSchFechaSalidas").show();
			$("#BtnSchSalidas").click(function(){
				window.open("salidas/buscar/?type=" + DataSchType + "&sch=" + $("#FechaSchSalidas").val(), "_parent");
			});
		}else{
			$("#TxtSalidasSch").text(DataSchType.charAt(0).toUpperCase() + DataSchType.slice(1));
		}
		if(DataSchType === "tecnico"){
			$("#TxtSalidasSch").text("Técnico");
			$("#ShowSchDefaultSalidas, #ShowSchFechaSalidas, #ShowSchClienteSalidas, #ShowSchFechaLlegadaSalidas, #ShowSchPIncidenciaSalidas, #ShowSchNFacturaSalidas").hide();
			$("#ShowSchTecnicoSalidas").show();
			$("#BtnSchSalidas").click(function(){
				window.open("salidas/buscar/?type=" + DataSchType + "&sch=" + $("#TecnicoSchSalidas").val(), "_parent");
			});
		}else{
			$("#TxtSalidasSch").text(DataSchType.charAt(0).toUpperCase() + DataSchType.slice(1));
		}
		if(DataSchType === "cliente"){
			$("#TxtSalidasSch").text("Cliente");
			$("#ShowSchDefaultSalidas, #ShowSchFechaSalidas, #ShowSchTecnicoSalidas, #ShowSchFechaLlegadaSalidas, #ShowSchPIncidenciaSalidas, #ShowSchNFacturaSalidas").hide();
			$("#ShowSchClienteSalidas").show();
			$("#BtnSchSalidas").click(function(){
				window.open("salidas/buscar/?type=" + DataSchType + "&sch=" + $("#ClienteSchSalidas").val(), "_parent");
			});
		}else{
			$("#TxtSalidasSch").text(DataSchType.charAt(0).toUpperCase() + DataSchType.slice(1));
		}
		if(DataSchType === "fechallegada"){
			$("#TxtSalidasSch").text("Fecha Llegada");
			$("#ShowSchDefaultSalidas, #ShowSchFechaSalidas, #ShowSchTecnicoSalidas, #ShowSchClienteSalidas, #ShowSchPIncidenciaSalidas, #ShowSchNFacturaSalidas").hide();
			$("#ShowSchFechaLlegadaSalidas").show();
			$("#BtnSchSalidas").click(function(){
				window.open("salidas/buscar/?type=" + DataSchType + "&sch=" + $("#FechaLlegadaSchSalidas").val(), "_parent");
			});
		}else{
			$("#TxtSalidasSch").text(DataSchType.charAt(0).toUpperCase() + DataSchType.slice(1));
		}
		if(DataSchType === "pincidencia"){
			$("#PIncidenciaSchSalidas").numeric();
			$("#TxtSalidasSch").text("Parte Incidencia");
			$("#ShowSchDefaultSalidas, #ShowSchFechaSalidas, #ShowSchTecnicoSalidas, #ShowSchClienteSalidas, #ShowSchFechaLlegadaSalidas, #ShowSchNFacturaSalidas").hide();
			$("#ShowSchPIncidenciaSalidas").show();
			$("#BtnSchSalidas").click(function(){
				window.open("salidas/buscar/?type=" + DataSchType + "&sch=" + $("#PIncidenciaSchSalidas").val(), "_parent");
			});
		}else{
			$("#TxtSalidasSch").text(DataSchType.charAt(0).toUpperCase() + DataSchType.slice(1));
		}
		if(DataSchType === "nfactura"){
			$("#PIncidenciaSchSalidas").numeric();
			$("#TxtSalidasSch").text("Nº Factura");
			$("#ShowSchDefaultSalidas, #ShowSchFechaSalidas, #ShowSchTecnicoSalidas, #ShowSchClienteSalidas, #ShowSchFechaLlegadaSalidas, #ShowSchPIncidenciaSalidas").hide();
			$("#ShowSchNFacturaSalidas").show();
			$("#BtnSchSalidas").click(function(){
				window.open("salidas/buscar/?type=" + DataSchType + "&sch=" + $("#NFacturaSchSalidas").val(), "_parent");
			});
		}else{
			$("#TxtSalidasSch").text(DataSchType.charAt(0).toUpperCase() + DataSchType.slice(1));
		}
	});
	
	// Añadir Salidas
	var SalidasError;
	var SalFecha = $("#salfecha");
	var SalHora = $("#salhora");
	var SalMinutos = $("#salminutos");
	var SalTecnico = $("#saltecnico");
	var SalCliente = $("#salcliente");
	var SalDescripcion = $("#saldescripcion");
	var SalFechaLlegada = $("#salfecha_llegada");
	var SalFechaLlegadaHora = $("#salfecha_llegada_hora");
	var SalFechaLlegadaMinutos = $("#salfecha_llegada_minutos");
	var SalParteIncidencias = $("#salp_incidencia");
	var SalParteComercial = true;
	var SalNFactura = $("#salnfactura");
	SalParteIncidencias.numeric();
	$("#salp_comercial").change(function(){
		if($(this).is(":checked")){
			$(SalParteIncidencias).attr("disabled", true);
			SalParteComercial = false;
		}else{
			$(SalParteIncidencias).attr("disabled", false);
			SalParteComercial = true;
		}
	});
	$("#BtnASalidas").click(function(){
		if(SalFecha.val().length <= 0 || SalHora.val().length <= 0 || SalMinutos.val().length <= 0){
			$("#frmASalidas label[for=fecha]").css("color", "#f94442");
			SalidasError = 1;
		}else{
			$("#frmASalidas label[for=fecha]").css("color", "#333333");
			SalidasError = 0;
		}
		if(SalCliente.val().length <= 0){
			$("#frmASalidas label[for=cliente]").css("color", "#f94442");
			SalidasError = 1;
		}else{
			$("#frmASalidas label[for=cliente]").css("color", "#333333");
			SalidasError = 0;
		}
		if(SalDescripcion.val().length <= 0){
			$("#frmASalidas label[for=descripcion]").css("color", "#f94442");
			SalidasError = 1;
		}else{
			$("#frmASalidas label[for=descripcion]").css("color", "#333333");
			SalidasError = 0;
		}
		if(SalidasError){
			swal({
				title: "¡Error al añadir la salida!",
				text: "Corrije los campos marcados en rojo",
				type: "error",
				confirmButtonText: "Cerrar"
			});
		}else{
			swal({
				title: "¡Un momento!",
				text: "Tu sesión está iniciada como: " + $(".userrol .name").text(),
				type: "info",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Soy yo, añadir salida",
				cancelButtonText: "Cancelar",
				closeOnConfirm: false,
				closeOnCancel: true,
				showLoaderOnConfirm: false,
				html: true
			}, function(isConfirm){
				if(isConfirm){
					swal({
						title: "¡Salida añadida correctamente!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						type: "POST",
						url: "pages/ajax/subirsalidas.php",
						data: {
							salfecha: SalFecha.val(),
							salhora: SalHora.val(),
							salminutos: SalMinutos.val(),
							saltecnico: SalTecnico.val(),
							salcliente: SalCliente.val(),
							saldescripcion: SalDescripcion.val(),
							salfechallegada: SalFechaLlegada.val(),
							salfechallegadahora: SalFechaLlegadaHora.val(),
							salfechallegadaminutos: SalFechaLlegadaMinutos.val(),
							salpincidencia: SalParteIncidencias.val(),
							salnfactura: SalNFactura.val()
						}
					});
					$("button.confirm").click(function(){
						window.open("./salidas/", "_parent");
					});
				}
			});
		}
	});
	
	// Editar Salidas
	var SalEdtID = $("#saledtid");
	var SalEdtFecha = $("#saledtfecha");
	var SalEdtHora = $("#saledthora");
	var SalEdtMinutos = $("#saledtminutos");
	var SalEdtCliente = $("#saledtcliente");
	var SalEdtDescripcion = $("#saledtdescripcion");
	var SalEdtFechaLlegada = $("#saledtfecha_llegada");
	var SalEdtFechaLlegadaHora = $("#saledtfecha_llegada_hora");
	var SalEdtFechaLlegadaMinutos = $("#saledtfecha_llegada_minutos");
	var SalEdtParteIncidencia = $("#saledtp_incidencia");
	var SalEdtNFactura = $("#saledtnfactura");
	$("#BtnEdtSal").click(function(){
		swal({
			title: "¡Salida guardada!",
			type: "success",
			confirmButtonText: "Cerrar"
		});
		$.ajax({
			type: "POST",
			url: "pages/ajax/editarsalida.php",
			data: {
				saledtid: SalEdtID.val(),
				saledtfecha: SalEdtFecha.val(),
				saledthora: SalEdtHora.val(),
				saledtminutos: SalEdtMinutos.val(),
				saledtcliente: SalEdtCliente.val(),
				saledtdescripcion: SalEdtDescripcion.val(),
				saledtfechallegada: SalEdtFechaLlegada.val(),
				saledtfechallegadahora: SalEdtFechaLlegadaHora.val(),
				saledtfechallegadaminutos: SalEdtFechaLlegadaMinutos.val(),
				saledtpincidencia: SalEdtParteIncidencia.val(),
				saledtnfactura: SalEdtNFactura.val()
			}
		});
		$("button.confirm").click(function(){
			window.open("./salidas/", "_parent");
		});
	});
	
	// Ver Salidas (modal)
	$(".VerSal").click(function(){
		var VerSalID = $(this).data("id");
		$("#ShowVerSalida").modal({
			show: true,
			backdrop: "static",
			keyboard: false
		});
		$("#IDSalVer").text(VerSalID);
		$.ajax({
			type: "POST",
			url: "pages/ajax/versalida.php",
			data: {
				salvid: VerSalID
			},
			success: function(data){
				$("#ResultVerSal").html(data);
			}
		});
		$(".modal button.btn-default").click(function(){
			window.open($(location).attr("href"), "_parent");
		});
	});
	
	// Guardar Salida en el historial
	$(".SavSal").click(function(){
		var SavID = $(this).data("id");
		swal({
			title: "¿Guardar salida?",
		  	text: "La salida #" + SavID + " será guardada en el historial",
		  	type: "info",
		  	showCancelButton: true,
			showLoaderOnConfirm: true,
		  	confirmButtonText: "Guardar",
			cancelButtonText: "Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true
		},
		function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Salida guardada en el historial!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						type: "POST",
						url: "pages/ajax/guardarsalida.php",
						data: {
							savsalid: SavID
						}
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open($(location).attr("href"), "_parent");
				});
			}
		});
	});
	
	// Eliminar Salidas
	$(".DelSal").click(function(){
		var DelID = $(this).data("id");
		swal({
			title: "¿Está seguro?",
		  	text: "¡Está apunto de eliminar la salida!",
		  	type: "warning",
		  	showCancelButton: true,
			showLoaderOnConfirm: true,
		  	confirmButtonColor: "#DD6B55",
		  	confirmButtonText: "Si, Eliminar",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true,
			html: true
		},
		function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Salida eliminada!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						type: "POST",
						url: "pages/ajax/eliminarsalida.php",
						data: {
							Dsalid: DelID
						}
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open($(location).attr("href"), "_parent");
				});
			}
		});
	});
	
	// Eliminar todo el historial de salidas
	$("#DelHisAllsalidas").click(function(){
		swal({
			title: "¿Está seguro?",
		  	text: "¡Está apunto de vaciar todo el historial de salidas!",
		  	type: "warning",
		  	showCancelButton: true,
		  	confirmButtonColor: "#DD6B55",
		  	confirmButtonText: "Si, Vaciar",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true,
			showLoaderOnConfirm: true
		},function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Se ha vaciado el historial!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						url: "pages/ajax/eliminarhissalidas.php"
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open($(location).attr("href"), "_parent");
				});
			}
		});
	});
	
	// Buscador Fhios
	$("#SchFhios > li > a").removeAttr("href").css("cursor", "pointer");
	$("#ShowSchFechaFhios").hide();
	$("#SchFhios > li > a").click(function(){
		var DataSchType = $(this).data("type");
		if(DataSchType === "fecha"){
			$("#TxtFhiosSch").text("Fecha");
			$("#ShowSchDefaultFhios").hide();
			$("#ShowSchFechaFhios").show();
			$("#BtnSchFhios").click(function(){
				window.open("fhios/buscar/?type=" + DataSchType + "&sch=" + $("#FechaSchFhios").val(), "_parent");
			});
		}
	});
	
	// Añadir Fhios
	var FhiosError;
	var FhiTecnico = $("#fhitecnico");
	var FhiNaviso = $("#fhinaviso");
	var FhiFecha = $("#fhifecha");
	var FhiHora = $("#fhihora");
	var FhiMinutos = $("#fhiminutos");
	var FhiCliente = $("#fhicliente");
	var FhiDireccion = $("#fhidireccion");
	var FhiDescripcion = $("#fhidescripcion");
	FhiNaviso.numeric();
	$("#BtnAFhios").click(function(){
		if(FhiNaviso.val().length <= 0){
			$("#frmAFhios label[for=naviso]").css("color", "#f94442");
			FhiosError = 1;
		}else{
			$("#frmAFhios label[for=naviso]").css("color", "#333333");
			FhiosError = 0;
		}
		if(FhiFecha.val().length <= 0 || FhiHora.val().length <= 0 || FhiMinutos.val().length <= 0){
			$("#frmAFhios label[for=fecha]").css("color", "#f94442");
			FhiosError = 1;
		}else{
			$("#frmAFhios label[for=fecha]").css("color", "#333333");
			FhiosError = 0;
		}
		if(FhiCliente.val().length <= 0){
			$("#frmAFhios label[for=cliente]").css("color", "#f94442");
			FhiosError = 1;
		}else{
			$("#frmAFhios label[for=cliente]").css("color", "#333333");
			FhiosError = 0;
		}
		if(FhiosError){
			swal({
				title: "¡Error al añadir fhios!",
				text: "Corrije los campos marcados en rojo",
				type: "error",
				confirmButtonText: "Cerrar"
			});
		}else{
			$.ajax({
				type: "POST",
				url: "pages/ajax/subirfhios.php",
				data: {
					fhitecnico: FhiTecnico.val(),
					fhinaviso: FhiNaviso.val(),
					fhifecha: FhiFecha.val(),
					fhihora: FhiHora.val(),
					fhiminutos: FhiMinutos.val(),
					fhicliente: FhiCliente.val(),
					fhidireccion: FhiDireccion.val(),
					fhidescripcion: FhiDescripcion.val()
				}
			});
			swal({
				title: "¡Fhios añadido correctamente!",
				type: "success",
				confirmButtonText: "Cerrar"
			});
			$("button.confirm").click(function(){
				window.open("./fhios", "_parent");
			});
		}
	});
	
	// Editar Fhios
	var EdtFhiosError;
	var EdtFhiID = $("#fhiedtid");
	var EdtFhiTecnico = $("#fhiedttecnico");
	var EdtFhiNaviso = $("#fhiedtnaviso");
	var EdtFhiFecha = $("#fhiedtfecha");
	var EdtFhiHora = $("#fhiedthora");
	var EdtFhiMinutos = $("#fhiedtminutos");
	var EdtFhiCliente = $("#fhiedtcliente");
	var EdtFhiDireccion = $("#fhiedtdireccion");
	var EdtFhiDescripcion = $("#fhiedtdescripcion");
	EdtFhiNaviso.numeric();
	$("#BtnEdtfhi").click(function(){
		if(EdtFhiNaviso.val().length <= 0){
			$("#frmEdtFhi label[for=edtnaviso]").css("color", "#f94442");
			EdtFhiosError = 1;
		}else{
			$("#frmEdtFhi label[for=edtnaviso]").css("color", "#333333");
			EdtFhiosError = 0;
		}
		if(EdtFhiCliente.val().length <= 0){
			$("#frmEdtFhi label[for=edtcliente]").css("color", "#f94442");
			EdtFhiosError = 1;
		}else{
			$("#frmEdtFhi label[for=edtcliente]").css("color", "#333333");
			EdtFhiosError = 0;
		}
		if(EdtFhiosError){
			swal({
				title: "¡Error al editar fhios!",
				text: "Corrije los campos marcados en rojo",
				type: "error",
				confirmButtonText: "Cerrar"
			});
		}else{
			$.ajax({
				type: "POST",
				url: "pages/ajax/editarfhios.php",
				data: {
					edtfhiid: EdtFhiID.val(),
					edtfhitecnico: EdtFhiTecnico.val(),
					edtfhinaviso: EdtFhiNaviso.val(),
					edtfhifecha: EdtFhiFecha.val(),
					edtfhihora: EdtFhiHora.val(),
					edtfhiminutos: EdtFhiMinutos.val(),
					edtfhicliente: EdtFhiCliente.val(),
					edtfhidireccion: EdtFhiDireccion.val(),
					edtfhidescripcion: EdtFhiDescripcion.val()
				}
			});
			swal({
				title: "¡Fhios guardado correctamente!",
				type: "success",
				confirmButtonText: "Cerrar"
			});
			$("button.confirm").click(function(){
				window.open("./fhios", "_parent");
			});
		}
	});
	
	// Ver Fhios (modal)
	$(".VerFhi").click(function(){
		var VerFhiID = $(this).data("id");
		var VerFhiNoAviso = $(this).data("naviso");
		$("#NoAvisoFhi").text(VerFhiNoAviso);
		$("#ShowVerFhios").modal({
			show: true,
			backdrop: "static",
			keyboard: false
		});
		$.ajax({
			type: "POST",
			url: "pages/ajax/verfhios.php",
			data: {
				fhivid: VerFhiID,
				fhivnaviso: VerFhiNoAviso
			},
			success: function(data){
				$("#NoAvisoFhi").text(VerFhiNoAviso);
				$("#ResultVerFhi").html(data);
			}
		});
		$(".modal button.btn-default").click(function(){
			window.open($(location).attr("href"), "_parent");
		});
	});
	
	// Ver Tabla Fhios (modal)
	$(".TblFhi").click(function(){
		var VerTblFhiID = $(this).data("id");
		var VerTblFhiNAviso = $(this).data("naviso");
		var VerTblFhiCliente = $(this).data("cliente");
		var VerFhiFin = $(this).data("fin");
		$("#NoAvisoFhiTbl").text((VerTblFhiNAviso) ? VerTblFhiNAviso : '--');
		$("#ClienteFhiTbl").text((VerTblFhiCliente) ? VerTblFhiCliente : '--');
		$("#ShowVerTblFhios").modal({
			show: true,
			backdrop: "static",
			keyboard: false
		});
		$.ajax({
			type: "POST",
			url: "pages/ajax/vertblfhios.php",
			data: {
				tblfhivid: VerTblFhiID,
				tblfhivnaviso: VerTblFhiNAviso,
				tblfhicliente: VerTblFhiCliente,
				tblfhifin: VerFhiFin
			},
			success: function(data){
				$("#NoAvisoTblFhi").text(VerTblFhiNAviso);
				$("#AddBtnFinFhios").html("");
				if(VerFhiFin === 0){
					$("#AddBtnFinFhios").append(
						"<button id='FinTblFhios' type='button' class='btn btn-gac' data-naviso='" + VerTblFhiNAviso + "' style='float:left'>Finalizar fhios</button>"
					);
				}
				$("#ResultVerTblFhi").html(data);
			}
		});
		$(".modal button.btn-default").click(function(){
			window.open($(location).attr("href"), "_parent");
		});
	});
	
	// Eliminar Fhios
	$(".DelFhi").click(function(){
		var DelNAviso = $(this).data("naviso");
		swal({
			title: "¿Está seguro?",
		  	text: "¡Está apunto de eliminar el fhios nº " + DelNAviso + "!",
		  	type: "warning",
		  	showCancelButton: true,
			showLoaderOnConfirm: true,
		  	confirmButtonColor: "#DD6B55",
		  	confirmButtonText: "Si, Eliminar",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true,
			html: true
		},
		function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Fhios eliminado!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						type: "POST",
						url: "pages/ajax/eliminarfhios.php",
						data: {
							Dfhinaviso: DelNAviso
						}
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open($(location).attr("href"), "_parent");
				});
			}
		});
	});
	
	// Eliminar todo el historial de fhios
	$("#DelHisAllfhios").click(function(){
		swal({
			title: "¿Está seguro?",
		  	text: "¡Está apunto de eliminar todo el historial de fhios!",
		  	type: "warning",
		  	showCancelButton: true,
		  	confirmButtonColor: "#DD6B55",
		  	confirmButtonText: "Si, Eliminar",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true,
			showLoaderOnConfirm: true
		},function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Se ha eliminado el historial!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						url: "pages/ajax/eliminarhisfhios.php"
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open($(location).attr("href"), "_parent");
				});
			}
		});
	});
	
	// Añadir Acceso Remoto
	var AccRemotoError;
	var AccRemotoCliente = $("#accremotoCliente");
	var AccRemotoDia = $("#accremotoDia");
	var AccRemotoHora = $("#accremotoHora");
	var AccRemotoMinutos = $("#accremotoMinutos");
	var AccRemotoSat = $("#accremotoSat");
	var AccRemotoTecnico = $("#accremotoTecnico");
	var AccRemotoFacturado = $("#accremotoFacturado");
	$("#BtnAaccRemoto").click(function(event){
		event.preventDefault();
		if(AccRemotoCliente.val().length <= 0){
			$("#frmAaccRemoto label[for=cliente]").css("color", "#f94442");
			AccRemotoError = 1;
		}else{
			$("#frmAaccRemoto label[for=cliente]").css("color", "#333333");
			AccRemotoError = 0;
		}
		if(AccRemotoDia.val().length <= 0 || AccRemotoHora.val().length <= 0 || AccRemotoMinutos.val().length <= 0){
			$("#frmAaccRemoto label[for=dia_hora]").css("color", "#f94442");
			AccRemotoError = 1;
		}else{
			$("#frmAaccRemoto label[for=dia_hora]").css("color", "#333333");
			AccRemotoError = 0;
		}
		if(AccRemotoTecnico.val().length <= 0){
			$("#frmAaccRemoto label[for=tecnico]").css("color", "#f94442");
			AccRemotoError = 1;
		}else{
			$("#frmAaccRemoto label[for=tecnico]").css("color", "#333333");
			AccRemotoError = 0;
		}
		if(AccRemotoError){
			swal({
				title: "¡Error al añadir el acceso!",
				text: "Corrije los campos marcados en rojo",
				type: "error",
				confirmButtonText: "Cerrar"
			});
		}else{
			$.ajax({
				type: "POST",
				url: "pages/ajax/subiraccesoremoto.php",
				data: {
					accremotocliente: AccRemotoCliente.val(),
					accremotodia: AccRemotoDia.val(),
					accremotoHora: AccRemotoHora.val() + ":" + AccRemotoMinutos.val(),
					accremotosat: AccRemotoSat.val(),
					accremototecnico: AccRemotoTecnico.val(),
					accremotofacturado: AccRemotoFacturado.val()
				}
			});
			swal({
				title: "¡Acceso añadido correctamente!",
				type: "success",
				confirmButtonText: "Cerrar"
			});
			$("button.confirm").click(function(){
				window.open("./accesoremoto", "_parent");
			});
		}
	});
	
	// Editar Acceso Remoto
	var EdtAccRemotoError;
	var EdtAccRemotoID = $("#accRemotoedtid");
	var EdtAccRemotoCliente = $("#accremotoEdtCliente");
	var EdtAccRemotoDia = $("#accremotoEdtDia");
	var EdtAccRemotoHora = $("#accremotoEdtHora");
	var EdtAccRemotoMinutos = $("#accremotoEdtMinutos");
	var EdtAccRemotoSat = $("#accremotoEdtSat");
	var EdtAccRemotoTecnico = $("#accremotoEdtTecnico");
	var EdtAccRemotoFacturado = $("#accremotoEdtFacturado");
	EdtAccRemotoSat.numeric();
	$("#BtnEdtaccRemoto").click(function(event){
		event.preventDefault();
		if(EdtAccRemotoCliente.val().length <= 0){
			$("#frmEdtaccRemoto label[for=cliente]").css("color", "#f94442");
			EdtAccRemotoError = 1;
		}else{
			$("#frmEdtaccRemoto label[for=cliente]").css("color", "#333333");
			EdtAccRemotoError = 0;
		}
		if(EdtAccRemotoDia.val().length <= 0 || EdtAccRemotoHora.val().length <= 0 || EdtAccRemotoMinutos.val().length <= 0){
			$("#frmEdtaccRemoto label[for=dia_hora]").css("color", "#f94442");
			EdtAccRemotoError = 1;
		}else{
			$("#frmEdtaccRemoto label[for=dia_hora]").css("color", "#333333");
			EdtAccRemotoError = 0;
		}
		if(EdtAccRemotoTecnico.val().length <= 0){
			$("#frmEdtaccRemoto label[for=tecnico]").css("color", "#f94442");
			EdtAccRemotoError = 1;
		}else{
			$("#frmEdtaccRemoto label[for=tecnico]").css("color", "#333333");
			EdtAccRemotoError = 0;
		}
		if(EdtAccRemotoError){
			swal({
				title: "¡Error al editar el acceso!",
				text: "Corrije los campos marcados en rojo",
				type: "error",
				confirmButtonText: "Cerrar"
			});
		}else{
			$.ajax({
				type: "POST",
				url: "pages/ajax/editaraccesoremoto.php",
				data: {
					edtaccremotoid: EdtAccRemotoID.val(),
					edtaccremotocliente: EdtAccRemotoCliente.val(),
					edtaccremotodia: EdtAccRemotoDia.val(),
					edtaccremotoHora: EdtAccRemotoHora.val() + ":" + EdtAccRemotoMinutos.val(),
					edtaccremotosat: EdtAccRemotoSat.val(),
					edtaccremototecnico: EdtAccRemotoTecnico.val(),
					edtaccremotofacturado: EdtAccRemotoFacturado.val()
				}
			});
			swal({
				title: "¡Acceso actualizado correctamente!",
				type: "success",
				confirmButtonText: "Cerrar"
			});
			$("button.confirm").click(function(){
				window.open("./accesoremoto", "_parent");
			});
		}
	});
	
	// Guardar Acceso Remoto
	$(".SavAccesoRemoto").click(function(){
		var SavID = $(this).data("id");
		swal({
			title: "¿Guardar Acceso Remoto?",
		  	text: "",
		  	type: "info",
		  	showCancelButton: true,
			showLoaderOnConfirm: true,
		  	confirmButtonText: "Guardar",
			cancelButtonText: "Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true
		},
		function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Acceso remoto guardado!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						type: "POST",
						url: "pages/ajax/guardaraccesoremoto.php",
						data: {
							savaccremotoid: SavID
						}
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open($(location).attr("href"), "_parent");
				});
			}
		});
	});
	
	// Eliminar Acceso Remoto
	$(".DelAccesoRemoto").click(function(){
		var DelID = $(this).data("id");
		swal({
			title: "¿Está seguro?",
		  	text: "¡Está apunto de eliminar este acceso remoto!",
		  	type: "warning",
		  	showCancelButton: true,
			showLoaderOnConfirm: true,
		  	confirmButtonColor: "#DD6B55",
		  	confirmButtonText: "Si, Eliminar",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true,
			html: true
		},
		function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Acceso remoto eliminado!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						type: "POST",
						url: "pages/ajax/eliminaraccesoremoto.php",
						data: {
							delaccremotoid: DelID
						}
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open($(location).attr("href"), "_parent");
				});
			}
		});
	});
	
	// Enviar Sugerencias
	var SugError;
	var SugTecnico = $("#sugtecnico").val();
	var SugMensaje = $("#sugmensaje");
	$("#btnEnviarSug").click(function(){
		if(SugMensaje.val().length <= 0){
			SugMensaje.closest(".control-group").addClass("has-error");
			SugError = 1;
		}else{
			SugMensaje.closest(".control-group").removeClass("has-error");
			SugError = 0;
		}
		if(SugError){
			swal({
				title: "¡Sugerencia no enviada!",
				text: "Introduce una sugerencia antes de enviar",
				type: "error",
				confirmButtonText: "Cerrar"
			});
		}else{
			$("#ModalSugenrencia").modal("hide");
			swal({
				title: "¡Sugerencia enviada!",
				text: "Gracias por tu sugerencia " + SugTecnico,
				type: "success",
				confirmButtonText: "Cerrar"
			});
			$.ajax({
				type: "POST",
				url: "pages/ajax/subirsugerencia.php",
				data: {
					sugtecnico: SugTecnico,
					sugmensaje: SugMensaje.val()
				}
			});
		}
	});
	
	// Ver Sugerencia (modal)
	$(".VerSug").click(function(){
		var VerSugID = $(this).data("id");
		var VerSugTecnico = $(this).data("tecnico");
		$("#ShowVerSug").modal({
			show: true,
			backdrop: "static",
			keyboard: false
		});
		$.ajax({
			type: "POST",
			url: "pages/ajax/versugerencias.php",
			data: {
				sugvid: VerSugID,
				sugvtecnico: VerSugTecnico
			},
			success: function(data){
				$("#VerSugTecnico").text(VerSugTecnico);
				$("#ResultVerSug").html(data);
			}
		});
		$(".modal button.btn-default").click(function(){
			window.open($(location).attr("href"), "_parent");
		});
	});
	
	// Eliminar Sugerencia
	$(".DelSug").click(function(){
		var DelSugID = $(this).data("id");
		var DelSugTecnico = $(this).data("tec");
		swal({
			title: "¿Está seguro?",
		  	text: "¡Está apunto de eliminar la sugerencia de " + DelSugTecnico + "!",
		  	type: "warning",
		  	showCancelButton: true,
			showLoaderOnConfirm: true,
		  	confirmButtonColor: "#DD6B55",
		  	confirmButtonText: "Si, Eliminar",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: false
		}, function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Sugerencia eliminada!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						type: "POST",
						url: "pages/ajax/eliminarsugerencia.php",
						data: {
							id: DelSugID
						}
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open("./sugerencias", "_parent");
				});
			}else{
				swal({
					title: "¡Sugerencia no eliminada!",
					type: "error",
					confirmButtonText: "Cerrar"
				});
			}
		});
	});
	
	// Eliminar Todas las Sugerencias
	$("#DelSugAll").click(function(){
		swal({
			title: "¿Está seguro?",
		  	text: "¡Está apunto de eliminar todas las sugerencias!",
		  	type: "warning",
		  	showCancelButton: true,
			showLoaderOnConfirm: true,
		  	confirmButtonColor: "#DD6B55",
		  	confirmButtonText: "Si, Eliminar",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: false
		}, function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Sugerencias eliminadas!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						type: "POST",
						url: "pages/ajax/eliminarsugerencias.php"
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open("./sugerencias", "_parent");
				});
			}else{
				swal({
					title: "¡Sugerencias no eliminadas!",
					type: "error",
					confirmButtonText: "Cerrar"
				});
			}
		});
	});
	
	// Eliminar actividad
	$(".DelAct").click(function(){
		var DelActID = $(this).data("id");
		swal({
			title: "¿Está seguro?",
		  	text: "¡Está apunto de eliminar la actividad!",
		  	type: "warning",
		  	showCancelButton: true,
			showLoaderOnConfirm: true,
		  	confirmButtonColor: "#DD6B55",
		  	confirmButtonText: "Si, Eliminar",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true
		}, function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Actividad eliminada!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						type: "POST",
						url: "pages/ajax/eliminaractividad.php",
						data: {
							id: DelActID
						}
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open("./historial", "_parent");
				});
			}
		});
	});
	
	// Eliminar todo el historial de actividades
	$("#DelHisAllAct").click(function(){
		swal({
			title: "¿Está seguro?",
		  	text: "¡Está apunto de vaciar todo el historial de actividades!",
		  	type: "warning",
		  	showCancelButton: true,
		  	confirmButtonColor: "#DD6B55",
		  	confirmButtonText: "Si, Vaciar",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true,
			showLoaderOnConfirm: true
		},function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Se ha vaciado el historial!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						url: "pages/ajax/eliminarhisactividad.php"
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open($(location).attr("href"), "_parent");
				});
			}
		});
	});
	
	// Mostrar notas del diario (tiempo real)
	setInterval(function(){
		$.post("pages/ajax/mostrarnotas.php", function(data){
			$("#diario").html(data);
		});
	}, 100);
	
	// Añadir nueva nota (entrada diario)
	$("#addentrada").keypress(function(e){
		if($(this).val().length > 0){
			if(e.which === 13){
				$("#btnaddentrada").click();
				return false;
			}
		}
	});
	setInterval(function(){
		var DateAddNow = new Date();
		var DateGetHour = DateAddNow.getHours();
		var DateGetMinutes = DateAddNow.getMinutes();
		if(DateGetHour < 10){
			DateGetHour = "0" + DateGetHour;
		}
		if(DateGetMinutes < 10){
			DateGetMinutes = "0" + DateGetMinutes;
		}
		$("#addentradahora option.addhoranow").val(DateGetHour).attr("hidden", true);
		$("#addentradahora option.addhoranow").text(DateGetHour);
		$("#addentradaminutos option.addminutosnow").val(DateGetMinutes).attr("hidden", true);
		$("#addentradaminutos option.addminutosnow").text(DateGetMinutes);
		if(DateAddNow.getSeconds() === 0){
			$("#addentradahora, #addentradaminutos").prop("selectedIndex", 0);
		}
	}, 100);
	$("#btnaddentrada").click(function(e){
		e.preventDefault();
		var AddHora = $("#addentradahora");
		var AddMinutos = $("#addentradaminutos");
		var AddEntrada = $("#addentrada");
		var AddPor = $("#addtecnico");
		if(AddEntrada.val() !== ""){
			AddEntrada.attr("disabled", true);
			setTimeout(function(){
				$.post("pages/ajax/subirnotas.php", {
					ntahora: AddHora.val(),
					ntaminutos: AddMinutos.val(),
					ntadescripcion: AddEntrada.val(),
					ntapor: AddPor.val()
				}, function(data){
					if(data === "success"){
						AddEntrada.attr("disabled", false);
						$("#frmAddDiario")[0].reset();
						AddEntrada.blur();
						AddEntrada.val("").attr("placeholder", "INTRODUCE TU ENTRADA AQUÍ Y PULSA ENTER...");
					}else{
						swal({
							title: "¡Ha ocurrido un error!",
							text: "No ha sido posible añadir la entrada. Inténtalo de nuevo.",
							type: "error",
							confirmButtonText: "Cerrar"
						});
						$("#frmAddDiario")[0].reset();
					}
				});
			}, 1000);
		}
	});
	
	// Eliminar nota
	$(".DelNota").click(function(){
		var DelNtaID = $(this).data("id");
		var DelNtaPor = $(this).data("por");
		swal({
			title: "¿Está seguro?",
		  	text: "Está apunto de eliminar la entra de " + DelNtaPor,
		  	type: "warning",
		  	showCancelButton: true,
			showLoaderOnConfirm: true,
		  	confirmButtonColor: "#DD6B55",
		  	confirmButtonText: "Si, Eliminar",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true
		}, function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Entrada eliminada!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						type: "POST",
						url: "pages/ajax/eliminarnotas.php",
						data: {
							delntaid: DelNtaID,
							delntapor: DelNtaPor
						}
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open("./diario/historial/", "_parent");
				});
			}
		});
	});
	
	// Eliminar todo el historial de notas
	$("#DelAllNotas").click(function(){
		swal({
			title: "¿Está seguro?",
		  	text: "¡Está apunto de vaciar todo el historial!",
		  	type: "warning",
		  	showCancelButton: true,
		  	confirmButtonColor: "#DD6B55",
		  	confirmButtonText: "Si, Vaciar",
			cancelButtonText: "No, Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true,
			showLoaderOnConfirm: true
		},function(isConfirm){
			if(isConfirm){
				setTimeout(function(){
					swal({
						title: "¡Se ha vaciado el historial!",
						type: "success",
						confirmButtonText: "Cerrar"
					});
					$.ajax({
						url: "pages/ajax/eliminarhisnotas.php"
					});
				}, 1000);
				$("button.confirm").click(function(){
					window.open("./diario/historial/", "_parent");
				});
			}
		});
	});
	
	// Buscador historial diario
	$("#SchNta > li > a").removeAttr("href").css("cursor", "pointer");
	$("#ShowSchFechaNta, #ShowSchTecnicoNta, #ShowSchFechaTecnicoNta").hide();
	$("#SchNta > li > a").click(function(){
		var DataSchType = $(this).data("type");
		if(DataSchType === "fecha"){
			$("#TxtNtaSch").text("Fecha");
			$("#ShowSchDefaultNta, #ShowSchTecnicoNta, #ShowSchFechaTecnicoNta").hide();
			$("#ShowSchFechaNta").show();
			$("#BtnSchNta").click(function(){
				window.open("diario/buscar/?type=" + DataSchType + "&sch=" + $("#FechaSchNta").val(), "_parent");
			});
		}else{
			$("#TxtNtaSch").text(DataSchType.charAt(0).toUpperCase() + DataSchType.slice(1));
		}
		if(DataSchType === "tecnico"){
			$("#BtnSchNta").css("margin-top", "0");
			$("#TxtNtaSch").text("Técnico");
			$("#ShowSchDefaultNta, #ShowSchFechaNta, #ShowSchFechaTecnicoNta").hide();
			$("#ShowSchTecnicoNta").show();
			$("#BtnSchNta").click(function(){
				window.open("diario/buscar/?type=" + DataSchType + "&sch=" + $("#TecSchNta").val(), "_parent");
			});
		}
		if(DataSchType === "fecha_tecnico"){
			$("#BtnSchNta").css("margin-top", "-5px");
			$("#TxtNtaSch").text("Fecha y Técnico");
			$("#ShowSchDefaultNta, #ShowSchFechaNta, #ShowSchTecnicoNta").hide();
			$("#ShowSchFechaTecnicoNta").show();
			$("#BtnSchNta").click(function(){
				window.open("diario/buscar/?type=" + DataSchType + "&sch=" + $("#FechaTSchNta").val() + "," + $("#TecFSchNta").val(), "_parent");
			});
		}
	});
	
	// Exportar tablas a PDF
	$(".exportPdf").click(function(event){
		event.preventDefault();
		var ExportTxt;
		var ExportTbl = $(this).data("tbl");
		if(ExportTbl === "incidencias"){
			ExportTxt = "<span style='font-size:25px'>¿Descargar listado de Incidencias?</span>";
		}else if(ExportTbl === "llamadas"){
			ExportTxt = "<span style='font-size:25px'>¿Descargar listado de Llamadas?</span>";
		}else if(ExportTbl === "pedidos"){
			ExportTxt = "<span style='font-size:27px'>¿Descargar listado de Pedidos?</span>";
		}else if(ExportTbl === "satsponsa"){
			ExportTbl = "satsponsa";
			ExportTxt = "<span style='font-size:20px'>¿Descargar listado de SAT's Santa Ponsa?</span>";
		}else if(ExportTbl === "satpaguera"){
			ExportTbl = "satpaguera";
			ExportTxt = "<span style='font-size:22px'>¿Descargar listado de SAT's Paguera?</span>";
		}else if(ExportTbl === "salidas"){
			ExportTxt = "<span style='font-size:27px'>¿Descargar listado de Salidas?</span>";
		}else if(ExportTbl === "fhios"){
			ExportTxt = "<span style='font-size:28px'>¿Descargar listado de Fhios?</span>";
		}else if(ExportTbl === "accesoremoto"){
			ExportTxt = "<span style='font-size:22px'>¿Descargar listado de Accesos Remotos?</span>";
		}else{
			ExportTbl = $(this).data("tbl");
			ExportTxt = ExportTbl;
		}
		swal({
			title: ExportTxt,
			type: "info",
			showCancelButton: true,
		  	confirmButtonColor: "#DD6B55",
		  	confirmButtonText: "Descargar",
			cancelButtonText: "Cancelar",
			closeOnConfirm: false,
  			closeOnCancel: true,
			showLoaderOnConfirm: true,
			html: true
		}, function(isExport){
			if(isExport){
				setTimeout(function(){
					$(".sweet-overlay, .sweet-alert").css("display", "none");
					window.open("pages/ajax/exportarpdf.php?export=" + ExportTbl, "_blank");
				}, 1000);
			}
		});
	});
	
});