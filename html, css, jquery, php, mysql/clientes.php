<?php
if(empty($_SESSION['ID'])) {
	echo '<script>window.location="./";</script>';
	exit();
}

$registros_por_pagina = 20;
	
if(!empty($_GET['pg'])) {
	$comienzo_pagina = ($_GET['pg'] - 1) * $registros_por_pagina;
	$pagina_actual = $_GET['pg'];
}else {
	$comienzo_pagina = 0;
	$pagina_actual = 1;
}

if($url['dir'] == 'clientes') {
	$clientes = 1;
	$clientes_agr = 0;
	$clientes_edt = 0;
	
	$sql = '
		SELECT
			id, name, username, email, created
		FROM
			clientes
		ORDER BY
			name ASC
		LIMIT
			'.$comienzo_pagina.', '.$registros_por_pagina.'
	';
			
	$rsc = $con->query($sql);
	
	$sql_total = '
		SELECT
			COUNT(id) AS total
        FROM
			clientes
	';
	$rsc_total = $con->query($sql_total);
	$total_clientes = $rsc_total->fetch_assoc();
	
	$pagina_anterior = $pagina_actual - 1;
		
	$pagina_siguiente = $pagina_actual + 1;
	
	$respuesta = $total_clientes['total'] % $registros_por_pagina;
	
	$ultima_pagina = $total_clientes['total'] / $registros_por_pagina;
	
	if($respuesta > 0) {
		$ultima_pagina = floor($ultima_pagina) + 1;
	}
	
	if(!empty($url['args'][0])) {
		$explode_args = explode('-', $url['args'][0]);
		if($explode_args[0] == 'agregar') {
			$clientes = 0;
			$clientes_agr = 1;
			$clientes_edt = 0;
			
		}
		if($explode_args[0] == 'editar') {
			$clientes = 0;
			$clientes_agr = 0;
			$clientes_edt = 1;
			
			$sql = '
				SELECT
					id, name, username, email, created
				FROM
					clientes
				WHERE
					id = "'.$explode_args[1].'"
				LIMIT
					1
			';
			
			$rsc = $con->query($sql);
			
			$rs_edt = $rsc->fetch_assoc();
			
		}
	}
}
?>
<div class="container">
	<div class="row">
    	<?php if($clientes == 1) { ?>
    	<div class="well well-sm white">
            <div class="btnRight" style="margin-bottom:20px">
                <a href="clientes/agregar"><button type="button" class="btn btn-gac">Añadir cliente</button></a>
            </div>
            <a href="./" class="btn btn-back">
                <i class="glyphicon glyphicon-circle-arrow-left"></i> Volver al inicio
            </a>
        	<div id="TableResponsive" class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr class="info">
                            <th>Nombre</th>
                            <th>Usuario</th>
                            <th>Email</th>
                            <th>Alta</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<?php if($total_clientes['total'] > 0) : ?>
							<?php while($row = $rsc->fetch_assoc()) : ?>
                            <tr>
                                <td><?= mb_strtoupper($row['name'], 'utf-8'); ?></td>
                                <td><?= ($row['username']) ? mb_strtoupper($row['username'], 'utf-8') : '--'; ?></td>
                                <td><?= ($row['email']) ? mb_strtoupper($row['email'], 'utf-8') : '--'; ?></td>
                                <?php $created_format = explode(' ', $row['created']); ?>
                                <?php $created = explode('-', $created_format[0]); ?>
                                <td><?= $created[2].'/'.$created[1].'/'.$created[0]; ?></td>
                                <td>
                                <div class="btn-group ActionResponsive">
                                    <button type="button" class="btn btn-default dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="position:relative">-- <span class="caret"></span></button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="clientes/editar-<?= $row['id']; ?>" style="cursor:pointer"><i class="glyphicon glyphicon-pencil"></i> Editar Cliente</a></li>
                                        <li><a class="DelCli" data-id="<?= $row['id']; ?>" data-name="<?= $row['name']; ?>" style="cursor:pointer"><i class="glyphicon glyphicon-remove"></i> Eliminar Cliente</a></li>
                                    </ul>
                                </div>
                                
                                <div class="ActionNoResponsive">
                                <a href="clientes/editar-<?= $row['id']; ?>" style="color:#333; margin-right:15px" data-toggle="tooltip" data-placement="bottom" title="Editar cliente"><i class="glyphicon glyphicon-pencil" style="font-size:12px"></i></a>
                                <a class="DelCli" data-id="<?= $row['id']; ?>" data-name="<?= $row['name']; ?>" style="color:#333; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Eliminar cliente"><i class="glyphicon glyphicon-remove"></i></a>
                                </div>
                                </td>
                            </tr>
                            <?php endwhile; ?>
                        <?php else : ?>
                        	<tr>
                            	<td colspan="5">No se han encontrado clientes para mostrar</td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <nav>
            	<?php if($ultima_pagina > 0) { ?>
                <div class="pull-left" style="margin:5px 0 0 5px">
                    <span class="text-white">Página <strong><?= $pagina_actual; ?></strong> de <strong><?= $ultima_pagina; ?></strong></span>
                </div>
                <?php } ?>
                
                <ul class="pager">
                    <?php if($ultima_pagina > 0) { ?>
                        <?php if($pagina_actual > 1) { ?>
                        <li><a href="clientes/?pg=<?= $pagina_anterior; ?>">Anterior</a></li>
                        <?php }else { ?>
                        <li class="disabled"><a langhref="#">Anterior</a></li>
                        <?php } ?>
                        <?php if($pagina_actual < $ultima_pagina) { ?>
                        <li><a href="clientes/?pg=<?= $pagina_siguiente; ?>">Siguiente</a></li>
                        <?php }else { ?>
                        <li class="disabled"><a langhref="#">Siguiente</a></li>
                        <?php } ?>
                    <?php } ?>
                </ul>
			</nav>
        </div>
        <?php } ?>
        
        <?php if($clientes_agr == 1) { ?>
    	<div class="well well-sm white">
            <a href="clientes" class="btn btn-back">
                <i class="glyphicon glyphicon-circle-arrow-left"></i> Volver atrás
            </a>
            <hr>
            <form id="frmAclientes" method="post" class="form-horizontal">
                <div class="form-group">
                    <div class="control-group">
                        <label for="nombre" class="col-sm-2 control-label">Nombre</label>
                        <div class="col-sm-10">
                        	<div class="ui-widget">
                            	<input type="text" class="form-control" id="clinombre">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="control-group">
                        <label for="username" class="col-sm-2 control-label">Nombre de usuario</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="cliusername">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="control-group">
                        <label for="email" class="col-sm-2 control-label">Dirección de email</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="cliemail">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="control-group">
                        <label for="password" class="col-sm-2 control-label">Contraseña</label>
                        <div class="col-sm-10">
                        	<div class="col-sm-12">
                                <button type="button" class="btn btn-gac BtnGenPassCli pull-right" style="position:absolute; right:0; top:32px">Generar contraseña</button>
                                <input type="password" class="form-control" id="clipassword" style="margin-left:-15px">
                                <a href="#" class="CliVerPassGen" style="position:absolute; right:180px; top: 7px; cursor:pointer; color:#333; display:none">
                                	<i class="glyphicon glyphicon-eye-open"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <button type="button" id="BtnAclientes" class="btn btn-gac btn-block">Añadir cliente</button>
                    </div>
           		</div>
            </form>
        </div>
        <?php } ?>
        
        <?php if($clientes_edt == 1) { ?>
    	<div class="well well-sm white">
            <a href="clientes" class="btn btn-back">
                <i class="glyphicon glyphicon-circle-arrow-left"></i> Volver atrás
            </a>
            <hr>
            <form id="frmEdtclientes" method="post" class="form-horizontal">
            	<input type="hidden" id="clieditid" value="<?= $rs_edt['id']; ?>">
                <div class="form-group">
                    <div class="control-group">
                        <label for="nombre" class="col-sm-2 control-label">Nombre</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="clieditnombre" value="<?= $rs_edt['name']; ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="control-group">
                        <label for="username" class="col-sm-2 control-label">Nombre de usuario</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="clieditusername" value="<?= $rs_edt['username']; ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="control-group">
                        <label for="email" class="col-sm-2 control-label">Dirección de email</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="clieditemail" value="<?= $rs_edt['email']; ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="control-group">
                        <label for="password" class="col-sm-2 control-label">Contraseña</label>
                        <div class="col-sm-10">
                        	<div class="col-sm-12">
                                <button type="button" class="btn btn-gac BtnGenPassCliEdit pull-right" style="position:absolute; right:0; top:32px">Generar nueva contraseña</button>
                                <input type="password" class="form-control" id="clieditpassword" placeholder="Para no cambiar, dejar en blanco" style="margin-left:-15px">
                                <a href="#" class="CliEditVerPassGen" style="position:absolute; right:220px; top: 7px; cursor:pointer; color:#333; display:none">
                                	<i class="glyphicon glyphicon-eye-open"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <button type="button" id="BtnEditclientes" class="btn btn-gac btn-block">Guardar cambios</button>
                    </div>
           		</div>
			</div>
            </form>
        </div>
        <?php } ?>
    </div>
</div>