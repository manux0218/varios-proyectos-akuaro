<?php
if(empty($_SESSION['ID']) || $show_inc != 1) {
	echo '<script>window.location="./";</script>';
	exit();
}

$posicion_urge = 0;
$posicion = 0;
$registros_por_pagina = 20;
	
if(!empty($_REQUEST['pg'])) {
	$comienzo_pagina = ($_REQUEST['pg'] - 1) * $registros_por_pagina;
	$pagina_actual   = $_REQUEST['pg'];
}else {
	$comienzo_pagina = 0;
	$pagina_actual   = 1;
}

$sql = 'SELECT nombre
	    FROM tecnicos
		ORDER BY nombre ASC';
$rsc_tecnicos = $con->query($sql);
$rsc_ftecnicos = $con->query($sql);
$asg_tecnicos = $con->query($sql);
$fn_tecnicos = $con->query($sql);
$total_tecnicos = mysqli_num_rows($rsc_tecnicos);

if($url['dir'] == 'incidencias') {
	$incidencias = 1;
	$incidencias_agr = 0;
	$incidencias_edt = 0;
	$incidencias_sch = 0;
	$incidencias_fin = 0;
	
	$sql = 'SELECT COUNT(id) as total
	        FROM incidencias
		    WHERE urge = 1';
	$rsc = $con->query($sql);
	$total_urge_incidencias = $rsc->fetch_assoc();
	
	if(!empty($_REQUEST['pg'])) {
		$comienzo_pagina_urge = ($_REQUEST['pg'] - 1) * $total_urge_incidencias['total'];
	}else {
		$comienzo_pagina_urge = 0;
	}
	
	if($total_urge_incidencias['total'] > 0) {
		if($row['admin'] == 1) {
			$where_inc_urg = '';
		}else {
			$where_inc_urg = 'AND (tecnico = "'.$row['nombre'].'" OR tecnico = "")';
		}
	}else {
		$where_inc_urg = '';
	}
	
	$sql_urge = 'SELECT id, n_incidencia, tomador, fecha, cliente, solicitante, descripcion, tecnico, tipo, urge, estado
	             FROM incidencias
				 WHERE urge = 1 AND estado = "urgente"
				 ORDER BY fecha ASC
				 LIMIT '.$comienzo_pagina_urge.', '.$total_urge_incidencias['total'];
	$rsc_urge = $con->query($sql_urge);
	
	$sql = 'SELECT COUNT(id) as total
	        FROM incidencias
			WHERE urge = 0 AND estado != "finalizada"';
	$rsc = $con->query($sql);
	$total_incidencias = $rsc->fetch_assoc();
	
	if($total_incidencias['total'] > 0) {
		if($row['admin'] == 1) {
			$where_inc = '';
		}else {
			$where_inc = 'AND (tecnico = "'.$row['nombre'].'" OR tecnico = "")';
		}
	}else {
		$where_inc = '';
	}
	
	$sql = 'SELECT id, n_incidencia, tomador, fecha, cliente, solicitante, descripcion, tecnico, tipo, urge, estado
	        FROM incidencias
			WHERE urge = 0 AND estado != "finalizada"
			ORDER BY fecha ASC
			LIMIT '.$comienzo_pagina.', '.$registros_por_pagina;
	$rsc = $con->query($sql);
	
	$pagina_anterior  = $pagina_actual - 1;
	
	$pagina_siguiente = $pagina_actual + 1;
	
	$respuesta        = $total_incidencias['total'] % $registros_por_pagina;
	
	$ultima_pagina    = $total_incidencias['total'] / $registros_por_pagina;
	
	if($respuesta > 0) {
		$ultima_pagina = floor($ultima_pagina) + 1;
	}
	
	if(!empty($url['args'][0])) {
		$explode_args = explode('-', $url['args'][0]);
		
		if($explode_args[0] == 'agregar') {
			$incidencias = 0;
			$incidencias_agr = 1;
			$incidencias_edt = 0;
			$incidencias_sch = 0;
			$incidencias_fin = 0;
		}
		if($explode_args[0] == 'editar') {
			$incidencias = 0;
			$incidencias_agr = 0;
			$incidencias_edt = 1;
			$incidencias_sch = 0;
			$incidencias_fin = 0;
			
			$sql = 'SELECT nombre
			        FROM tecnicos';
			$redttecnico  = $con->query($sql);
			$rfedttecnico = $con->query($sql);
			
			$sql = 'SELECT id, n_incidencia, tomador, fecha, cliente, solicitante, descripcion, tecnico, tipo, urge, 
			               estado
			        FROM incidencias
					WHERE n_incidencia = "'.$explode_args[1].'" AND estado != "finalizada"
					LIMIT 1';
			$rsc_edt = $con->query($sql);
			$rs_edt = $rsc_edt->fetch_assoc();
			
			$edt_total = mysqli_num_rows($rsc_edt);
			
			$fecha_edt = explode(' ', $rs_edt['fecha']);
			$edt_fecha = explode('-', $fecha_edt[0]);
			$edt_hm = explode(':', $fecha_edt[1]);
			$edt_fecha = $edt_fecha[2].'/'.$edt_fecha[1].'/'.$edt_fecha[0];
			$edt_hora = $edt_hm[0];
			$edt_minutos = $edt_hm[1];
		}
		if($explode_args[0] == 'eliminar') {
			$sql = 'DELETE FROM incidencias
			        WHERE n_incidencia = "'.$explode_args[1].'" AND estado != "finalizada"';
			$con->query($sql);
			echo '<script>window.location="./incidencias";</script>';
		}
		if($url['args'][0] == 'buscar') {
			$incidencias = 0;
			$incidencias_agr = 0;
			$incidencias_edt = 0;
			$incidencias_sch = 1;
			$incidencias_fin = 0;
			
			if($row['admin'] == 1) {
				$where_schinc = '';
			}else {
				$where_schinc = 'AND tecnico = "'.$row['nombre'].'" OR tecnico = ""';
			}
			
			if($_REQUEST['type'] == 'fecha') {
				$fecha_schinc = explode('-', $_REQUEST['sch']);
				$sep1_schinc = explode('/', $fecha_schinc[0]);
				$sep2_schinc = explode('/', $fecha_schinc[1]);
				$fecha_schinc1 = $sep1_schinc[2].'-'.$sep1_schinc[1].'-'.$sep1_schinc[0];
				$fecha_schinc2 = $sep2_schinc[2].'-'.$sep2_schinc[1].'-'.$sep2_schinc[0];
				
				$wheresch = 'DATE(fecha) BETWEEN "'.$fecha_schinc1.'" AND "'.$fecha_schinc2.'"';
			}else {
				$wheresch = ''.$_REQUEST['type'].' LIKE "%'.$_REQUEST['sch'].'%"';
			}
			
			$sql = 'SELECT COUNT(id) as total
					FROM incidencias
					WHERE '.$wheresch.' '.$where_schinc;
			$rsc = $con->query($sql);
			$total_schincidencias = $rsc->fetch_assoc();
			
			$sql = 'SELECT id, n_incidencia, tomador, fecha, cliente, solicitante, descripcion, tecnico, tipo, urge, 
			               estado
					FROM incidencias
					WHERE '.$wheresch.' '.$where_schinc.'
					ORDER BY fecha ASC
					LIMIT '.$comienzo_pagina.', '.$registros_por_pagina;
			$rsc_schincidencias = $con->query($sql);
			
			$pagina_anterior  = $pagina_actual - 1;
			
			$pagina_siguiente = $pagina_actual + 1;
			
			$respuesta        = $total_schincidencias['total'] % $registros_por_pagina;
			
			$ultima_pagina    = $total_schincidencias['total'] / $registros_por_pagina;
			
			if($respuesta > 0) {
				$ultima_pagina = floor($ultima_pagina) + 1;
			}
		}
		if($url['args'][0] == 'finalizadas') {
			$incidencias = 0;
			$incidencias_agr = 0;
			$incidencias_edt = 0;
			$incidencias_sch = 0;
			$incidencias_fin = 1;
			
			$sql = 'SELECT COUNT(id) as total
					FROM incidencias
					WHERE estado = "finalizada"';
			$rsc = $con->query($sql);
			$total_finalizadas = $rsc->fetch_assoc();
			
			$sql_finalizadas = 'SELECT id, n_incidencia, cliente, descripcion, urge, estado, tecnico_fin, fecha_fin, 
			                           facturado
			                    FROM incidencias
								WHERE urge = 0 AND estado = "finalizada"
								ORDER BY fecha DESC
								LIMIT '.$comienzo_pagina.', '.$registros_por_pagina.'';
			$rsc_finalizadas = $con->query($sql_finalizadas);
			
			$pagina_anterior = $pagina_actual - 1;
	
			$pagina_siguiente = $pagina_actual + 1;
			
			$respuesta = $total_finalizadas['total'] % $registros_por_pagina;
			
			$ultima_pagina = $total_finalizadas['total'] / $registros_por_pagina;
			
			if($respuesta > 0) {
				$ultima_pagina = floor($ultima_pagina) + 1;
			}
		}
	}
}
?>
<div id="AlertOverlay" class="sweet-overlay"></div>
<div class="container logeed">
    <div class="row">
    <?php if($incidencias == 1) { ?>
    <div class="well well-sm incidencias">
    	<ol class="breadcrumb">
       		<li>Incidencias</li>
        </ol>
        <form id="frmSchInc" class="form-inline">
        <div class="btn-group">
        	<button type="button" class="btn btn-gac" data-toggle="dropdown" id="TxtIncSch" style="display:inline-block; width:120px" <?= ($total_incidencias['total'] > 0) ? '' : 'disabled'; ?>>Buscar por</button>
          	<button type="button" class="btn btn-gac dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-bottom-right-radius:0; border-top-right-radius:0; border-right:none" <?= ($total_incidencias['total'] > 0) ? '' : 'disabled'; ?>>
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          	</button>
          	<ul class="dropdown-menu" id="SchInc">
            	<li><a href="#" data-type="n_incidencia">Nº Incidencia</a></li>
                <li><a href="#" data-type="fecha">Fecha</a></li>
                <li><a href="#" data-type="tecnico">Técnico</a></li>
                <li><a href="#" data-type="tipo">Tipo</a></li>
                <li><a href="#" data-type="estado">Estado</a></li>
          	</ul>
			
            <div class="input-group">
            	<div id="ShowSchDefaultInc">
          			<input type="text" class="form-control" size="40" placeholder="&laquo;-- Selecciona el tipo de búsqueda" style="border-bottom-left-radius:0; border-top-left-radius:0" disabled>
                </div>
                <div id="ShowSchNincidenciaInc">
          			<input type="text" id="NinSchInc" class="form-control" placeholder="introduce un nº incidencia..." size="40" style="border-bottom-left-radius:0; border-top-left-radius:0">
                </div>
                <div id="ShowSchFechaInc">
                	<input type="text" id="FcSchInc1" class="form-control" placeholder="Desde..." style="border-bottom-left-radius:0; border-top-left-radius:0; display:inline-block; width:50%">
                    <input type="text" id="FcSchInc2" class="form-control" placeholder="...Hasta" style="border-bottom-right-radius:0; border-top-right-radius:0; border-left:none; display:inline-block; width:50%">
                </div>
                <div id="ShowSchTecnicoInc">
                	<div class="select-style" style="min-width:250px">
                	<select id="TecSchInc">
                    <option value="" selected hidden>--&raquo; Selecciona un técnico &laquo;--</option>
                    	<?php while($schtecinc = $rsc_tecnicos->fetch_assoc()) { ?>
                    	<option value="<?= strtolower($schtecinc['nombre']); ?>"><?= $schtecinc['nombre']; ?></option>
                        <?php } ?>
                    </select>
                    </div>
                </div>
                <div id="ShowSchTipoInc">
                	<div class="select-style" style="min-width:300px">
                	<select id="TipSchInc">
                    <option value="" selected hidden>--&raquo; Selecciona tipo de incidencia &laquo;--</option>
                    	<option value="aviso telefono">Aviso Teléfono</option>
                        <option value="sat">SAT</option>
                        <option value="comercial">Comercial</option>
                        <option value="devolucion llamada">Devolución Llamada</option>
                    </select>
                    </div>
                </div>
                <div id="ShowSchEstadoInc">
                	<div class="select-style" style="min-width:250px">
                	<select id="EstSchInc">
                    <option value="" selected hidden>--&raquo; Selecciona un estado &laquo;--</option>
                    	<option value="sin empezar">Sin Empezar</option>
                        <option value="en curso">En Curso</option>
                        <option value="interrumpida">Interrumpida</option>
                        <option value="falta material">Falta Material</option>
                        <option value="urgente">Urgente</option>
                    </select>
                    </div>
                </div>
          		<span class="input-group-btn">
            		<button id="BtnSchInc" class="btn btn-gac" type="button" <?= ($total_incidencias['total'] > 0) ? '' : 'disabled'; ?>>
                    	<i class="glyphicon glyphicon-search"></i>
                    </button>
          		</span>
        	</div>
		</div>
        </form>
        
        <div class="btnRight">
        	<a href="incidencias/agregar"><button type="button" class="btn btn-gac">Añadir Incidencia</button></a>
            <a href="incidencias/finalizadas"><button class="btn btn-gac">Ver Incidencias Finalizadas</button></a>
            <button type="button" class="btn btn-gac exportPdf" data-tbl="incidencias" <?= ($total_incidencias['total'] > 0) ? '' : 'disabled'; ?>>
            	<i class="glyphicon glyphicon-print"></i>
            </button>
        </div>
        
        <div id="TableResponsive" class="table-responsive">
        <table id="tblIncidencias" class="table table-bordered">
			<thead>
				<tr class="info">
					<th>#</th>
                    <th>Fecha</th>
                    <th>Cliente</th>
                    <th>Asunto</th>
                    <th>Técnico</th>
                    <th>Tipo</th>
                    <th>Estado</th>
                    <th>Acción</th>
				</tr>
			</thead>
            <tbody>
            <?php if($total_urge_incidencias['total'] > 0 || $total_incidencias['total'] > 0) { ?>
            	<?php
				if($total_urge_incidencias['total'] > 0) {
					 $posicion_urge = 0;
				 }
				 $posicion_urge++;
				 ?>
				<?php while($incidencias_urge = $rsc_urge->fetch_array()) { ?>
                <?php if($incidencias_urge['estado'] != 'finalizada') { ?>
                <input type="hidden" name="AsgIncID" value="<?= $incidencias_urge['id']; ?>">
                 <?php
                 $separar_fecha_urge = explode(' ', $incidencias_urge['fecha']);
                 $formato_fecha_urge = explode('-', $separar_fecha_urge[0]);
                 $incidencias_urge['fecha'] = $formato_fecha_urge[2].'/'.$formato_fecha_urge[1].'/'.$formato_fecha_urge[0];
                 ?>
                <?php if($total_urge_incidencias['total'] > 0) { ?>
                <tr class="danger">
                    <td><?= $posicion_urge; ?></td>
                    <td><?= $incidencias_urge['fecha']; ?></td>
                    <td><?= RecTexto(mb_strtoupper($incidencias_urge['cliente'], 'utf-8'), 12); ?></td>
                    <td><?= RecTexto(mb_strtoupper($incidencias_urge['descripcion'], 'utf-8'), 12); ?></td>
                    <td><?= ($incidencias_urge['tecnico'] != '') ? RecTexto(mb_strtoupper($incidencias_urge['tecnico'], 'utf-8'), 12) : '--'; ?></td>
                    <td><?= RecTexto(mb_strtoupper($incidencias_urge['tipo'], 'utf-8'), 12); ?></td>
                    <td><?= RecTexto(mb_strtoupper($incidencias_urge['estado'], 'utf-8'), 12); ?></td>
                    <td>
                    <div class="btn-group ActionResponsive">
						<button type="button" class="btn btn-default dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="position:relative">
                        	<i class="glyphicon glyphicon-cog"></i> <span class="caret"></span>
                   		</button>
						<ul class="dropdown-menu dropdown-menu-right">
                            <li><a class="AsgTInc" data-id="<?= $incidencias_urge['id']; ?>" style="cursor:pointer"><i class=" glyphicon glyphicon-user" style="font-size:12px"></i> Asignar técnico</a></li>
                            <li><a class="VerInc" data-id="<?= $incidencias_urge['n_incidencia']; ?>" style="cursor:pointer"><i class=" glyphicon glyphicon-eye-open" style="font-size:12px"></i> Ver incidencia</a></li>
                            <li><a href="incidencias/editar-<?= $incidencias_urge['n_incidencia']; ?>"><i class="glyphicon glyphicon-pencil" style="font-size:12px"></i> Modificar incidencia</a></li>
                            <li><a class="FinalizarInc" data-id="<?= $incidencias_urge['n_incidencia']; ?>" style="cursor:pointer"><i class="glyphicon glyphicon-check"></i> Finalizar incidencia</a></li>
                            <?php if($row['admin'] == 1) { ?>
                            <li><a class="DelInc" data-id="<?= $incidencias_urge['n_incidencia']; ?>" style="cursor:pointer"><i class="glyphicon glyphicon-remove"></i> Eliminar incidencia</a></li>
                        	<?php } ?>
						</ul>
                    </div>
                    
                    <div class="ActionNoResponsive">
                    <a class="AsgTInc" data-id="<?= $incidencias_urge['id']; ?>" style="color:#333; margin-right:15px; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Asignar Técnico"><i class="glyphicon glyphicon-user" style="font-size:12px"></i></a>
                    <a class="VerInc" data-id="<?= $incidencias_urge['n_incidencia']; ?>" style="color:#333; margin-right:15px; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Ver Incidencia"><i class=" glyphicon glyphicon-eye-open" style="font-size:12px"></i></a>
                    <a href="incidencias/editar-<?= $incidencias_urge['n_incidencia']; ?>" style="color:#333; margin-right:15px" data-toggle="tooltip" data-placement="bottom" title="Editar Incidencia"><i class="glyphicon glyphicon-pencil" style="font-size:12px"></i></a>
                    <a class="FinalizarInc" data-id="<?= $incidencias_urge['n_incidencia']; ?>" style="color:#333; margin-right:15px; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Finalizar Incidencia"><i class="glyphicon glyphicon-check" style="font-size:12px"></i></a>
                    <?php if($row['admin'] == 1) { ?>
                    <a class="DelInc" data-id="<?= $incidencias_urge['n_incidencia']; ?>" style="color:#333; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Eliminar Incidencia"><i class="glyphicon glyphicon-remove"></i></a>
                    <?php } $posicion_urge++; ?>
                    </div>
                    </td>
                </tr>
                <?php } ?>
                <?php } ?>
                <?php } ?>
                <?php
				if($total_incidencias['total'] > 0) {
					if($total_urge_incidencias['total'] > 0) {
						$posicion = $total_urge_incidencias['total'];
					}else {
						$posicion = 0;
					}
					if(!empty($_REQUEST['pg']) && $_REQUEST['pg'] > 1) {
						$posicion = $posicion + 1;
					}
				}
				?>
            	<?php while($incidencias = $rsc->fetch_array()) { ?>
                <?php if($incidencias['estado'] != 'finalizada') { ?>
                <input type="hidden" name="AsgIncID" value="<?= $incidencias['id']; ?>">
                <?php
				$separar_fecha = explode(' ', $incidencias['fecha']);
				$formato_fecha = explode('-', $separar_fecha[0]);
				$incidencias['fecha'] = $formato_fecha[2].'/'.$formato_fecha[1].'/'.$formato_fecha[0];
				if($incidencias['estado'] == 'en curso') {
					$class_estado = 'success';
					$class_accion = '#333';
				}else if($incidencias['estado'] == 'interrumpida') {
					$class_estado = 'warning';
					$class_accion = '#333';
				}else {
					$class_estado = '';
					$class_accion = '#333';
				}
				$posicion++;
				?>
            	<tr class="<?= $class_estado; ?>">
                	<td><?= $posicion; ?></td>
                    <td><?= $incidencias['fecha']; ?></td>
                    <td><?= RecTexto(mb_strtoupper($incidencias['cliente'], 'utf-8'), 12); ?></td>
                    <td><?= RecTexto(mb_strtoupper($incidencias['descripcion'], 'utf-8'), 12); ?></td>
                    <td><?= ($incidencias['tecnico'] != '') ? RecTexto(mb_strtoupper($incidencias['tecnico'], 'utf-8'), 12) : '--'; ?></td>
                    <td><?= RecTexto(mb_strtoupper($incidencias['tipo'], 'utf-8'), 12); ?></td>
                    <td><?= RecTexto(mb_strtoupper($incidencias['estado'], 'utf-8'), 12); ?></td>
                    <td>
                        <div class="btn-group ActionResponsive">
                            <button type="button" class="btn btn-default dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="position:relative"><i class="glyphicon glyphicon-cog"></i> <span class="caret"></span></button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a class="AsgTInc" data-id="<?= $incidencias['id']; ?>" style="cursor:pointer"><i class=" glyphicon glyphicon-user" style="font-size:12px"></i> Asignar Técnico</a></li>
                                <li><a class="VerInc" data-id="<?= $incidencias['n_incidencia']; ?>" style="cursor:pointer"><i class=" glyphicon glyphicon-eye-open" style="font-size:12px"></i> Ver Incidencia</a></li>
                                <li><a href="incidencias/editar-<?= $incidencias['n_incidencia']; ?>"><i class="glyphicon glyphicon-pencil" style="font-size:12px"></i> Modificar Incidencia</a></li>
                                <li><a class="FinalizarInc" data-id="<?= $incidencias['n_incidencia']; ?>" style="cursor:pointer"><i class="glyphicon glyphicon-check"></i> Finalizar incidencia</a></li>
                                <?php if($row['admin'] == 1) { ?>
                                <li><a class="DelInc" data-id="<?= $incidencias['n_incidencia']; ?>" style="cursor:pointer"><i class="glyphicon glyphicon-remove"></i> Eliminar Incidencia</a></li>
                                <?php } ?>
                            </ul>
                        </div>
                        
                        <div class="ActionNoResponsive">
                        <a class="AsgTInc" data-id="<?= $incidencias['id']; ?>" style="color:<?= $class_accion; ?>; margin-right:15px; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Asignar técnico"><i class=" glyphicon glyphicon-user" style="font-size:12px"></i></a>
                        <a class="VerInc" data-id="<?= $incidencias['n_incidencia']; ?>" style="color:<?= $class_accion; ?>; margin-right:15px; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Ver incidencia"><i class=" glyphicon glyphicon-eye-open" style="font-size:12px"></i></a>
                        <a href="incidencias/editar-<?= $incidencias['n_incidencia']; ?>" style="color:<?= $class_accion; ?>; margin-right:15px" data-toggle="tooltip" data-placement="bottom" title="Editar incidencia"><i class="glyphicon glyphicon-pencil" style="font-size:12px"></i></a>
                        <a class="FinalizarInc" data-id="<?= $incidencias['n_incidencia']; ?>" style="color:<?= $class_accion; ?>; margin-right:15px; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Finalizar Incidencia"><i class="glyphicon glyphicon-check" style="font-size:12px"></i></a>
                        <?php if($row['admin'] == 1) { ?>
                        <a class="DelInc" data-id="<?= $incidencias['n_incidencia']; ?>" style="color:<?= $class_accion; ?>; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Eliminar incidencia"><i class="glyphicon glyphicon-remove"></i></a>
                        <?php } ?>
                        </div>
                    </td>
                </tr>
                <?php } ?>
                <?php } ?>
                <?php }else { ?>
                <tr>
                	<td colspan="8" align="center" style="padding:10px">No se han encontrado incidencias</td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        </div>
        
        <nav>
        	<?php if($ultima_pagina > 0) { ?>
            <div class="pull-left" style="margin:5px 0 0 5px">
                <span class="text-white">Página <strong><?= $pagina_actual; ?></strong> de <strong><?= $ultima_pagina; ?></strong></span>
            </div>
            <?php } ?>
  			<ul class="pager">
            	<?php if($ultima_pagina > 0) { ?>
					<?php if($pagina_actual > 1) { ?>
                    <li><a href="incidencias/?pg=<?= $pagina_anterior; ?>">Anterior</a></li>
                    <?php }else { ?>
                    <li class="disabled"><a langhref="#">Anterior</a></li>
                    <?php } ?>
                    <?php if($pagina_actual < $ultima_pagina) { ?>
                    <li><a href="incidencias/?pg=<?= $pagina_siguiente; ?>">Siguiente</a></li>
                    <?php }else { ?>
                    <li class="disabled"><a langhref="#">Siguiente</a></li>
                    <?php } ?>
                <?php } ?>
  			</ul>
		</nav>
    </div>
    <?php } ?>
    
    <?php if($incidencias_agr == 1) { ?>
    <div class="well well-sm incidencias">
    	<ol class="breadcrumb">
       		<li>Añadir nueva incidencia</li>
        </ol>
    	<a href="incidencias" class="btn btn-back">
       		<i class="glyphicon glyphicon-circle-arrow-left"></i> Volver atrás
    	</a>
        <hr>
        <form id="frmAIncidencias" method="post" class="form-horizontal">
        	<div class="form-group">
            	<div class="control-group">
                <label for="tomador" class="col-sm-2 control-label">Tomador</label>
                <div class="col-sm-10">
                	<div class="select-style">
                	<select id="tomador">
                    	<option value="" hidden selected>--</option>
                    	<?php while($agrtecnicosinc = $rsc_tecnicos->fetch_assoc()) { ?>
                    	<option value="<?= $agrtecnicosinc['nombre']; ?>"><?= $agrtecnicosinc['nombre']; ?></option>
                        <?php } ?>
                    </select>
                    </div>
				</div>
                </div>
			</div>
            
            <div class="form-group">
            	<div class="control-group">
                <label for="fecha" class="col-sm-2 control-label">Fecha y Hora</label>
                <div class="col-sm-10">
                	<table>
                    	<tr>
                    		<td>
                    			<input type="text" class="form-control" id="fecha" placeholder="__/__/____" style="width:170px" onkeyup="FormatoFecha(this,'/',patron,true)">
                    		</td>
                    	<td>
                                <div class="select-style time">
                                    <select id="hora">
                                        <option value="" hidden selected>--</option>
                                        <?php for($hora = 9; $hora < 22; $hora++) { ?>
                                        <option value="<?= $hora; ?>"><?= ($hora < 10) ? '0'.$hora : $hora; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                    		</td>
                    		<td>
                                <div class="select-style time">
                                    <select id="minuto">
                                        <option value="" hidden selected>--</option>
                                        <?php for($minuto = 0; $minuto < 60; $minuto++) { ?>
                                        <option value="<?= $minuto; ?>"><?= ($minuto < 10) ? '0'.$minuto : $minuto; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                    		</td>
                            <td>
                            	<a class="TodayDate"><i class="glyphicon glyphicon-calendar"></i> Fecha Hoy</a>
                            	<a class="TodayTime"><i class="glyphicon glyphicon-time"></i> Hora Actual</a>
                            </td>
                    	</tr>
                    </table>
				</div>
                </div>
			</div>
            
            <div class="form-group">
            	<div class="control-group">
                <label for="cliente" class="col-sm-2 control-label">Cliente / Empresa</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="cliente">
				</div>
                </div>
			</div>
            
            <div class="form-group">
            	<div class="control-group">
                <label for="solicitante" class="col-sm-2 control-label">Nombre de Solicitante</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="solicitante">
				</div>
                </div>
			</div>
            
            <div class="form-group">
                <div class="control-group">
                <label for="descripcion" class="col-sm-2 control-label">Descripción</label>
                <div class="col-sm-10">
                    <textarea class="form-control" id="descripcion" rows="1"></textarea>
				</div>
                </div>
			</div>
            
            <div class="form-group">
            	<div class="control-group">
                <label for="tipo" class="col-sm-2 control-label">Tipo Incidencia</label>
                <div class="col-sm-10">
                	<div class="select-style">
                    <select id="tipo">
                   	<option value="" selected hidden>--</option>
                    <option value="aviso telefono">Aviso Teléfono</option>
                    <option value="sat">Sat</option>
                    <option value="comercial">Comercial</option>
                    <option value="devolucion llamada">Devolución Llamada</option>
                    </select>
                    </div>
				</div>
                </div>
			</div>
            
            <div class="form-group">
            	<div id="ErrorUrgInc" class="control-group">
                    <label for="urgencia" class="col-sm-2 control-label">Urgencia</label>
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="btn-group sino" data-toggle="buttons">
                            <label id="btnSi" class="btn btn-default">
                                <input type="radio" name="urgente" id="urgeSi" value="Si"> Si
                            </label>
                            <label id="btnNo" class="btn btn-default">
                                <input type="radio" name="urgente" id="urgeNo" value="No"> No
                            </label>
                        </div>
                    </div>
                </div>
			</div>
            
            <div id="IncEstado" class="form-group" style="margin-top:-20px">
            	<div class="control-group">
                    <label for="estado" class="col-sm-2 control-label">Estado</label>
                    <div class="col-sm-10">
                        <div class="select-style">
                        <select id="estado">
                        	<option value="" hidden selected>--</option>
                            <option value="sin empezar">Sin Empezar</option>
                            <option value="en curso">En Curso</option>
                            <option value="interrumpida">Interrumpida</option>
                            <option value="falta material">Falta Material</option>
                        </select>
                        </div>
                    </div>
                </div>
			</div>
            
            <div class="form-group">
                <div class="col-sm-12">
                	<button type="button" id="BtnAIncidencia" class="btn btn-gac btn-block">Añadir incidencia</button>
                </div>
           	</div>
        </form>
    </div>
    <?php } ?>
    
    <?php if($incidencias_edt == 1) { ?>
    <div class="well well-sm incidencias">
    	<ol class="breadcrumb">
       		<li>Editar incidencia nº <?= $explode_args[1]; ?></li>
        </ol>
    	<a href="incidencias" class="btn btn-back">
       		<i class="glyphicon glyphicon-circle-arrow-left"></i> Volver atrás
    	</a>
        <hr>
        <?php if($edt_total > 0) { ?>
    	<form id="frmEIncidencias" method="post" class="form-horizontal">
        	<input type="hidden" id="incedt_id" value="<?= $rs_edt['id']; ?>">
        	<div class="form-group">
            	<div class="control-group">
                <label class="col-sm-2 control-label">Tomador</label>
                <div class="col-sm-10">
                	<div class="select-style">
                        <select id="incedt_tomador">
                            <option value="<?= $rs_edt['tomador']; ?>" hidden selected><?= $rs_edt['tomador']; ?></option>
                            <?php while($agrtecnicosinc = $rsc_tecnicos->fetch_assoc()) { ?>
                            <option value="<?= $agrtecnicosinc['nombre']; ?>"><?= $agrtecnicosinc['nombre']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
				</div>
                </div>
			</div>
            
            <div class="form-group">
            	<div class="control-group">
                <label class="col-sm-2 control-label">Fecha y Hora</label>
                <div class="col-sm-10">
                    <table>
                        <tr>
                            <td>
                                <input type="text" class="form-control" id="incedt_fecha" placeholder="__/__/____" value="<?= $edt_fecha; ?>" style="width:170px" onkeyup="FormatoFecha(this,'/',patron,true)">
                            </td>
                            <td>
                                <div class="select-style time">
                                    <select id="incedt_hora">
                                        <option value="<?= $edt_hora; ?>" hidden selected><?= $edt_hora; ?></option>
                                        <?php for($hora = 9; $hora < 22; $hora++) { ?>
                                        <option value="<?= $hora; ?>"><?= ($hora < 10) ? '0'.$hora : $hora; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <div class="select-style time">
                                    <select id="incedt_minuto">
                                        <option value="<?= $edt_minutos; ?>" hidden selected><?= $edt_minutos; ?></option>
                                        <?php for($minuto = 0; $minuto < 60; $minuto++) { ?>
                                        <option value="<?= $minuto; ?>"><?= ($minuto < 10) ? '0'.$minuto : $minuto; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </td>
                            <?php if($row['admin'] == 1) { ?>
                            <td>
                                <a class="TodayDate"><i class="glyphicon glyphicon-calendar"></i> Fecha Hoy</a>
                                <a class="TodayTime"><i class="glyphicon glyphicon-time"></i> Hora Actual</a>
                            </td>
                            <?php } ?>
                        </tr>
                    </table>
				</div>
                </div>
			</div>
            
            <div class="form-group">
            	<div class="control-group">
                <label class="col-sm-2 control-label">Cliente / Empresa</label>
                <div class="col-sm-10">
                	<input type="text" class="form-control" id="incedt_cliente" value="<?= $rs_edt['cliente']; ?>">
				</div>
                </div>
			</div>
            
            <div class="form-group">
            	<div class="control-group">
                <label class="col-sm-2 control-label">Nombre de Solicitante</label>
                <div class="col-sm-10">
                	<input type="text" class="form-control" id="incedt_solicitante" value="<?= $rs_edt['solicitante']; ?>">
				</div>
                </div>
			</div>
            
            <div class="form-group">
                <div class="control-group">
                <label class="col-sm-2 control-label">Descripción</label>
                <div class="col-sm-10">
                	<textarea class="form-control" id="incedt_descripcion" rows="1"><?= $rs_edt['descripcion']; ?></textarea>
				</div>
                </div>
			</div>
            
            <div class="form-group">
            	<div class="control-group">
                <label class="col-sm-2 control-label">Asignar Técnico</label>
                <div class="col-sm-10">
                	<div class="select-style" style="max-width:200px">
                    <select id="incedt_tecnico">
                    <?php if(!empty($rs_edt['tecnico'])) { ?>
                    <option value="<?= $rs_edt['tecnico']; ?>" selected><?= $rs_edt['tecnico']; ?></option>
                    <?php }else { ?>
                    <option value="" selected hidden>Sin asignar</option>
                    <?php } ?>
                    	<?php while($edttecnico = $redttecnico->fetch_assoc()) { ?>
                        <?php if($edttecnico['nombre'] != $rs_edt['tecnico']) { ?>
                    	<option value="<?= $edttecnico['nombre']; ?>"><?= $edttecnico['nombre']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                    </div>
				</div>
                </div>
			</div>
            
            <div class="form-group">
            	<div class="control-group">
                <label class="col-sm-2 control-label">Tipo Incidencia</label>
                <div class="col-sm-10">
                	<div class="select-style" style="max-width:200px">
                    <select id="incedt_tipo">
                    <option value="aviso telefono" <?= ($rs_edt['tipo'] == 'aviso telefono') ? 'selected' : ''; ?>>Aviso Teléfono</option>
                    <option value="sat" <?= ($rs_edt['tipo'] == 'sat') ? 'selected' : ''; ?>>Sat</option>
                    <option value="comercial" <?= ($rs_edt['tipo'] == 'comercial') ? 'selected' : ''; ?>>Comercial</option>
                    <option value="devolucion llamada" <?= ($rs_edt['tipo'] == 'devolucion llamada') ? 'selected' : ''; ?>>Devolución Llamada</option>
                    </select>
                    </div>
				</div>
                </div>
			</div>
            
            <div class="form-group">
            	<div class="control-group">
                <label class="col-sm-2 control-label">Urgencia</label>
                <div class="col-sm-offset-2 col-sm-10">
      				<div class="btn-group sino" data-toggle="buttons">
                    	<input type="hidden" id="urge_sel" value="<?= $rs_edt['urge']; ?>">
  						<label id="btnSi" class="btn btn-default" <?= ($rs_edt['urge'] == 1) ? 'style="background:#7EC142; color:#fff"' : ''; ?>>
    						<input type="radio" name="incedt_urgente" value="Si"> Si
  						</label>
  						<label id="btnNo" class="btn btn-default" <?= ($rs_edt['urge'] == 0) ? 'style="background:#F0592A; color:#fff; border:1px solid red"' : ''; ?>>
    						<input type="radio" name="incedt_urgente" value="No"> No
  						</label>
					</div>
    			</div>
                <script type="text/javascript">
				$(document).ready(function(){
					var EdtUrgencia = "<?= $rs_edt['urge']; ?>";
					if(EdtUrgencia === "1"){
						$("#ShowEdtEstado").hide();
					}else{
						$("#ShowEdtEstado").show();
					}
				});
				</script>
                </div>
			</div>
            
            <div id="ShowEdtEstado" class="form-group">
                <div class="control-group">
                    <label class="col-sm-2 control-label">Estado</label>
                    <div class="col-sm-10">
                        <div class="select-style">
                            <select id="incedt_estado">
                                <option value="sin empezar" <?= ($rs_edt['estado'] == 'sin empezar') ? 'selected' : ''; ?>>Sin Empezar</option>
                                <option value="en curso" <?= ($rs_edt['estado'] == 'en curso') ? 'selected' : ''; ?>>En Curso</option>
                                <option value="interrumpida" <?= ($rs_edt['estado'] == 'interrumpida') ? 'selected' : ''; ?>>Interrumpida</option>
                                <option value="falta material" <?= ($rs_edt['estado'] == 'falta material') ? 'selected' : ''; ?>>Falta Material</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-sm-12">
                	<button type="button" id="BtnEIncidencia" class="btn btn-gac btn-block">Guardar cambios</button>
                </div>
           	</div>
        </form>
        <?php }else { ?>
        <h3 class="text-white">La incidencia que intentas editar no se ha encontrado, puede ser por los siguientes motivos:</h3>
        <ul class="text-white" style="margin-top:15px">
        	<li><h4>Todavía no ha sido creada y no existe.</h4></li>
            <li><h4>Ya ha sido finalizada por un técnico, por lo que ya no es posible editarla.</h4></li>
            <li><h4>Ha sido eliminada por un técnico o un administrador.</h4></li>
        </ul>
        <br>
        <h5 class="text-white">Haz clic en "Volver atrás" para volver donde estabas o en el logo de la parte superior izquierda para volver a la página principal.</h5>
        <?php } ?>
    </div>
    <?php } ?>
    
    <?php if($incidencias_sch == 1) { ?>
    <div class="well well-sm incidencias">
    	<ol class="breadcrumb">
       		<li>Búsqueda incidencias</li>
        </ol>
        
    	<a href="incidencias" class="btn btn-back">
       		<i class="glyphicon glyphicon-circle-arrow-left"></i> Volver atrás
    	</a>
        <form id="frmSchInc" class="form-inline">
        <div class="btn-group">
            <button type="button" class="btn btn-gac" data-toggle="dropdown" id="TxtIncSch" style="display:inline-block; width:100px">Buscar por</button>
            <button type="button" class="btn btn-gac dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-bottom-right-radius:0; border-top-right-radius:0; border-right:none">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu" id="SchInc">
                <li><a href="#" data-type="n_incidencia">Nº Incidencia</a></li>
                <li><a href="#" data-type="fecha">Fecha</a></li>
                <li><a href="#" data-type="tecnico">Técnico</a></li>
                <li><a href="#" data-type="tipo">Tipo</a></li>
                <li><a href="#" data-type="estado">Estado</a></li>
            </ul>
            
            <div class="input-group">
                <div id="ShowSchDefaultInc">
                    <input type="text" class="form-control" size="40" placeholder="&laquo;-- Selecciona el tipo de búsqueda" style="border-bottom-left-radius:0; border-top-left-radius:0" disabled>
                </div>
                <div id="ShowSchNincidenciaInc">
                    <input type="text" id="NinSchInc" class="form-control" placeholder="introduce un nº incidencia..." size="40" style="border-bottom-left-radius:0; border-top-left-radius:0">
                </div>
                <div id="ShowSchFechaInc">
                    <input type="text" id="FcSchInc1" class="form-control" placeholder="Desde..." style="border-bottom-left-radius:0; border-top-left-radius:0; display:inline-block; width:50%">
                    <input type="text" id="FcSchInc2" class="form-control" placeholder="...Hasta" style="border-bottom-right-radius:0; border-top-right-radius:0; border-left:none; display:inline-block; width:50%">
                </div>
                <div id="ShowSchTecnicoInc">
                    <div class="select-style">
                    <select id="TecSchInc">
                    <option value="" selected hidden>--&raquo; Selecciona un técnico &laquo;--</option>
                        <?php while($schtecnicosinc = $rsc_tecnicos->fetch_assoc()) { ?>
                        <option value="<?= strtolower($schtecnicosinc['nombre']); ?>"><?= $schtecnicosinc['nombre']; ?></option>
                        <?php } ?>
                    </select>
                    </div>
                </div>
                <div id="ShowSchTipoInc">
                    <div class="select-style">
                    <select id="TipSchInc">
                    <option value="" selected hidden>--&raquo; Selecciona tipo de incidencia &laquo;--</option>
                        <option value="aviso telefono">Aviso Teléfono</option>
                        <option value="sat">SAT</option>
                        <option value="comercial">Comercial</option>
                        <option value="devolucion llamada">Devolución Llamada</option>
                    </select>
                    </div>
                </div>
                <div id="ShowSchEstadoInc">
                    <div class="select-style">
                    <select id="EstSchInc">
                    <option value="" selected hidden>--&raquo; Selecciona un estado &laquo;--</option>
                        <option value="sin empezar">Sin Empezar</option>
                        <option value="en curso">En Curso</option>
                        <option value="interrumpida">Interrumpida</option>
                        <option value="falta material">Falta Material</option>
                        <option value="urgente">Urgente</option>
                    </select>
                    </div>
                </div>
                <span class="input-group-btn">
                    <button id="BtnSchInc" class="btn btn-gac" type="button">
                    	<i class="glyphicon glyphicon-search"></i>
                    </button>
                </span>
            </div>
        </div>
        </form>
     
     	<div class="pull-right text-default" style="font-size:18px">
            Se han encontrado <strong><?= $total_schincidencias['total']; ?></strong> resultados
        </div>
        
        <div id="TableResponsive" class="table-responsive">
        <table class="table table-bordered">
			<thead>
				<tr class="info">
                    <th>Fecha</th>
                    <th>Cliente</th>
                    <th>Técnico</th>
                    <th>Tipo</th>
                    <th>Estado</th>
                    <th>Acción</th>
				</tr>
			</thead>
            <tbody>
            	<?php if($total_schincidencias['total'] > 0) { ?>
            	<?php while($rs_schincidencias = $rsc_schincidencias->fetch_assoc()) { ?>
				<?php
				if($rs_schincidencias['estado'] == 'en curso') {
					$class_estado = 'success';
					$class_accion = '#fff';
				}else if($rs_schincidencias['estado'] == 'interrumpida') {
					$class_estado = 'warning';
					$class_accion = '#fff';
				}else {
					$class_estado = '';
					$class_accion = '#333';
				}
				
				$sep_fechaschinc = explode(' ', $rs_schincidencias['fecha']);
				$fechaschinc = explode('-', $sep_fechaschinc[0]);
				$rs_schincidencias['fecha'] = $fechaschinc[2].'/'.$fechaschinc[1].'/'.$fechaschinc[0];
				?>
            	<tr class="<?= $class_estado; ?>">
                    <td><?= $rs_schincidencias['fecha']; ?></td>
                    <td><?= RecTexto(mb_strtoupper($rs_schincidencias['cliente'], 'utf-8'), 12); ?></td>
                    <td><?= RecTexto(mb_strtoupper($rs_schincidencias['tecnico'], 'utf-8'), 12); ?></td>
                    <td><?= RecTexto(mb_strtoupper($rs_schincidencias['tipo'], 'utf-8'), 12); ?></td>
                    <td><?= RecTexto(mb_strtoupper($rs_schincidencias['estado'], 'utf-8'), 12); ?></td>
                    <td>
                    <div class="btn-group ActionResponsive">
						<button type="button" class="btn btn-default dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="position:relative">-- <span class="caret"></span></button>
						<ul class="dropdown-menu dropdown-menu-right">
                            <li><a class="AsgTInc" data-id="<?= $rs_schincidencias['id']; ?>" style="cursor:pointer"><i class=" glyphicon glyphicon-user" style="font-size:12px"></i> Asignar Técnico</a></li>
                            <li><a class="VerInc" data-id="<?= $rs_schincidencias['n_incidencia']; ?>" style="cursor:pointer"><i class=" glyphicon glyphicon-eye-open" style="font-size:12px"></i> Ver Incidencia</a></li>
                            <li><a href="incidencias/editar-<?= $rs_schincidencias['n_incidencia']; ?>"><i class="glyphicon glyphicon-pencil" style="font-size:12px"></i> Modificar Incidencia</a></li>
                            <?php if($row['admin'] == 1) { ?>
                            <li><a class="DelInc" data-id="<?= $rs_schincidencias['n_incidencia']; ?>" style="cursor:pointer"><i class="glyphicon glyphicon-remove"></i> Eliminar Incidencia</a></li>
                        	<?php } ?>
						</ul>
                    </div>
                    
                    <div class="ActionNoResponsive">
                    <a class="AsgTInc" data-id="<?= $rs_schincidencias['id']; ?>" style="color:<?= $class_accion; ?>; margin-right:15px; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Asignar técnico"><i class=" glyphicon glyphicon-user" style="font-size:12px"></i></a>
                    <a class="VerInc" data-id="<?= $rs_schincidencias['n_incidencia']; ?>" style="color:<?= $class_accion; ?>; margin-right:15px; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Ver incidencia"><i class=" glyphicon glyphicon-eye-open" style="font-size:12px"></i></a>
                    <a href="incidencias/editar-<?= $rs_schincidencias['n_incidencia']; ?>" style="color:<?= $class_accion; ?>; margin-right:15px" data-toggle="tooltip" data-placement="bottom" title="Editar incidencia"><i class="glyphicon glyphicon-pencil" style="font-size:12px"></i></a>
                    <?php if($row['admin'] == 1) { ?>
                    <a class="DelInc" data-id="<?= $rs_schincidencias['n_incidencia']; ?>" style="color:<?= $class_accion; ?>; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Eliminar incidencia"><i class="glyphicon glyphicon-remove"></i></a>
                    <?php } ?>
                    </div>
                    </td>
                </tr>
				<?php } ?>
                <?php }else { ?>
                <tr>
                	<td align="left" colspan="6">No se han encontrado resultados de su búsqueda</td>
                </tr>
                <?php } ?>
            </tbody>
       </table>
       </div>
    </div>
    <?php } ?>
    
    <?php if($incidencias_fin == 1) { ?>
    <div class="well well-sm incidencias"> 
    	<ol class="breadcrumb">
       		<li>Historial incidencias finalizadas</li>
        </ol>
        
		<a href="incidencias">
        	<button class="btn btn-back"><i class="glyphicon glyphicon-circle-arrow-left"></i> Volver atrás</button>
        </a>
        
        <?php if($row['admin'] == 1) { ?>
        <div class="btnRight">
        	<a>
            	<button id="DelHisAllInc" class="btn btn-gac" <?= ($total_finalizadas['total'] > 0) ? '' : 'disabled'; ?>>
            		Eliminar todas las incidencias finalizadas
              	</button>
          	</a>
        </div>
        <?php } ?>
        
        <div id="TableResponsive" class="table-responsive">
        <table class="table table-bordered">
			<thead>
				<tr class="info">
                    <th>Nº Incidencia</th>
                    <th>Cliente</th>
                    <th>Asunto</th>
                    <th>Técnico</th>
                    <th>Fecha</th>
                    <th>Facturado</th>
                    <th>Acción</th>
				</tr>
			</thead>
            <tbody>
            <?php if($total_finalizadas['total'] > 0) { ?>
				<?php while($rs_finalizadas = $rsc_finalizadas->fetch_assoc()) { ?>
                <?php
                $sql_tfacturado = 'SELECT SUM(facturado) as total_facturado
                                   FROM incidencias';
                $rsc_tfacturado = $con->query($sql_tfacturado);
                $rs_tfacturado  = $rsc_tfacturado->fetch_assoc();
                
                $fechafin = explode(' ', $rs_finalizadas['fecha_fin']);
                $fechafin = explode('-', $fechafin[0]);
                $fechafin = $fechafin[2].'/'.$fechafin[1].'/'.$fechafin[0];
                $rs_finalizadas['fecha_fin'] = $fechafin;
                ?>
                <tr>
                    <td><?= $rs_finalizadas['n_incidencia']; ?></td>
                    <td><?= RecTexto(mb_strtoupper($rs_finalizadas['cliente'], 'utf-8'), 12); ?></td>
                    <td><?= RecTexto(mb_strtoupper($rs_finalizadas['descripcion'], 'utf-8'), 12); ?></td>
                    <td><?= ($rs_finalizadas['tecnico_fin']) ? RecTexto(mb_strtoupper($rs_finalizadas['tecnico_fin'], 'utf-8'), 12) : '--'; ?></td>
                    <td><?= $rs_finalizadas['fecha_fin']; ?></td>
                    <td><?= ($rs_finalizadas['facturado'] <= 0) ? '/' : $rs_finalizadas['facturado']; ?></td>
                    <td>
                    <div class="btn-group ActionResponsive">
						<button type="button" class="btn btn-default dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="position:relative">-- <span class="caret"></span></button>
						<ul class="dropdown-menu dropdown-menu-right">
                            <li><a class="VerInc" data-id="<?= $rs_finalizadas['n_incidencia']; ?>" style="cursor:pointer"><i class="glyphicon glyphicon-eye-open" style="font-size:12px"></i> Ver Incidencia</a></li>
                            <?php if($row['admin'] == 1) { ?>
                            <li><a class="DelInc" data-id="<?= $rs_finalizadas['n_incidencia']; ?>" style="cursor:pointer"><i class="glyphicon glyphicon-remove"></i> Eliminar Incidencia</a></li>
                        	<?php } ?>
						</ul>
                    </div>
                    
                    <div class="ActionNoResponsive">
                    <a class="VerInc" data-id="<?= $rs_finalizadas['n_incidencia']; ?>" style="color:#333; margin-right:15px; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Ver incidencia"><i class=" glyphicon glyphicon-eye-open" style="font-size:12px"></i></a>
                    <?php if($row['admin'] == 1) { ?>
                    <a class="DelInc" data-id="<?= $rs_finalizadas['n_incidencia']; ?>" style="color:#333; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Eliminar incidencia"><i class="glyphicon glyphicon-remove"></i></a>
                    <?php } $posicion_urge++; ?>
                    </div>
                    </td>
                </tr>
                <?php } ?>
            <?php }else { ?>
            	<tr>
                	<td colspan="7" align="center" style="padding:10px">No hay incidencias finalizadas para mostrar</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        </div>
        
        <nav>
        	<?php if($total_finalizadas['total'] > 0) { ?>
            <div class="pull-left" style="margin:10px 0 0 0">
                <span class="text-white">Página <strong><?= $pagina_actual; ?></strong> de <strong><?= $ultima_pagina; ?></strong></span>
            </div>
            <?php } ?>
  			<ul class="pager">
            	<?php if($ultima_pagina > 0) { ?>
					<?php if($pagina_actual > 1) { ?>
                    <li><a href="incidencias/finalizadas/?pg=<?= $pagina_anterior; ?>">Anterior</a></li>
                    <?php }else { ?>
                    <li class="disabled"><a langhref="#">Anterior</a></li>
                    <?php } ?>
                    <?php if($pagina_actual < $ultima_pagina) { ?>
                    <li><a href="incidencias/finalizadas/?pg=<?= $pagina_siguiente; ?>">Siguiente</a></li>
                    <?php }else { ?>
                    <li class="disabled"><a langhref="#">Siguiente</a></li>
                    <?php } ?>
                <?php } ?>
  			</ul>
		</nav>
    </div>
    <?php } ?>
    
    <!-- Asignar tecnico -->
    <div id="AlertIncAsgTecnico" data-animation="pop" class="hidden">
        <h2><i class='glyphicon glyphicon-wrench' style='font-size:35px'></i> <span>Asignar Técnico</span></h2>
        
        <div class="form-group">
            <div class="control-group">
                <div align="center" class="col-sm-12">
                	<div class="select-style lg">
                    <select id="AsgTecIncSel">
                    <option value="" selected>--</option>
                        <?php while($rsasgtecnico = $asg_tecnicos->fetch_assoc()) { ?>
                        <option value="<?= $rsasgtecnico['nombre']; ?>"><?= mb_strtoupper($rsasgtecnico['nombre'], 'utf-8'); ?></option>
                        <?php } ?>
                    </select>
                    </div>
                </div>
            </div>
        </div>
    	
       <div class="col-sm-12" style="margin:0; padding:0">
            <p style="display:block">&nbsp;</p>
            <div id="AsgTinputError" class="sa-error-container">
                <div class="icon">!</div>
                <p>¡Debes seleccionar un técnico!</p>
            </div>
        </div>
        
        <div class="sa-button-container">
            <button class="cancel" id="CancelAsgTinc" tabindex="2">Cancelar</button>
            <div class="sa-confirm-button-container">
                <button class="confirm" id="ConfirmAsgTinc">Confirmar</button>
            </div>
        </div>
    </div>
    </div>
</div>

<!-- Ver incidencias -->
<div class="modal fade" id="ShowVerInc" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border:none; margin-bottom:-20px">
                <h3 class="modal-title" id="myModalLabel">VER INCIDENCIA Nº <span id="IDIncVer"></span></h3>
            </div>
            <div id="ResultVerInc" class="modal-body" style="text-transform:uppercase"></div>
            <div class="modal-footer" style="text-align:center; border:none">
                <button type="button" class="btn btn-gac" data-dismiss="modal">Cerrar ventana</button>
            </div>
        </div>
    </div>
</div>
        
<!-- Finalizar incidencia -->
<div class="modal fade finIncidenciaEdt" data-toggle="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border:none">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">FINALIZAR INCIDENCIA Nº <span id="NincFinModal"></span></h3>
                <p class="text-muted">Antes de finalizar la incidencia, por favor, rellene los campos a continuación.</p>
                <form id="frmMFIncidencias" method="post" class="form-horizontal">
                    <div class="form-group">
            <div class="control-group">
            <label for="tecnico" class="col-sm-3 control-label" style="background:none">Técnico</label>
            <div class="col-sm-9">
                <div class="select-style">
                    <select id="edtftecnico">
                    <option value="" hidden selected>--</option>
                        <?php while($rsfn_tecnicos = $fn_tecnicos->fetch_assoc()) { ?>
                        <option value="<?= $rsfn_tecnicos['nombre']; ?>"><?= $rsfn_tecnicos['nombre']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            </div>
        </div>
        
        <div class="form-group">
            <div class="control-group">
            <label for="fecha" class="col-sm-3 control-label" style="background:none">Fecha y Hora</label>
            <div class="col-sm-9">
            	<table>
                	<tr>
                    	<td>
                			<input type="text" class="form-control" id="edtffecha" placeholder="__/__/____" style="width:170px" onkeyup="FormatoFecha(this,'/',patron,true)">
                      	</td>
                        <td>
                            <div class="select-style time">
                                <select id="edtfhora">
                                	<option value="" hidden selected>--</option>
                                    <?php for($hora = 9; $hora < 22; $hora++) { ?>
                                    <option value="<?= $hora; ?>"><?= ($hora < 10) ? '0'.$hora : $hora; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                     	</td>
                        <td>
                            <div class="select-style time">
                                <select id="edtfminutos">
                                	<option value="" hidden selected>--</option>
                                    <?php for($minuto = 0; $minuto < 60; $minuto++) { ?>
                                    <option value="<?= $minuto; ?>"><?= ($minuto < 10) ? '0'.$minuto : $minuto; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                    	</td>
                        <td>
                            <a class="TodayAll"><i class="glyphicon glyphicon-calendar"></i> Fecha y Hora</a>
                        </td>
                	</tr>
                </table>
            </div>
            </div>
        </div>
        
        <div class="form-group">
            <div class="control-group">
                <label for="solucion" class="col-sm-3 control-label" style="background:none">Solución</label>
                <div class="col-sm-9">
                    <textarea class="form-control" id="edtfsolucion" rows="1"></textarea>
                </div>
            </div>
        </div>
        
        <div class="form-group">
            <div class="control-group">
                <label for="facturado" class="col-sm-3 control-label" style="background:none">Facturado</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="edtffacturado" placeholder="0" onpaste="return false;" style="max-width:125px">
                </div>
            </div>
        </div>
        <button type="button" id="BtnEdtFIncidencia" class="btn btn-gac btn-block">Finalizar incidencia</button>
    </div>
            
            
    	</form>
    	</div>
    	</div>
    </div>
</div>