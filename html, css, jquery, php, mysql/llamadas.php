<?php
if(empty($_SESSION['ID']) || $show_lla != 1) {
	echo '<script>window.location="./";</script>';
	exit();
}

$registros_por_pagina = 20;
	
if(!empty($_REQUEST['pg'])) {
	$comienzo_pagina = ($_REQUEST['pg'] - 1) * $registros_por_pagina;
	$pagina_actual   = $_REQUEST['pg'];
}else {
	$comienzo_pagina = 0;
	$pagina_actual   = 1;
}

$sql = 'SELECT nombre
	    FROM tecnicos
		ORDER BY nombre ASC';
$rtecnicos = $con->query($sql);
$rcontecnicos = $con->query($sql);
$total_tecnicos = mysqli_num_rows($rtecnicos);

if($url['dir'] == 'llamadas') {
	$llamadas = 1;
	$llamadas_agr = 0;
	$llamadas_edt = 0;
	$llamadas_sch = 0;
	$llamadas_his = 0;
	
	$sql = 'SELECT quien
	    	FROM llamadas
			GROUP BY quien
			ORDER BY quien ASC';
	$rquien = $con->query($sql);
	
	$sql = 'SELECT COUNT(id) as total
	        FROM llamadas
			WHERE save = 0';
	$rsc = $con->query($sql);
	$total_llamadas = $rsc->fetch_assoc();
	
	$sql = 'SELECT id, tienda, tomador, dia, donde, quien, motivo, phone, con, view, inc
	        FROM llamadas
			WHERE save = 0
			ORDER BY id ASC
			LIMIT '.$comienzo_pagina.', '.$registros_por_pagina;
	$rsc = $con->query($sql);
	
	$pagina_anterior = $pagina_actual - 1;
	
	$pagina_siguiente = $pagina_actual + 1;
	
	$respuesta = $total_llamadas['total'] % $registros_por_pagina;
	
	$ultima_pagina = $total_llamadas['total'] / $registros_por_pagina;
	
	if($respuesta > 0) {
		$ultima_pagina = floor($ultima_pagina) + 1;
	}
	
	if(!empty($url['args'][0])) {
		$explode_args = explode('-', $url['args'][0]);
		
		if($explode_args[0] == 'agregar') {
			$llamadas = 0;
			$llamadas_agr = 1;
			$llamadas_edt = 0;
			$llamadas_sch = 0;
			$llamadas_his = 0;
		}
		if($explode_args[0] == 'editar') {
			$llamadas = 0;
			$llamadas_agr = 0;
			$llamadas_edt = 1;
			$llamadas_sch = 0;
			$llamadas_his = 0;
			
			$sql = 'SELECT id, tienda, tomador, dia, donde, quien, motivo, phone, con
			        FROM llamadas
					WHERE id = "'.$explode_args[1].'"
					LIMIT 1';
			$rsc_edt = $con->query($sql);
			$rs_edt = $rsc_edt->fetch_assoc();
			
			$edt_total = mysqli_num_rows($rsc_edt);
			
			$sep_edtlladiahora = explode(' ', $rs_edt['dia']);
			$sep_edtlladia = explode('-', $sep_edtlladiahora[0]);
			$edtlladia = $sep_edtlladia[2].'/'.$sep_edtlladia[1].'/'.$sep_edtlladia[0];
			$edtllahora = substr($sep_edtlladiahora[1], 0, -6);
			$edtllaminutos = substr($sep_edtlladiahora[1], 3, -3);
		}
		if($explode_args[0] == 'buscar') {
			$llamadas = 0;
			$llamadas_agr = 0;
			$llamadas_edt = 0;
			$llamadas_sch = 1;
			$llamadas_his = 0;
			
			$sql = 'SELECT COUNT(id) as total
	        		FROM llamadas
					WHERE '.$_REQUEST['type'].' LIKE "%'.$_REQUEST['sch'].'%"';
			$rsc = $con->query($sql);
			$total_schllamadas = $rsc->fetch_assoc();
			
			$sql = 'SELECT id, tienda, tomador, dia, donde, quien, motivo, phone, con
					FROM llamadas
					WHERE '.$_REQUEST['type'].' LIKE "%'.$_REQUEST['sch'].'%"
					ORDER BY id ASC
					LIMIT '.$comienzo_pagina.', '.$registros_por_pagina;
			$rsc_schllamadas = $con->query($sql);
			
			$pagina_anterior  = $pagina_actual - 1;
			
			$pagina_siguiente = $pagina_actual + 1;
			
			$respuesta        = $total_schllamadas['total'] % $registros_por_pagina;
			
			$ultima_pagina    = $total_schllamadas['total'] / $registros_por_pagina;
			
			if($respuesta > 0) {
				$ultima_pagina = floor($ultima_pagina) + 1;
			}
		}
		if($explode_args[0] == 'historial') {
			$llamadas = 0;
			$llamadas_agr = 0;
			$llamadas_edt = 0;
			$llamadas_sch = 0;
			$llamadas_his = 1;
			
			$sql = 'SELECT COUNT(id) as total
	        		FROM llamadas
					WHERE save = 1';
			$rsc = $con->query($sql);
			$total_hisllamadas = $rsc->fetch_assoc();
			
			$sql = 'SELECT id, tienda, tomador, dia, donde, quien, motivo, phone, con
					FROM llamadas
					WHERE save = 1
					ORDER BY id DESC
					LIMIT '.$comienzo_pagina.', '.$registros_por_pagina;
			$rsc_hisllamadas = $con->query($sql);
			
			$pagina_anterior  = $pagina_actual - 1;
			
			$pagina_siguiente = $pagina_actual + 1;
			
			$respuesta        = $total_hisllamadas['total'] % $registros_por_pagina;
			
			$ultima_pagina    = $total_hisllamadas['total'] / $registros_por_pagina;
			
			if($respuesta > 0) {
				$ultima_pagina = floor($ultima_pagina) + 1;
			}
		}
	}
}
?>
<div class="container">
	<div class="row">
    	<?php if($llamadas == 1) { ?>
    	<div class="well well-sm llamadas">
        	<ol class="breadcrumb">
                <li>Llamadas</li>
            </ol>
            <form id="frmSchllamadas" class="form-inline">
        	<div class="btn-group">
        	<button type="button" class="btn btn-gac" data-toggle="dropdown" id="TxtllamadasSch" style="display:inline-block" <?= ($total_llamadas['total'] > 0) ? '' : 'disabled'; ?>>Buscar por</button>
          	<button type="button" class="btn btn-gac dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-bottom-right-radius:0; border-top-right-radius:0; border-right:none" <?= ($total_llamadas['total'] > 0) ? '' : 'disabled'; ?>>
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          	</button>
          	<ul class="dropdown-menu" id="Schllamadas">
            	<li><a href="#" data-type="tomador">Tomador</a></li>
                <li><a href="#" data-type="dia">Día</a></li>
                <li><a href="#" data-type="quien">Quién</a></li>
                <li><a href="#" data-type="con">Hablar con</a></li>
          	</ul>
			
            <div class="input-group">
            	<div id="ShowSchDefaultllamadas">
          			<input type="text" class="form-control" size="40" placeholder="&laquo;-- Selecciona el tipo de búsqueda" style="border-bottom-left-radius:0; border-top-left-radius:0" disabled>
                </div>
                <div id="ShowSchTomadorllamadas">
                	<div class="select-style">
                	<select id="TomadorSchllamadas">
                    <option value="" selected hidden>--&raquo; Selecciona un tomador &laquo;--</option>
                    	<?php while($rstecnicos = $rtecnicos->fetch_assoc()) { ?>
                    	<option value="<?= strtolower($rstecnicos['nombre']); ?>"><?= $rstecnicos['nombre']; ?></option>
                        <?php } ?>
                    </select>
                    </div>
                </div>
                <div id="ShowSchDiallamadas">
                	<input type="text" class="form-control" id="DiaSchllamadas" placeholder="__/__/____" style="display:inline-block; width:100%" onkeyup="FormatoFecha(this,'/',patron,true)">
                </div>
                <div id="ShowSchConllamadas">
                	<input type="text" id="ConSchllamadas" class="form-control" placeholder="Introduce un nombre..." size="30" style="border-bottom-left-radius:0; border-top-left-radius:0">
                </div>
                <div id="ShowSchQuienllamadas">
                	<div class="select-style" style="min-width:200px">
                        <select id="QuienSchllamadas">
                        <option value="" selected hidden>--&raquo; Selecciona al cliente &laquo;--</option>
                            <?php while($rsquien = $rquien->fetch_assoc()) { ?>
                            <?php if($rsquien['quien'] != '') { ?>
                            <option value="<?= strtolower($rsquien['quien']); ?>"><?= $rsquien['quien']; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
          		<span class="input-group-btn">
            		<button id="BtnSchllamadas" class="btn btn-gac" type="button" <?= ($total_llamadas['total'] > 0) ? '' : 'disabled'; ?>>
                    	<i class="glyphicon glyphicon-search"></i>
                    </button>
          		</span>
        	</div>
		</div>
        </form>
            
            <div class="btnRight">
                <a href="llamadas/agregar"><button type="button" class="btn btn-gac">Añadir llamada</button></a>
                <a href="llamadas/historial"><button class="btn btn-gac">Ver Historial Llamadas</button></a>
                <button type="button" class="btn btn-gac exportPdf" data-tbl="llamadas" <?= ($total_llamadas['total'] > 0) ? '' : 'disabled'; ?>>
                    <i class="glyphicon glyphicon-print"></i>
                </button>
            </div>
            
            <div id="TableResponsive" class="table-responsive">
            <table id="tblLlamadas" class="table table-bordered">
                <thead>
                    <tr class="info">
                    	<th>Tienda</th>
                        <th>Tomador</th>
                        <th>Día</th>
                        <th>Hora</th>
                        <th>De donde...</th>
                        <th>Quién...</th>
                        <th>Motivo</th>
                        <th>Hablar con...</th>
                        <th>Acción</th>
                    </tr>
                </thead>
                <tbody>
                	<?php if($total_llamadas['total'] > 0) { ?>
						<?php while($rs_llamadas = $rsc->fetch_assoc()) { ?>
                        <?php
						$semanas = array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');
						$sep_lladiahora = explode(' ', $rs_llamadas['dia']);
						$sep_lladia = explode('-', $sep_lladiahora[0]);
						$lladia = $semanas[date('w', strtotime($sep_lladiahora[0]))];
						$llahora = substr($sep_lladiahora[1], 0, -3);
						
						if($rs_llamadas['tienda'] == 'sponsa') {
							$rs_llamadas['tienda'] = 'S. Ponsa';
						}
						?>
                        <tr class="<?= ($rs_llamadas['view'] == 1) ? 'success' : ''; ?>">
                        	<td><?= mb_strtoupper($rs_llamadas['tienda'], 'utf-8'); ?></td>
                            <td><?= mb_strtoupper($rs_llamadas['tomador'], 'utf-8'); ?></td>
                            <td><?= mb_strtoupper($lladia, 'utf-8').', '.$sep_lladia[2]; ?></td>
                            <td><?= $llahora; ?></td>
                            <td><?= ($rs_llamadas['donde']) ? RecTexto(mb_strtoupper($rs_llamadas['donde'], 'utf-8'), 12) : '--'; ?></td>
                            <td><?= ($rs_llamadas['quien']) ? RecTexto(mb_strtoupper($rs_llamadas['quien'], 'utf-8'), 12) : '--'; ?></td>
                            <td><?= RecTexto(mb_strtoupper($rs_llamadas['motivo'], 'utf-8'), 12); ?></td>
                            <td><?= ($rs_llamadas['con']) ? mb_strtoupper($rs_llamadas['con'], 'utf-8') : '--'; ?></td>
                            <td>
                            <div class="btn-group ActionResponsive">
                                <button type="button" class="btn btn-default dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="position:relative"><i class="glyphicon glyphicon-cog"></i> <span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                	<li>
                                    <a class="VerLlamada" data-id="<?= $rs_llamadas['id']; ?>" style="color:#333; cursor:pointer"><i class="glyphicon glyphicon-eye-open"></i> Ver llamada</a>
                                    </li>
                                    <li>
                                    <a class="PIncLlamada" data-nid="<?= $rs_llamadas['id']; ?>" style="color:#333; cursor:pointer"><i class="glyphicon glyphicon-share-alt"></i> Pasar a incidencias</a>
                                    </li>
                                    <li>
                                    <a href="llamadas/editar-<?= $rs_llamadas['id']; ?>" style="color:#333"><i class="glyphicon glyphicon-pencil" style="font-size:12px"></i> Editar llamada</a>
                                    </li>
                                    <li><a class="SavLlamada" data-id="<?= $rs_llamadas['id']; ?>" style="color:#333; cursor:pointer"><i class="glyphicon glyphicon-saved"></i> Guardar llamada</a></li>
                                    <?php if($row['admin'] == 1) { ?>
                                    <li><a class="DelLlamada" data-id="<?= $rs_llamadas['id']; ?>" style="color:#333; cursor:pointer"><i class="glyphicon glyphicon-remove"></i> Eliminar llamada</a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                            
                            <div class="ActionNoResponsive">
                            <a class="VerLlamada" data-id="<?= $rs_llamadas['id']; ?>" style="color:#333; margin-right:15px; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Ver Llamada"><i class="glyphicon glyphicon-eye-open"></i></a>
                            <a class="PIncLlamada" data-nid="<?= $rs_llamadas['id']; ?>" style="color:#333; margin-right:15px; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Pasar a Incidencias"><i class="glyphicon glyphicon-share-alt"></i></a>
                            <a href="llamadas/editar-<?= $rs_llamadas['id']; ?>" style="color:#333; margin-right:15px" data-toggle="tooltip" data-placement="bottom" title="Editar Llamada"><i class="glyphicon glyphicon-pencil" style="font-size:12px"></i></a>
                            <a class="SavLlamada" data-id="<?= $rs_llamadas['id']; ?>" style="color:#333; margin-right:15px; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Guardar Llamada"><i class="glyphicon glyphicon-saved" ></i></a>
                            <?php if($row['admin'] == 1) { ?>
                            <a class="DelLlamada" data-id="<?= $rs_llamadas['id']; ?>" style="color:#333; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Eliminar Llamada"><i class="glyphicon glyphicon-remove"></i></a>
                            <?php } ?>
                            </div>
                            </td>
                        </tr>
                        <?php } ?>
                    <?php }else { ?>
                    	<tr>
                        	<td colspan="9" align="center" style="padding:10px">No se han encontrado llamadas para mostrar</td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            </div>
            
            <nav>
            	<?php if($ultima_pagina > 0) { ?>
                <div class="pull-left" style="margin:5px 0 0 5px">
                    <span class="text-white">Página <strong><?= $pagina_actual; ?></strong> de <strong><?= $ultima_pagina; ?></strong></span>
                </div>
                <?php } ?>
                
                <ul class="pager">
                    <?php if($ultima_pagina > 0) { ?>
                        <?php if($pagina_actual > 1) { ?>
                        <li><a href="llamadas/?pg=<?= $pagina_anterior; ?>">Anterior</a></li>
                        <?php }else { ?>
                        <li class="disabled"><a langhref="#">Anterior</a></li>
                        <?php } ?>
                        <?php if($pagina_actual < $ultima_pagina) { ?>
                        <li><a href="llamadas/?pg=<?= $pagina_siguiente; ?>">Siguiente</a></li>
                        <?php }else { ?>
                        <li class="disabled"><a langhref="#">Siguiente</a></li>
                        <?php } ?>
                    <?php } ?>
                </ul>
			</nav>
    	</div>
        
        <div class="modal fade" id="ShowPasLlamadaInc">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="border:none">
                    	<button id="ModalCloseBtn" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title">PASAR LLAMADA Nº <span id="NoLlaPas"></span> A INCIDENCIAS</h3>
                        <p>Por favor, rellena los campos requeridos para pasar la llamada como incidencia.</p>
                    </div>
                    <div id="ResultPasLlaInc"></div>
                </div>
            </div>
        </div>
        <?php } ?>
        
        <?php if($llamadas_agr == 1) { ?>
    	<div class="well well-sm llamadas">
        	<ol class="breadcrumb">
                <li>Añadir llamada</li>
            </ol>
            <a href="llamadas" class="btn btn-back">
                <i class="glyphicon glyphicon-circle-arrow-left"></i> Volver atrás
            </a>
            <hr>
            <form id="frmAllamada" method="post" class="form-horizontal">
            	<div class="form-group">
                    <div class="control-group">
                        <label for="tienda" class="col-sm-2 control-label">Tienda</label>
                        <div class="col-sm-10">
                            <div class="btn-group" data-toggle="buttons" style="margin:0">
                                <label id="btnPaguera" class="btn btn-default">
                                    <input type="radio" name="lltienda" id="lltiendaPaguera" value="paguera"> Paguera
                                </label>
                                <label id="btnSPonsa" class="btn btn-default">
                                    <input type="radio" name="lltienda" id="lltiendaSPonsa" value="sponsa"> Santa Ponsa
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="control-group">
                        <label for="tomador" class="col-sm-2 control-label">Tomador</label>
                        <div class="col-sm-10">
                        	<div class="select-style" style="max-width:200px">
                            <select id="lltomador">
                                <option value="" hidden selected>--</option>
                                <?php while($rstecnicos = $rtecnicos->fetch_assoc()) { ?>
                                <option value="<?= $rstecnicos['nombre']; ?>"><?= $rstecnicos['nombre']; ?></option>
                                <?php } ?>
                            </select>
                        	</div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="control-group">
                        <label for="dia" class="col-sm-2 control-label">Fecha y Hora</label>
                        <div class="col-sm-10">
                        	<table>
                    			<tr>
                    				<td>
                        				<input type="text" class="form-control" id="lldia" placeholder="__/__/____" style="width:170px" onkeyup="FormatoFecha(this,'/',patron,true)">
                                    </td>
                                    <td>
                                        <div class="select-style time">
                                            <select id="llhora">
                                                <option value="" hidden selected>--</option>
                                                <?php for($hora = 9; $hora < 22; $hora++) { ?>
                                                <option value="<?= $hora; ?>"><?= ($hora < 10) ? '0'.$hora : $hora; ?></option>
                                                <?php } ?>
                                                </select>
                                            </select>
                                        </div>
                            		</td>
                                    <td>
                                        <div class="select-style time">
                                            <select id="llminutos">
                                                <option value="" hidden selected>--</option>
                                                <?php for($minuto = 0; $minuto < 60; $minuto++) { ?>
                                                <option value="<?= $minuto; ?>"><?= ($minuto < 10) ? '0'.$minuto : $minuto; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <a class="TodayDate"><i class="glyphicon glyphicon-calendar"></i> Fecha Hoy</a>
                                        <a class="TodayTime"><i class="glyphicon glyphicon-time"></i> Hora Actual</a>
                                    </td>
                        		</tr>
                        	</table>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="control-group">
                        <label for="donde" class="col-sm-2 control-label">De Donde Llama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="lldonde">
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="control-group">
                        <label for="quien" class="col-sm-2 control-label">Quién Llama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="llquien">
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="control-group">
                        <label for="motivo" class="col-sm-2 control-label">Motivo</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="llmotivo" rows="1"></textarea>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="control-group">
                        <label for="con" class="col-sm-2 control-label">Hablar Con</label>
                        <div class="col-sm-10">
                        	<div class="select-style">
                            <select id="llcon" name="llcon">
                                <option value="" hidden selected>--</option>
                                <?php while($rscontecnicos = $rcontecnicos->fetch_assoc()) { ?>
                                <option value="<?= $rscontecnicos['nombre']; ?>"><?= $rscontecnicos['nombre']; ?></option>
                                <?php } ?>
                            </select>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group" id="showhablarcon">
                    <div class="control-group">
                        <label for="quien" class="col-sm-2 control-label">Teléfono de Contacto</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="llphone" style="width:200px">
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-12">
                        <button type="button" id="BtnAllamada" class="btn btn-gac btn-block">Añadir llamada</button>
                    </div>
           		</div>
			</div>
            </form>
        </div>
        <?php } ?>
        
        <?php if($llamadas_edt == 1) { ?>
    	<div class="well well-sm llamadas">
        	<ol class="breadcrumb">
                <li>Editar llamada nº <?= $explode_args[1]; ?></li>
            </ol>
        	<a href="llamadas" class="btn btn-back">
                <i class="glyphicon glyphicon-circle-arrow-left"></i> Volver atrás
            </a>
        	<hr>
            <?php if($edt_total > 0) { ?>
            <form id="frmEdtllamada" method="post" class="form-horizontal">
            	<input type="hidden" id="lledtid" value="<?= $rs_edt['id']; ?>">
                <div class="form-group">
                    <div class="control-group">
                        <label for="tienda" class="col-sm-2 control-label">Tienda</label>
                        <input type="hidden" id="lledttienda" value="<?= $rs_edt['tienda']; ?>">
                        <div class="col-sm-10">
                        	<?php if($row['admin'] == 1) { ?>
                            <div class="btn-group" data-toggle="buttons">
                                <label id="btnPaguera" class="btn btn-default" <?= ($rs_edt['tienda'] == 'paguera') ? 'style="background:#FFC60A; color:#fff; border-color:transparent"': ''; ?>>
                                    <input type="radio" name="lledttienda" id="lledttiendaPaguera" value="paguera"> Paguera
                                </label>
                                <label id="btnSPonsa" class="btn btn-default" <?= ($rs_edt['tienda'] == 'sponsa') ? 'style="background:#66CDF2; color:#fff; border-color:transparent"': ''; ?>>
                                    <input type="radio" name="lledttienda" id="lledttiendaSPonsa" value="sponsa"> Santa Ponsa
                                </label>
                            </div>
                            <?php }else { ?>
                            <div class="btn-group sino" data-toggle="buttons">
                            	<?php if($rs_edt['tienda'] == 'paguera') { ?>
                                <label id="btnPaguera" class="btn btn-default disabled">
                                    <input type="radio" name="lledttienda" id="lledttiendaPaguera" value="paguera"> Paguera
                                </label>
                                <?php }else if($rs_edt['tienda'] == 'sponsa') { ?>
                                <label id="btnSPonsa" class="btn btn-default disabled">
                                    <input type="radio" name="lledttienda" id="lledttiendaSPonsa" value="sponsa"> Santa Ponsa
                                </label>
                                <?php } ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-top:-15px">
                    <div class="control-group">
                        <label for="tomador" class="col-sm-2 control-label">Tomador</label>
                        <div class="col-sm-10">
                        	<?php if($row['admin'] == 1) { ?>
                            <div class="select-style">
                            <select id="lledttomador">
                                <option value="<?= $rs_edt['tomador']; ?>" hidden selected><?= $rs_edt['tomador']; ?></option>
                                <?php while($rstecnicos = $rtecnicos->fetch_assoc()) { ?>
                                <option value="<?= $rstecnicos['nombre']; ?>"><?= mb_strtoupper($rstecnicos['nombre'], 'utf-8'); ?></option>
                                <?php } ?>
                            </select>
                            </div>
                            <?php }else { ?>
                            <div class="select-style disabled">
                            <select id="lledttomador" disabled>
                                <option value="<?= $rs_edt['tomador']; ?>" selected><?= $rs_edt['tomador']; ?></option>
                            </select>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="control-group">
                        <label for="dia" class="col-sm-2 control-label">Día y Hora</label>
                        <div class="col-sm-10">
                        	<?php if($row['admin'] == 1) { ?>
                            <table>
                            	<tr>
                                	<td>
                            			<input type="text" class="form-control" id="lledtdia" placeholder="__/__/____" value="<?= $edtlladia; ?>" style="width:170px" onkeyup="FormatoFecha(this,'/',patron,true)">
                                	</td>
                                    <td>
                                        <div class="select-style time">
                                            <select id="lledthora">
                                                <option value="<?= $edtllahora; ?>" hidden selected><?= $edtllahora; ?></option>
                                                <?php for($hora = 9; $hora < 22; $hora++) { ?>
                                                <option value="<?= $hora; ?>"><?= ($hora < 10) ? '0'.$hora : $hora; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="select-style time">
                                            <select id="lledtminutos">
                                                <option value="<?= $edtllaminutos; ?>" hidden selected><?= $edtllaminutos; ?></option>
                                                <?php for($minuto = 0; $minuto < 60; $minuto++) { ?>
                                                <option value="<?= $minuto; ?>"><?= ($minuto < 10) ? '0'.$minuto : $minuto; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </td>
                                    <?php if($row['admin'] == 1) { ?>
                                    <td>
                                        <a class="TodayDate"><i class="glyphicon glyphicon-calendar"></i> Fecha Hoy</a>
                                        <a class="TodayTime"><i class="glyphicon glyphicon-time"></i> Hora Actual</a>
                                    </td>
                                    <?php } ?>
                             	</tr>
                             </table>
							<?php }else { ?>
                            <table>
                            	<tr>
                                	<td>
                            			<input type="text" class="form-control" id="lledtdia" placeholder="__/__/____" style="width:170px" value="<?= $edtlladia; ?>" disabled>
                                    </td>
                                    <td>
                                        <div class="select-style time disabled">
                                            <select id="lledthora" disabled>
                                                <option value="<?= $edtllahora; ?>" selected><?= $edtllahora; ?></option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="select-style time disabled">
                                            <select id="lledtminutos" disabled>
                                                <option value="<?= $edtllaminutos; ?>" selected><?= $edtllaminutos; ?></option>
                                            </select>
                                        </div>
                                    </td>
                             	</tr>
                            </table>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="control-group">
                        <label for="donde" class="col-sm-2 control-label">De Donde Llama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="lledtdonde" value="<?= $rs_edt['donde']; ?>">
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="control-group">
                        <label for="quien" class="col-sm-2 control-label">Quién Llama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="lledtquien" value="<?= $rs_edt['quien']; ?>">
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="control-group">
                        <label for="motivo" class="col-sm-2 control-label">Motivo</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="lledtmotivo" rows="1"><?= $rs_edt['motivo']; ?></textarea>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="control-group">
                        <label for="con" class="col-sm-2 control-label">Hablar Con</label>
                        <div class="col-sm-10">
                        	<div class="select-style" style="max-width:200px">
                            <select id="lledtcon" name="lledtcon">
                                <option value="<?= $rs_edt['con']; ?>" selected><?= $rs_edt['con']; ?></option>
                                <?php
								$sql = 'SELECT nombre
										FROM tecnicos
										WHERE nombre != "'.$rs_edt['con'].'"
										ORDER BY nombre ASC';
								$rscedt_tecnicos = $con->query($sql);
								?>
                                <?php while($rsedt_tecnicos = $rscedt_tecnicos->fetch_assoc()) { ?>
                                <option value="<?= $rsedt_tecnicos['nombre']; ?>"><?= $rsedt_tecnicos['nombre']; ?></option>
                                <?php } ?>
                            </select>
                            </div>
                        </div>
                    </div>
                </div>
                
                <script type="text/javascript">
				$(document).ready(function(){
					var EdtCon = "<?= $rs_edt['con']; ?>";
					if(EdtCon !== ""){
						$("#showedthablarcon").show();
					}else{
						$("#showedthablarcon").hide();
					}
				});
				</script>
                
                <div class="form-group" id="showedthablarcon">
                    <div class="control-group">
                        <label for="quien" class="col-sm-2 control-label">Teléfono de Contacto</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="lledtphone" style="width:200px" value="<?= $rs_edt['phone']; ?>">
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-12">
                        <button type="button" id="BtnEdtllamada" class="btn btn-gac btn-block">Guardar cambios</button>
                    </div>
           		</div>
			</div>
            </form>
            <?php }else { ?>
            <h3 class="text-white">La llamada que intentas editar no se ha encontrado, puede ser por los siguientes motivos:</h3>
            <ul class="text-white" style="margin-top:15px">
                <li><h4>Todavía no ha sido creada y no existe.</h4></li>
                <li><h4>Ya ha sido guardada, por lo que ya no es posible editarla.</h4></li>
                <li><h4>Ha sido eliminada por un técnico o un administrador.</h4></li>
            </ul>
            <br>
            <h5 class="text-white">Haz clic en "Volver atrás" para volver donde estabas o en el logo de la parte superior izquierda para volver a la página principal.</h5>
            <?php } ?>
        </div>
        <?php } ?>
        
        <?php if($llamadas_sch == 1) { ?>
        <div class="well well-sm llamadas">
        	<ol class="breadcrumb">
                <li>Búsqueda llamadas</li>
            </ol>
            <a href="llamadas" class="btn btn-back">
                <i class="glyphicon glyphicon-circle-arrow-left"></i> Volver atrás
            </a>
            <form id="frmSchllamadas" class="form-inline">
        <div class="btn-group" style="margin-top:5px">
        	<button type="button" class="btn btn-gac" data-toggle="dropdown" id="TxtllamadasSch" style="display:inline-block">Buscar por</button>
          	<button type="button" class="btn btn-gac dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-bottom-right-radius:0; border-top-right-radius:0; border-right:none">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          	</button>
          	<ul class="dropdown-menu" id="Schllamadas">
            	<li><a href="#" data-type="tomador">Tomador</a></li>
                <li><a href="#" data-type="dia">Día</a></li>
                <li><a href="#" data-type="con">Hablar con</a></li>
          	</ul>
			
            <div class="input-group">
            	<div id="ShowSchDefaultllamadas">
          			<input type="text" class="form-control" size="40" placeholder="&laquo;-- Selecciona el tipo de búsqueda" style="border-bottom-left-radius:0; border-top-left-radius:0" disabled>
                </div>
                <div id="ShowSchTomadorllamadas">
                	<select id="TomadorSchllamadas" class="form-control" style="width:100%">
                    <option value="" selected hidden>--&raquo; Selecciona un tomador &laquo;--</option>
                    	<?php while($rstecnicos = $rtecnicos->fetch_assoc()) { ?>
                    	<option value="<?= strtolower($rstecnicos['nombre']); ?>"><?= $rstecnicos['nombre']; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div id="ShowSchDiallamadas">
                	<input type="text" class="form-control" id="DiaSchllamadas" placeholder="__/__/____" style="display:inline-block; width:100%" onkeyup="FormatoFecha(this,'/',patron,true)">
                </div>
                <div id="ShowSchConllamadas">
                	<input type="text" id="ConSchllamadas" class="form-control" placeholder="Introduce un nombre..." size="40" style="border-bottom-left-radius:0; border-top-left-radius:0">
                </div>
          		<span class="input-group-btn">
            		<button id="BtnSchllamadas" class="btn btn-gac" type="button">
                    	<i class="glyphicon glyphicon-search"></i>
                    </button>
          		</span>
        	</div>
		</div>
        </form>
            
            <div class="pull-right text-default">
                Se han encontrado <strong><?= $total_schllamadas['total']; ?></strong> resultados
            </div>
            
            <div id="TableResponsive" class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr class="info">
                    	<th>Tienda</th>
                        <th>Tomador</th>
                        <th>Día</th>
                        <th>Hora</th>
                        <th>De donde llama...</th>
                        <th>Quién llama...</th>
                        <th>Motivo</th>
                        <th>Hablar con...</th>
                        <th>Acción</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($total_schllamadas['total'] > 0) { ?>
                    <?php while($rs_schllamadas = $rsc_schllamadas->fetch_assoc()) { ?>
                    <?php
                    if($rs_schllamadas['tienda'] == 'sponsa') {
						$rs_schllamadas['tienda'] = 'S. Ponsa';
					}
					
					$schsemanas = array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');
					$sep_schlladiahora = explode(' ', $rs_schllamadas['dia']);
					$sep_schlladia = explode('-', $sep_schlladiahora[0]);
					$schlladia = $schsemanas[date('w', strtotime($sep_schlladiahora[0]))];
					$schllahora = substr($sep_schlladiahora[1], 0, -3);
					?>
                    <tr>
                        <td><?= mb_strtoupper($rs_schllamadas['tienda'], 'utf-8'); ?></td>
                        <td><?= RecTexto(mb_strtoupper($rs_schllamadas['tomador'], 'utf-8'), 12); ?></td>
                        <td><?= mb_strtoupper($schlladia, 'utf-8').', '.$sep_schlladia[2]; ?></td>
                        <td><?= $schllahora; ?></td>
                        <td><?= RecTexto(mb_strtoupper($rs_schllamadas['donde'], 'utf-8'), 12); ?></td>
                        <td><?= RecTexto(mb_strtoupper($rs_schllamadas['quien'], 'utf-8'), 12); ?></td>
                        <td><?= RecTexto(mb_strtoupper($rs_schllamadas['motivo'], 'utf-8'), 12); ?></td>
                        <td><?= ($rs_schllamadas['con']) ? mb_strtoupper($rs_schllamadas['con'], 'utf-8') : '--'; ?></td>
                        <td>
                        <div class="btn-group ActionResponsive">
                            <button type="button" class="btn btn-default dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="position:relative"><i class="glyphicon glyphicon-cog"></i> <span class="caret"></span></button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                <a class="VerLlamada" data-id="<?= $rs_schllamadas['id']; ?>" style="color:#333; cursor:pointer"><i class="glyphicon glyphicon-eye-open"></i> Ver llamada</a>
                                </li>
                                <li>
                                <a class="PIncLlamada" data-nid="<?= $rs_schllamadas['id']; ?>" style="color:#333; cursor:pointer"><i class="glyphicon glyphicon-share-alt"></i> Pasar a incidencias</a>
                                </li>
                                <li>
                                <a href="llamadas/editar-<?= $rs_schllamadas['id']; ?>" style="color:#333"><i class="glyphicon glyphicon-pencil" style="font-size:12px"></i> Editar llamada</a>
                                </li>
                                <?php if($row['admin'] == 1) { ?>
                                <li><a class="SavLlamada" data-id="<?= $rs_schllamadas['id']; ?>" style="color:#333; cursor:pointer"><i class="glyphicon glyphicon-saved"></i> Guardar llamada</a></li>
                                <?php } ?>
                            </ul>
                        </div>
                        
                        <div class="ActionNoResponsive">
                        <a class="VerLlamada" data-id="<?= $rs_schllamadas['id']; ?>" style="color:#333; margin-right:15px; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Ver Llamada"><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a class="PIncLlamada" data-nid="<?= $rs_schllamadas['id']; ?>" style="color:#333; margin-right:15px; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Pasar a Incidencias"><i class="glyphicon glyphicon-share-alt"></i></a>
                        <a href="llamadas/editar-<?= $rs_schllamadas['id']; ?>" style="color:#333; margin-right:15px" data-toggle="tooltip" data-placement="bottom" title="Editar Llamada"><i class="glyphicon glyphicon-pencil" style="font-size:12px"></i></a>
                        <a class="SavLlamada" data-id="<?= $rs_schllamadas['id']; ?>" style="color:#333; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Guardar Llamada"><i class="glyphicon glyphicon-saved"></i></a>
                        </div>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php }else { ?>
                    <tr>
                        <td align="center" colspan="9">No se han encontrado resultados de su búsqueda</td>
                    </tr>
                    <?php } ?>
                </tbody>
           </table>
           </div>
    	</div>
        <?php } ?>
        
        <?php if($llamadas_his == 1) { ?>
    <div class="well well-sm llamadas">
    	<ol class="breadcrumb">
       		<li>Historial llamadas</li>
        </ol>
    	<a href="llamadas">
        	<button class="btn btn-back"><i class="glyphicon glyphicon-circle-arrow-left"></i> Volver atrás</button>
        </a>
    	<form id="frmSchllamadas" class="form-inline">
        <div class="btn-group">
        	<button type="button" class="btn btn-gac" data-toggle="dropdown" id="TxtllamadasSch" style="display:inline-block" <?= ($total_hisllamadas['total'] > 0) ? '' : 'disabled'; ?>>Buscar por</button>
          	<button type="button" class="btn btn-gac dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-bottom-right-radius:0; border-top-right-radius:0; border-right:none" <?= ($total_hisllamadas['total'] > 0) ? '' : 'disabled'; ?>>
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          	</button>
          	<ul class="dropdown-menu" id="Schllamadas">
                <li><a href="#" data-type="dia">Día</a></li>
          	</ul>
			
            <div class="input-group">
            	<div id="ShowSchDefaultllamadas">
          			<input type="text" class="form-control" size="40" placeholder="&laquo;-- Selecciona el tipo de búsqueda" style="border-bottom-left-radius:0; border-top-left-radius:0" disabled>
                </div>
                <div id="ShowSchDiallamadas">
                	<input type="text" class="form-control" id="DiaSchllamadas" placeholder="__/__/____" style="display:inline-block; width:100%" onkeyup="FormatoFecha(this,'/',patron,true)">
                </div>
          		<span class="input-group-btn">
            		<button id="BtnSchllamadas" class="btn btn-gac" type="button" <?= ($total_hisllamadas['total'] > 0) ? '' : 'disabled'; ?>>
                    	<i class="glyphicon glyphicon-search"></i>
                    </button>
          		</span>
        	</div>
		</div>
        </form>
        
        <?php if($row['admin'] == 1) { ?>
        <div class="btnRight">
        	<a>
            	<button id="DelHisAllLla" class="btn btn-gac" <?= ($total_hisllamadas['total'] > 0) ? '' : 'disabled'; ?>>
                	Eliminar todo el historial de llamadas
              	</button>
          	</a>
        </div>
        <?php } ?>
        
        <div id="TableResponsive" class="table-responsive">
        <table class="table table-bordered">
			<thead>
				<tr class="info">
                    <th>Tienda</th>
                    <th>Tomador</th>
                    <th>Día</th>
                    <th>Hora</th>
                    <th>De donde llama...</th>
                    <th>Quién llama...</th>
                    <th>Motivo</th>
                    <th>Hablar con...</th>
                    <th>Acción</th>
				</tr>
			</thead>
            <tbody>
            <?php if($total_hisllamadas['total'] > 0) { ?>
            <?php while($rs_hisllamadas = $rsc_hisllamadas->fetch_assoc()) { ?>
            <?php
			if($rs_hisllamadas['tienda'] == 'sponsa') {
				$rs_hisllamadas['tienda'] = 'S. Ponsa';
			}
			
			$hissemanas = array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');
			$sep_hislladiahora = explode(' ', $rs_hisllamadas['dia']);
			$sep_hislladia = explode('-', $sep_hislladiahora[0]);
			$hislladia = $hissemanas[date('w', strtotime($sep_hislladiahora[0]))];
			$hisllahora = substr($sep_hislladiahora[1], 0, -3);
			?>
            <tr>
                <td><?= RecTexto(mb_strtoupper($rs_hisllamadas['tienda'], 'utf-8'), 12); ?></td>
                <td><?= RecTexto(mb_strtoupper($rs_hisllamadas['tomador'], 'utf-8'), 12); ?></td>
                <td><?= mb_strtoupper($hislladia, 'utf-8').', '.$sep_hislladia[2]; ?></td>
                <td><?= $hisllahora; ?></td>
                <td><?= ($rs_hisllamadas['donde']) ? RecTexto(mb_strtoupper($rs_hisllamadas['donde'], 'utf-8'), 12) : '--'; ?></td>
                <td><?= ($rs_hisllamadas['quien']) ? RecTexto(mb_strtoupper($rs_hisllamadas['quien'], 'utf-8'), 12) : '--'; ?></td>
                <td><?= RecTexto(mb_strtoupper($rs_hisllamadas['motivo'], 'utf-8'), 12); ?></td>
                <td><?= ($rs_hisllamadas['con']) ? mb_strtoupper($rs_hisllamadas['con'], 'utf-8') : '--'; ?></td>
                <td>
                <div class="btn-group ActionResponsive">
                    <button type="button" class="btn btn-default dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="position:relative"><i class="glyphicon glyphicon-cog"></i> <span class="caret"></span></button>
                    <ul class="dropdown-menu dropdown-menu-right">
                    	<li>
                        <a class="VerLlamada" data-id="<?= $rs_hisllamadas['id']; ?>" style="color:#333; cursor:pointer"><i class="glyphicon glyphicon-eye-open"></i> Ver llamada</a>
                        </li>
                       <?php if($row['admin'] == 1) { ?>
                        <li><a class="DelLlamada" data-id="<?= $rs_hisllamadas['id']; ?>" style="color:#333; cursor:pointer"><i class="glyphicon glyphicon-remove"></i> Eliminar llamada</a></li>
                        <?php } ?>
                    </ul>
                </div>
                
                <div class="ActionNoResponsive">
                <a class="VerLlamada" data-id="<?= $rs_hisllamadas['id']; ?>" style="color:#333; margin-right:15px; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Ver Llamada"><i class="glyphicon glyphicon-eye-open"></i></a>
                <?php if($row['admin'] == 1) { ?>
                <a class="DelLlamada" data-id="<?= $rs_hisllamadas['id']; ?>" style="color:#333; cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Eliminar Llamada"><i class="glyphicon glyphicon-remove"></i></a>
                <?php } ?>
                </div>
                </td>
            </tr>
			<?php } ?>
            <?php }else { ?>
            <tr>
            	<td align="center" colspan="9" style="padding:10px">No se han encontrado historial para mostrar</td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
        </div>
        
        <nav>
        	<?php if($ultima_pagina > 0) { ?>
            <div class="pull-left" style="margin:5px 0 0 5px">
                <span class="text-white">Página <strong><?= $pagina_actual; ?></strong> de <strong><?= $ultima_pagina; ?></strong></span>
            </div>
            <?php } ?>
  			<ul class="pager">
            	<?php if($ultima_pagina > 0) { ?>
					<?php if($pagina_actual > 1) { ?>
                    <li><a href="llamadas/historial/?pg=<?= $pagina_anterior; ?>">Anterior</a></li>
                    <?php }else { ?>
                    <li class="disabled"><a langhref="#">Anterior</a></li>
                    <?php } ?>
                    <?php if($pagina_actual < $ultima_pagina) { ?>
                    <li><a href="llamadas/historial/?pg=<?= $pagina_siguiente; ?>">Siguiente</a></li>
                    <?php }else { ?>
                    <li class="disabled"><a langhref="#">Siguiente</a></li>
                    <?php } ?>
                <?php } ?>
  			</ul>
		</nav>
    </div>
    <?php } ?>
    </div>
</div>

<div class="modal fade" id="ShowVerLlamada" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border:none; margin-bottom:-20px">
                <h3 class="modal-title" id="myModalLabel">VER LLAMADA Nº <span id="IDLlaVer"></span></h3>
            </div>
            <div id="ResultVerLla" class="modal-body" style="text-transform:uppercase"></div>
            <div class="modal-footer" style="text-align:center; border:none; margin-top:-10px">
                <button type="button" class="btn btn-gac" data-dismiss="modal">Cerrar ventana</button>
            </div>
        </div>
    </div>
</div>