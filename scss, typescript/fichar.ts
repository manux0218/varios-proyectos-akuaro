import { Component, Injectable } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { Http, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/map';

import { Diagnostic } from '@ionic-native/diagnostic';
import { Device } from '@ionic-native/device';
import { Camera } from '@ionic-native/camera';
import { Geolocation } from '@ionic-native/geolocation';
import { SQLite, SQLiteObject } from "@ionic-native/sqlite";
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

declare var FileUploadOptions: any;

@Component({
  selector: 'page-fichar',
  templateUrl: 'fichar.html'
})

@Injectable()

export class FicharPage {

  urlWebService: string;
  imageFichar: string;
  valMaquina: string;
  valEmpresa: string;
  valUsuario: string;
  valCTarjeta: string;
  valFrase: string;
  geoLat: number;
  geoLng: number;
  isSending: boolean = false;
  isSendSuccess: boolean = false;
  isActiveGPS: boolean;
  responseMsg: string;

  private fileTransfer: FileTransferObject = this.transfer.create();

  constructor(
    public navCtrl: NavController,
    private toastCtrl: ToastController,
    private diagnostic: Diagnostic,
    private device: Device,
    private camera: Camera,
    public http: Http,
    public transfer: FileTransfer,
    public file: File,
    private geolocation: Geolocation,
    private sqlite: SQLite
  ) {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql("SELECT * FROM registrados WHERE uuid = ? LIMIT 1", [this.device.uuid]).then((data) => {
        this.valMaquina = data.rows.item(0).maquina;
        this.valEmpresa = data.rows.item(0).empresa;
        this.valUsuario = data.rows.item(0).usuario;
        this.valCTarjeta = data.rows.item(0).ctarjeta;
        this.valFrase = data.rows.item(0).frase;
      });
    });
    setInterval(() => {
      this.diagnostic.isLocationEnabled().then((status) => {
        this.isActiveGPS = status;
      });
    }, 1000);
  }

  btnHacerFoto() {
    let cameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      correctOrientation: true,
      allowEdit: false,
      saveToPhotoAlbum: false
    };
    this.camera.getPicture(cameraOptions).then((imageData) => {
      this.imageFichar = imageData;
    });
  };

  btnEnviar() {
    if(this.isActiveGPS){
      this.geolocation.getCurrentPosition().then((resp) => {
        this.geoLat = resp.coords.latitude;
        this.geoLng = resp.coords.longitude;
        this.isSending = true;
      });
      setTimeout(() => {
        this.urlWebService = 'ELIMINADO';
        let params: URLSearchParams = new URLSearchParams();
        params.set('call', 'fichar');
        params.set('m', this.valMaquina);
        params.set('e', this.valEmpresa);
        params.set('l', this.valUsuario);
        params.set('p', this.valCTarjeta);
        params.set('pa', this.valFrase);
        params.set('u', this.geoLat + ',' + this.geoLng);
        this.http.get(this.urlWebService, { search: params }).timeout(10000).map(res => res.json()).subscribe(data => {
          if(data.success === true){
            this.uploadFileImage();
            this.responseMsg = data.request;
          }else{
            this.responseMsg = 'En estos momentos no se puede establecer la conexión con el servidor.';
          }
        }, () => {
          this.responseMsg = 'El servidor no responde. Inténtelo de nuevo más tarde.';
        });
        this.isSendSuccess = true;
      }, 1000);
    }else{
      let toast = this.toastCtrl.create({
        message: 'Es necesario activar GPS',
        duration: 3000
      });
      toast.present();
    }
  };

  btnBack() {
    this.navCtrl.pop();
  };

}